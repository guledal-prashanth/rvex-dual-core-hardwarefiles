library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library std;
use std.textio.all;

library rvex;
use rvex.common_pkg.all;
use rvex.utils_pkg.all;
use rvex.simUtils_pkg.all;
use rvex.simUtils_mem_pkg.all;
use rvex.bus_pkg.all;
use rvex.core_pkg.all;
use rvex.cache_pkg.all;
use rvex.synchronize_pkg.all;

entity testbench is
end testbench;

architecture Behavioral of testbench is

  -- Core and cache configuration.
  constant RCFG                 : rvex_generic_config_type := rvex_cfg(
    numLanesLog2                => 3,
    numLaneGroupsLog2           => 2,
    numContextsLog2             => 2,
    traceEnable                 => 0
  );
  constant CCFG                 : cache_generic_config_type := cache_cfg(
    instrCacheLinesLog2         => 8,
    dataCacheLinesLog2          => 8
  );
  constant SREC_FILENAME        : string := "../test-progs/sim.srec";

  -- AXI bus widths
  constant AXI_ADDRW_L2B        : natural := 5;
  constant AXI_DATAW_L2B        : natural := 6;

  -- System control signals in the rvex library format.
  signal reset                  : std_logic;
  signal clk                    : std_logic;
  signal clkEnCPU               : std_logic;
  signal clkEnBus               : std_logic;

  -- Debug interface signals.
  signal dbg2rv_addr            : rvex_address_type;
  signal dbg2rv_readEnable      : std_logic;
  signal dbg2rv_writeEnable     : std_logic;
  signal dbg2rv_writeMask       : rvex_mask_type;
  signal dbg2rv_writeData       : rvex_data_type;
  signal rv2dbg_readData        : rvex_data_type;

  -- Common cache interface signals.
  signal rv2cache_decouple      : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal cache2rv_blockReconfig : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal cache2rv_stallIn       : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2cache_stallOut      : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal cache2rv_status        : rvex_cacheStatus_array(2**RCFG.numLaneGroupsLog2-1 downto 0);

  -- Debug cache interface signals
  signal cache2rv_dStallIn      : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal cache2rv_iStallIn      : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);

  -- Instruction cache interface signals.
  signal rv2icache_PCs          : rvex_address_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2icache_fetch        : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2icache_cancel       : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal icache2rv_instr        : rvex_syllable_array(2**RCFG.numLanesLog2-1 downto 0);
  signal icache2rv_busFault     : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal icache2rv_affinity     : std_logic_vector(2**RCFG.numLaneGroupsLog2*RCFG.numLaneGroupsLog2-1 downto 0);

  -- Data cache interface signals.
  signal rv2dcache_addr         : rvex_address_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2dcache_readEnable   : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2dcache_writeData    : rvex_data_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2dcache_writeMask    : rvex_mask_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2dcache_writeEnable  : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2dcache_synchronize  : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal rv2dcache_bypass       : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal dcache2rv_readData     : rvex_data_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal dcache2rv_busFault     : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal dcache2rv_ifaceFault   : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);

  -- Cache to arbiter interface signals.
  signal cache2arb_bus          : bus_mst2slv_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal arb2cache_bus          : bus_slv2mst_array(2**RCFG.numLaneGroupsLog2-1 downto 0);

  -- Arbited bus connected to the memory model.
  signal arb2mem_bus_sourceless : bus_mst2slv_type;
  signal arb2mem_bus_syncIn     : bus_mst2slv_type;
  signal arb2mem_bus_syncOut    : bus_mst2slv_type;
  signal mem2arb_bus_syncIn     : bus_slv2mst_type;
  signal mem2arb_bus_syncOut    : bus_slv2mst_type;

  -- Bus snooping interface signals
  signal bus2cache_invalAddr    : rvex_address_type;
  signal bus2cache_invalSource  : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  signal bus2cache_invalEnable  : std_logic;

  -- Grab the source of the bus signal
  signal arb_source             : rvex_data_type;

  signal maxi_aclk                  : std_logic;
  signal maxi_awid                  : std_logic_vector(2 DOWNTO 0);
  signal maxi_awaddr                : std_logic_vector(2**AXI_ADDRW_L2B - 1 downto 0);
  signal maxi_awlen                 : std_logic_vector(3 downto 0);
  signal maxi_awsize                : std_logic_vector(2 downto 0);
  signal maxi_awburst               : std_logic_vector(1 downto 0);
  signal maxi_awlock                : std_logic_vector(1 downto 0);
  signal maxi_awcache               : std_logic_vector(3 downto 0);
  signal maxi_awprot                : std_logic_vector(2 downto 0);
  signal maxi_awqos                 : std_logic_vector(3 downto 0);
  signal maxi_awuser                : std_logic_vector(4 downto 0);
  signal maxi_awvalid               : std_logic;
  signal maxi_awready               : std_logic;
  signal maxi_wid                   : std_logic_vector(2 downto 0);
  signal maxi_wdata                 : std_logic_vector(2**AXI_DATAW_L2B - 1 downto 0);
  signal maxi_wstrb                 : std_logic_vector(2**(AXI_DATAW_L2B - 3) - 1 downto 0);
  signal maxi_wlast                 : std_logic;
  signal maxi_wvalid                : std_logic;
  signal maxi_wready                : std_logic;
  signal maxi_bid                   : std_logic_vector(2 downto 0);
  signal maxi_bresp                 : std_logic_vector(1 downto 0);
  signal maxi_bvalid                : std_logic;
  signal maxi_bready                : std_logic;
  signal maxi_arid                  : std_logic_vector(2 downto 0);
  signal maxi_araddr                : std_logic_vector(2**AXI_ADDRW_L2B - 1 downto 0);
  signal maxi_arlen                 : std_logic_vector(3 downto 0);
  signal maxi_arsize                : std_logic_vector(2 downto 0);
  signal maxi_arburst               : std_logic_vector(1 downto 0);
  signal maxi_arlock                : std_logic_vector(1 downto 0);
  signal maxi_arcache               : std_logic_vector(3 downto 0);
  signal maxi_arprot                : std_logic_vector(2 downto 0);
  signal maxi_arqos                 : std_logic_vector(3 downto 0);
  signal maxi_aruser                : std_logic_vector(4 downto 0);
  signal maxi_arvalid               : std_logic;
  signal maxi_arready               : std_logic;
  signal maxi_rid                   : std_logic_vector(2 downto 0);
  signal maxi_rdata                 : std_logic_vector(2**AXI_DATAW_L2B - 1 downto 0);
  signal maxi_rresp                 : std_logic_vector(1 downto 0);
  signal maxi_rlast                 : std_logic;
  signal maxi_rvalid                : std_logic;
  signal maxi_rready                : std_logic;

--=============================================================================
begin -- architecture
--=============================================================================

  -----------------------------------------------------------------------------
  -- System control
  -----------------------------------------------------------------------------
  -- Generate clock.
  clk_proc: process is
  begin
    clk <= '1';
    wait for 5 ns;
    clk <= '0';
    wait for 5 ns;
  end process;

  -- Generate reset.
  reset_proc: process is
  begin
    reset <= '1';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    reset <= '0';
    wait;
  end process;

  -- Generate clock enables.
  clkEnCPU  <= '1';
  clkEnBus  <= '1';

  -----------------------------------------------------------------------------
  -- Instantiate the rvex core
  -----------------------------------------------------------------------------
  rvex_inst: entity rvex.core
    generic map (
      CFG                       => RCFG
    )
    port map (

      -- System control.
      reset                     => reset,
      clk                       => clk,
      clkEn                     => clkEnCPU,

      -- Common memory interface.
      rv2mem_decouple           => rv2cache_decouple,
      mem2rv_blockReconfig      => cache2rv_blockReconfig,
      mem2rv_stallIn            => cache2rv_stallIn,
      rv2mem_stallOut           => rv2cache_stallOut,
      mem2rv_cacheStatus        => cache2rv_status,

      -- Instruction memory interface.
      rv2imem_PCs               => rv2icache_PCs,
      rv2imem_fetch             => rv2icache_fetch,
      rv2imem_cancel            => rv2icache_cancel,
      imem2rv_instr             => icache2rv_instr,
      imem2rv_affinity          => icache2rv_affinity,
      imem2rv_busFault          => icache2rv_busFault,

      -- Data memory interface.
      rv2dmem_addr              => rv2dcache_addr,
      rv2dmem_readEnable        => rv2dcache_readEnable,
      rv2dmem_writeData         => rv2dcache_writeData,
      rv2dmem_writeMask         => rv2dcache_writeMask,
      rv2dmem_writeEnable       => rv2dcache_writeEnable,
      rv2dmem_synchronize       => rv2dcache_synchronize,
      dmem2rv_readData          => dcache2rv_readData,
      dmem2rv_ifaceFault        => dcache2rv_busFault,
      dmem2rv_busFault          => dcache2rv_ifaceFault,

      -- Control/debug bus interface.
      dbg2rv_addr               => dbg2rv_addr,
      dbg2rv_readEnable         => dbg2rv_readEnable,
      dbg2rv_writeEnable        => dbg2rv_writeEnable,
      dbg2rv_writeMask          => dbg2rv_writeMask,
      dbg2rv_writeData          => dbg2rv_writeData,
      rv2dbg_readData           => rv2dbg_readData

    );

  -- We're not using the debug interface.
  dbg2rv_addr         <= (others => '0');
  dbg2rv_readEnable   <= '0';
  dbg2rv_writeEnable  <= '0';
  dbg2rv_writeMask    <= (others => '1');
  dbg2rv_writeData    <= (others => '0');

  -----------------------------------------------------------------------------
  -- Test cache integrity
  -----------------------------------------------------------------------------
  test_mem_block: block is
    signal PCs_r                : rvex_address_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
    signal fetch_r              : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    signal addr_r               : rvex_address_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
    signal readEnable_r         : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    signal writeData_r          : rvex_data_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
    signal writeMask_r          : rvex_mask_array(2**RCFG.numLaneGroupsLog2-1 downto 0);
    signal writeEnable_r        : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    signal synchronize_r        : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
  begin
    test_mem_regs: process (clk) is
    begin
      if rising_edge(clk) then
        if reset = '1' then
          PCs_r         <= (others => (others => '0'));
          fetch_r       <= (others => '0');
          addr_r        <= (others => (others => '0'));
          readEnable_r  <= (others => '0');
          writeData_r   <= (others => (others => '0'));
          writeMask_r   <= (others => (others => '0'));
          writeEnable_r <= (others => '0');
        elsif clkEnCPU = '1' then
          for laneGroup in 0 to 2**RCFG.numLaneGroupsLog2-1 loop
            if rv2cache_stallOut(laneGroup) = '0' then
              PCs_r(laneGroup)          <= rv2icache_PCs(laneGroup);
              fetch_r(laneGroup)        <= rv2icache_fetch(laneGroup);
              addr_r(laneGroup)         <= rv2dcache_addr(laneGroup);
              readEnable_r(laneGroup)   <= rv2dcache_readEnable(laneGroup);
              writeData_r(laneGroup)    <= rv2dcache_writeData(laneGroup);
              writeMask_r(laneGroup)    <= rv2dcache_writeMask(laneGroup);
              writeEnable_r(laneGroup)  <= rv2dcache_writeEnable(laneGroup);
              synchronize_r(laneGroup)  <= rv2dcache_synchronize(laneGroup);
            end if;
          end loop;
        end if;
      end if;
    end process;

    test_mem_model: process is
      variable mem      : rvmem_memoryState_type;
      variable readData : rvex_data_type;
      variable lane     : natural;
      variable PC       : rvex_address_type;
      variable aff      : std_logic_vector(RCFG.numLaneGroupsLog2-1 downto 0);
      variable oldReadEnable  : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
      variable checked        : std_logic_vector(2**RCFG.numLaneGroupsLog2-1 downto 0);
    begin

      -- Load the srec file into the memory.
      rvmem_clear(mem, '0');
      rvmem_loadSRec(mem, SREC_FILENAME);

      -- Check memory results as seen by the core.
      loop

        -- Wait for the next clock.
        wait until rising_edge(clk) and clkEnCPU = '1';

        -- Loop over all the lane groups.
        for laneGroup in 0 to 2**RCFG.numLaneGroupsLog2-1 loop
          if cache2rv_dStallIn(laneGroup) = '0' then
            if oldReadEnable(laneGroup) = '0' and readEnable_r(laneGroup) = '1' then
              checked(laneGroup) := '0';
            end if;
            oldReadEnable(laneGroup) := readEnable_r(laneGroup);

            -- Check data access.
            if readEnable_r(laneGroup) = '1' and checked(laneGroup) = '0' then
              rvmem_read(mem, addr_r(laneGroup), readData);
              checked(laneGroup) := '1';
              if not std_match(readData, dcache2rv_readData(laneGroup)) then
                report "*****ERROR***** Data read from address " & rvs_hex(addr_r(laneGroup))
                     & " should have returned " & rvs_hex(readData)
                     & " but returned " & rvs_hex(dcache2rv_readData(laneGroup))
                  severity warning;
              else
                --report "Data read from address " & rvs_hex(addr_r(laneGroup))
                --     & " correctly returned " & rvs_hex(dcache2rv_readData(laneGroup))
                --  severity note;
              end if;
            elsif writeEnable_r(laneGroup) = '1' then
              if synchronize_r(laneGroup) = '0' or dcache2rv_readData(laneGroup) /= X"FFFFFFFF" then
                rvmem_write(mem, addr_r(laneGroup), writeData_r(laneGroup), writeMask_r(laneGroup));
              end if;
              --report "Processed write to address " & rvs_hex(addr_r(laneGroup))
              --     & ", value is " & rvs_hex(writeData_r(laneGroup))
              --     & " with mask " & rvs_bin(writeMask_r(laneGroup))
              --  severity note;
            end if;
          end if;

          
          if cache2rv_iStallIn(laneGroup) = '0' then
            -- Check instruction access.
            if fetch_r(laneGroup) = '1' and rv2icache_cancel(laneGroup) = '0' then
              aff := icache2rv_affinity(
                laneGroup*RCFG.numLaneGroupsLog2 + RCFG.numLaneGroupsLog2 - 1
                downto
                laneGroup*RCFG.numLaneGroupsLog2
              );
              for laneIndex in 0 to 2**(RCFG.numLanesLog2-RCFG.numLaneGroupsLog2)-1 loop
                lane := group2firstLane(laneGroup, RCFG) + laneIndex;
                PC := std_logic_vector(unsigned(PCs_r(laneGroup)) + laneIndex*4);
                rvmem_read(mem, PC, readData);
                if not std_match(readData, icache2rv_instr(lane)) then
                  report "*****ERROR***** Instruction read from address " & rvs_hex(PC)
                       & " should have returned " & rvs_hex(readData)
                       & " but returned " & rvs_hex(icache2rv_instr(lane))
                       & " from block " & rvs_uint(aff)
                    severity warning;
                else
                  --report "Instruction read from address " & rvs_hex(PC)
                  --     & " correctly returned " & rvs_hex(icache2rv_instr(lane))
                  --     & " from block " & rvs_uint(aff)
                  --  severity note;
                end if;
              end loop;
            end if;

          end if;
        end loop;

      end loop;

    end process;
  end block;

  -----------------------------------------------------------------------------
  -- Generate the bypass signals
  -----------------------------------------------------------------------------
  -- When the bypass signal is high, memory accesses bypass the cache. This is
  -- important for peripheral accesses. Because the rvex core does not generate
  -- such a signal, we generate one here based on the address: we assume that
  -- everything in the high 2 GiB memory space is mapped to peripherals and
  -- should not be cached.
  bypass_gen: for laneGroup in 2**RCFG.numLaneGroupsLog2-1 downto 0 generate
    rv2dcache_bypass(laneGroup) <= rv2dcache_addr(laneGroup)(31);
  end generate;

  -----------------------------------------------------------------------------
  -- Instantiate the cache
  -----------------------------------------------------------------------------
  cache_inst: entity rvex.cache
    generic map (
      RCFG                      => RCFG,
      CCFG                      => CCFG
    )
    port map (

      -- System control.
      reset                     => reset,
      clk                       => clk,
      clkEnCPU                  => clkEnCPU,
      clkEnBus                  => clkEnBus,

      -- Core common memory interface.
      rv2cache_decouple         => rv2cache_decouple,
      cache2rv_blockReconfig    => cache2rv_blockReconfig,
      cache2rv_stallIn          => cache2rv_stallIn,
      rv2cache_stallOut         => rv2cache_stallOut,
      cache2rv_status           => cache2rv_status,

      -- Debug memory interface
      cache2rv_dStallIn         => cache2rv_dStallIn,
      cache2rv_iStallIn         => cache2rv_iStallIn,

      -- Core instruction memory interface.
      rv2icache_PCs             => rv2icache_PCs,
      rv2icache_fetch           => rv2icache_fetch,
      rv2icache_cancel          => rv2icache_cancel,
      icache2rv_instr           => icache2rv_instr,
      icache2rv_busFault        => icache2rv_busFault,
      icache2rv_affinity        => icache2rv_affinity,

      -- Core data memory interface.
      rv2dcache_addr            => rv2dcache_addr,
      rv2dcache_readEnable      => rv2dcache_readEnable,
      rv2dcache_writeData       => rv2dcache_writeData,
      rv2dcache_writeMask       => rv2dcache_writeMask,
      rv2dcache_writeEnable     => rv2dcache_writeEnable,
      rv2dcache_synchronize     => rv2dcache_synchronize,
      rv2dcache_bypass          => rv2dcache_bypass,
      dcache2rv_readData        => dcache2rv_readData,
      dcache2rv_busFault        => dcache2rv_busFault,
      dcache2rv_ifaceFault      => dcache2rv_ifaceFault,

      -- Bus master interface.
      cache2bus_bus             => cache2arb_bus,
      bus2cache_bus             => arb2cache_bus,

      -- Bus snooping interface
      bus2cache_invalAddr       => bus2cache_invalAddr,
      bus2cache_invalSource     => bus2cache_invalSource,
      bus2cache_invalEnable     => bus2cache_invalEnable

    );

  -----------------------------------------------------------------------------
  -- Instantiate the bus arbiter
  -----------------------------------------------------------------------------
  bus_arb: entity rvex.bus_arbiter
    generic map (
      NUM_MASTERS               => 2**RCFG.numLaneGroupsLog2
    )
    port map (

      -- System control.
      reset                     => reset,
      clk                       => clk,
      clkEn                     => clkEnBus,

      -- Busses.
      mst2arb                   => cache2arb_bus,
      arb2mst                   => arb2cache_bus,
      arb2slv                   => arb2mem_bus_sourceless,
      slv2arb                   => mem2arb_bus_syncOut,

      -- Source signal
      arb2slv_source            => arb_source

    );

    arb2mem_bus_syncIn <= bus_setSource(arb2mem_bus_sourceless, arb_source);

  -----------------------------------------------------------------------------
  -- Instantiate the syncronization unit
  -----------------------------------------------------------------------------
  sync_unit: entity rvex.sync_unit
  generic map(  SOURCECOUNT => 4 )
  port map(     clk => clk,
                clkEn => clkEnBus,
                reset => reset,
                bus_mst2slv_in => arb2mem_bus_syncIn,
                bus_mst2slv_out => arb2mem_bus_syncOut,
                bus_slv2mst_in => mem2arb_bus_syncIn,
                bus_slv2mst_out => mem2arb_bus_syncOut,
                flush => (others => '0') );

  -----------------------------------------------------------------------------
  -- Generate line invalidation signals
  -----------------------------------------------------------------------------
  -- Connect address end enable trivially.
  bus2cache_invalAddr   <= arb2mem_bus_syncOut.address;
  bus2cache_invalEnable <= arb2mem_bus_syncOut.writeEnable;

  -- Decode arb2slv_source to get the bus2cache_invalSource signal.
  inval_source_proc: process (arb_source) is
  begin
    bus2cache_invalSource <= (others => '0');
    -- A synchronization signal has no real source, since its own cache ignores it
    -- Therefore, make sure it is properly invalidated
    if arb2mem_bus_syncOut.flags.synchronize = '0' then
      for laneGroup in 0 to 2**RCFG.numLaneGroupsLog2-1 loop
        if vect2uint(arb_source) = laneGroup then
          bus2cache_invalSource(laneGroup) <= '1';
        end if;
      end loop;
    end if;
  end process;

  rvex2AXI4: entity rvex.pvexAxiSlaveToplevel
  generic map (
    AXI_ADDRESSWIDTHLOG2B => AXI_ADDRW_L2B,
    AXI_DATAWIDTHLOG2B => AXI_DATAW_L2B,
    CACHE_LINECOUNTLOG2 => 10,
    CACHE_LINESIZELOG2B => 8,
    CACHE_BLOCKCOUNTLOG2 => 4
  )
  port map (
    clk => clk,
    rst => reset,
    updateAddrBase  => '0',
    baseAddrIn      => (others => '0'),
    rvex_bus_in     =>  arb2mem_bus_syncOut,
    rvex_bus_out    =>  mem2arb_bus_syncIn,
    maxi_aclk       =>  maxi_aclk,
    maxi_awid       =>  maxi_awid,
    maxi_awaddr     =>  maxi_awaddr,
    maxi_awlen      =>  maxi_awlen,
    maxi_awsize     =>  maxi_awsize,
    maxi_awburst    =>  maxi_awburst,
    maxi_awlock     =>  maxi_awlock,
    maxi_awcache    =>  maxi_awcache,
    maxi_awprot     =>  maxi_awprot,
    maxi_awqos      =>  maxi_awqos,
    maxi_awuser     =>  maxi_awuser,
    maxi_awvalid    =>  maxi_awvalid,
    maxi_awready    =>  maxi_awready,
    maxi_wid        =>  maxi_wid,
    maxi_wdata      =>  maxi_wdata,
    maxi_wstrb      =>  maxi_wstrb,
    maxi_wlast      =>  maxi_wlast,
    maxi_wvalid     =>  maxi_wvalid,
    maxi_wready     =>  maxi_wready,
    maxi_bid        =>  maxi_bid,
    maxi_bresp      =>  maxi_bresp,
    maxi_bvalid     =>  maxi_bvalid,
    maxi_bready     =>  maxi_bready,
    maxi_arid       =>  maxi_arid,
    maxi_araddr     =>  maxi_araddr,
    maxi_arlen      =>  maxi_arlen,
    maxi_arsize     =>  maxi_arsize,
    maxi_arburst    =>  maxi_arburst,
    maxi_arlock     =>  maxi_arlock,
    maxi_arcache    =>  maxi_arcache,
    maxi_arprot     =>  maxi_arprot,
    maxi_arqos      =>  maxi_arqos,
    maxi_aruser     =>  maxi_aruser,
    maxi_arvalid    =>  maxi_arvalid,
    maxi_arready    =>  maxi_arready,
    maxi_rid        =>  maxi_rid,
    maxi_rdata      =>  maxi_rdata,
    maxi_rresp      =>  maxi_rresp,
    maxi_rlast      =>  maxi_rlast,
    maxi_rvalid     =>  maxi_rvalid,
    maxi_rready     =>  maxi_rready,
    prng_newseed    => (others => '0'),
    prng_reseed     => '0'
  );

  axi_sim: entity work.AxiSlaveMock
    generic map (
  
      -- Bus metrics.
      DATA_WIDTH                  => 2**AXI_DATAW_L2B,
      ADDR_WIDTH                  => 2**AXI_ADDRW_L2B,
      LEN_WIDTH                   => 4,
      ID_WIDTH                    => 3,
  
      -- Timing stuff.
      SEED                        => 1,
      AW_RANDOM                   => true,
      AW_DEPTH                    => 4,
      W_RANDOM                    => true,
      W_DEPTH                     => 4,
      B_RANDOM                    => true,
      B_DEPTH                     => 4,
      AR_RANDOM                   => true,
      AR_DEPTH                    => 4,
      R_RANDOM                    => true,
      R_DEPTH                     => 4,
  
      -- S-record file to load into memory.
      SREC_FILE                   => SREC_FILENAME
  
    )
    port map (
  
      -- Global signals.
      clk                         => clk,
      reset                       => reset,
  
      -- Write address channel.
      awid                        => maxi_awid,
      awaddr                      => maxi_awaddr,
      awlen                       => maxi_awlen,
      awsize                      => maxi_awsize,
      awburst                     => maxi_awburst,
      awvalid                     => maxi_awvalid,
      awready                     => maxi_awready,
  
      -- Write data channel.
      wdata                       => maxi_wdata,
      wstrb                       => maxi_wstrb,
      wlast                       => maxi_wlast,
      wvalid                      => maxi_wvalid,
      wready                      => maxi_wready,
  
      bid                         => maxi_bid,
      bresp                       => maxi_bresp,
      bvalid                      => maxi_bvalid,
      bready                      => maxi_bready,
      arid                        => maxi_arid,
      araddr                      => maxi_araddr,
      arlen                       => maxi_arlen,
      arsize                      => maxi_arsize,
      arburst                     => maxi_arburst,
      arvalid                     => maxi_arvalid,
      arready                     => maxi_arready,
      rid                         => maxi_rid,
      rdata                       => maxi_rdata,
      rresp                       => maxi_rresp,
      rlast                       => maxi_rlast,
      rvalid                      => maxi_rvalid,
      rready                      => maxi_rready
    );
end Behavioral;

