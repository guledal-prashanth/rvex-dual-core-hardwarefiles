library std;
use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library work;
use work.Streams.all;
use work.Utils.all;
use work.Memory.all;

entity AxiSlaveMock is
  generic (
    
    -- Bus metrics.
    DATA_WIDTH                  : natural := 64;
    ADDR_WIDTH                  : natural := 32;
    LEN_WIDTH                   : natural := 8;
    ID_WIDTH                    : natural := 2;
    
    -- Timing stuff.
    SEED                        : positive := 1;
    AW_RANDOM                   : boolean := true;
    AW_DEPTH                    : natural := 4;
    W_RANDOM                    : boolean := true;
    W_DEPTH                     : natural := 4;
    B_RANDOM                    : boolean := true;
    B_DEPTH                     : natural := 4;
    AR_RANDOM                   : boolean := true;
    AR_DEPTH                    : natural := 4;
    R_RANDOM                    : boolean := true;
    R_DEPTH                     : natural := 4;
    
    -- S-record file to load into memory.
    SREC_FILE                   : string := ""
    
  );
  port (
    
    -- Global signals.
    clk                         : in  std_logic;
    reset                       : in  std_logic;
    
    -- Write address channel.
    awid                        : in  std_logic_vector(ID_WIDTH-1 downto 0);
    awaddr                      : in  std_logic_vector(ADDR_WIDTH-1 downto 0);
    awlen                       : in  std_logic_vector(LEN_WIDTH-1 downto 0);
    awsize                      : in  std_logic_vector(2 downto 0);
    awburst                     : in  std_logic_vector(1 downto 0);
    awvalid                     : in  std_logic;
    awready                     : out std_logic;
    
    -- Write data channel.
    wdata                       : in  std_logic_vector(DATA_WIDTH-1 downto 0);
    wstrb                       : in  std_logic_vector(DATA_WIDTH/8-1 downto 0);
    wlast                       : in  std_logic;
    wvalid                      : in  std_logic;
    wready                      : out std_logic;
    
    -- Write response channel.
    bid                         : out std_logic_vector(ID_WIDTH-1 downto 0);
    bresp                       : out std_logic_vector(1 downto 0);
    bvalid                      : out std_logic;
    bready                      : in  std_logic;
    
    -- Read address channel.
    arid                        : in  std_logic_vector(ID_WIDTH-1 downto 0);
    araddr                      : in  std_logic_vector(ADDR_WIDTH-1 downto 0);
    arlen                       : in  std_logic_vector(LEN_WIDTH-1 downto 0);
    arsize                      : in  std_logic_vector(2 downto 0);
    arburst                     : in  std_logic_vector(1 downto 0);
    arvalid                     : in  std_logic;
    arready                     : out std_logic;
    
    -- Read response channel.
    rid                         : out std_logic_vector(ID_WIDTH-1 downto 0);
    rdata                       : out std_logic_vector(DATA_WIDTH-1 downto 0);
    rresp                       : out std_logic_vector(1 downto 0);
    rlast                       : out std_logic;
    rvalid                      : out std_logic;
    rready                      : in  std_logic
    
  );
end AxiSlaveMock;

architecture Behavioral of AxiSlaveMock is
  
  -- Write address channel.
  signal iawid                  : std_logic_vector(ID_WIDTH-1 downto 0);
  signal iawaddr                : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal iawlen                 : std_logic_vector(LEN_WIDTH-1 downto 0);
  signal iawsize                : std_logic_vector(2 downto 0);
  signal iawburst               : std_logic_vector(1 downto 0);
  signal iawvalid               : std_logic;
  signal iawready               : std_logic;
  constant AWI : nat_array := cumulative((
    4 => iawid'length,
    3 => iawaddr'length,
    2 => iawlen'length,
    1 => iawsize'length,
    0 => iawburst'length
  ));
  signal awpld                  : std_logic_vector(AWI(AWI'high)-1 downto 0);
  signal iawpld                 : std_logic_vector(AWI(AWI'high)-1 downto 0);
  
  -- Write data channel.
  signal iwdata                 : std_logic_vector(DATA_WIDTH-1 downto 0);
  signal iwstrb                 : std_logic_vector(DATA_WIDTH/8-1 downto 0);
  signal iwlast                 : std_logic;
  signal iwvalid                : std_logic;
  signal iwready                : std_logic;
  constant WI : nat_array := cumulative((
    2 => iwdata'length,
    1 => iwstrb'length,
    0 => 1 -- iwlast
  ));
  signal wpld                   : std_logic_vector(WI(WI'high)-1 downto 0);
  signal iwpld                  : std_logic_vector(WI(WI'high)-1 downto 0);
  
  -- Write response channel.
  signal ibid                   : std_logic_vector(ID_WIDTH-1 downto 0);
  signal ibresp                 : std_logic_vector(1 downto 0);
  signal ibvalid                : std_logic;
  signal ibready                : std_logic;
  constant BI : nat_array := cumulative((
    1 => ibid'length,
    0 => ibresp'length
  ));
  signal bpld                   : std_logic_vector(BI(BI'high)-1 downto 0);
  signal ibpld                  : std_logic_vector(BI(BI'high)-1 downto 0);
  
  -- Read address channel.
  signal iarid                  : std_logic_vector(ID_WIDTH-1 downto 0);
  signal iaraddr                : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal iarlen                 : std_logic_vector(LEN_WIDTH-1 downto 0);
  signal iarsize                : std_logic_vector(2 downto 0);
  signal iarburst               : std_logic_vector(1 downto 0);
  signal iarvalid               : std_logic;
  signal iarready               : std_logic;
  constant ARI : nat_array := cumulative((
    4 => iarid'length,
    3 => iaraddr'length,
    2 => iarlen'length,
    1 => iarsize'length,
    0 => iarburst'length
  ));
  signal arpld                  : std_logic_vector(ARI(ARI'high)-1 downto 0);
  signal iarpld                 : std_logic_vector(ARI(ARI'high)-1 downto 0);
  
  -- Read response channel.
  signal irid                   : std_logic_vector(ID_WIDTH-1 downto 0);
  signal irdata                 : std_logic_vector(DATA_WIDTH-1 downto 0);
  signal irresp                 : std_logic_vector(1 downto 0);
  signal irlast                 : std_logic;
  signal irvalid                : std_logic;
  signal irready                : std_logic;
  constant RI : nat_array := cumulative((
    3 => irid'length,
    2 => irdata'length,
    1 => irresp'length,
    0 => 1 -- irlast
  ));
  signal rpld                   : std_logic_vector(RI(RI'high)-1 downto 0);
  signal irpld                  : std_logic_vector(RI(RI'high)-1 downto 0);
  
  -- Write burst address channel.
  signal xwid                   : std_logic_vector(ID_WIDTH-1 downto 0);
  signal xwaddr                 : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal xwsize                 : std_logic_vector(2 downto 0);
  signal xwlast                 : std_logic;
  signal xwvalid                : std_logic;
  signal xwready                : std_logic;
  
  -- Read burst address channel.
  signal xrid                   : std_logic_vector(ID_WIDTH-1 downto 0);
  signal xraddr                 : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal xrsize                 : std_logic_vector(2 downto 0);
  signal xrlast                 : std_logic;
  signal xrvalid                : std_logic;
  signal xrready                : std_logic;
  
  -- Synchronized handshake for xw and iw streams.
  signal swvalid                : std_logic;
  
begin
  
  -- AXI substream randomization units; these provide realistic, random timing.
  awpld(AWI(5)-1 downto AWI(4)) <= awid;
  awpld(AWI(4)-1 downto AWI(3)) <= awaddr;
  awpld(AWI(3)-1 downto AWI(2)) <= awlen;
  awpld(AWI(2)-1 downto AWI(1)) <= awsize;
  awpld(AWI(1)-1 downto AWI(0)) <= awburst;
  
  aw_randomizer: entity work.AxiSlaveMockRandomizer
    generic map (
      X_WIDTH                   => AWI(AWI'high),
      SEED                      => SEED + 1,
      X_RANDOM_SINK             => AW_RANDOM,
      X_DEPTH                   => AW_DEPTH
    )
    port map (
      clk                       => clk,
      reset                     => reset,
      sink_data                 => awpld,
      sink_valid                => awvalid,
      sink_ready                => awready,
      source_data               => iawpld,
      source_valid              => iawvalid,
      source_ready              => iawready
    );
  
  iawid     <= iawpld(AWI(5)-1 downto AWI(4));
  iawaddr   <= iawpld(AWI(4)-1 downto AWI(3));
  iawlen    <= iawpld(AWI(3)-1 downto AWI(2));
  iawsize   <= iawpld(AWI(2)-1 downto AWI(1));
  iawburst  <= iawpld(AWI(1)-1 downto AWI(0));
  
  wpld(WI(3)-1 downto WI(2)) <= wdata;
  wpld(WI(2)-1 downto WI(1)) <= wstrb;
  wpld(               WI(0)) <= wlast;
  
  w_randomizer: entity work.AxiSlaveMockRandomizer
    generic map (
      X_WIDTH                   => WI(WI'high),
      SEED                      => SEED + 2,
      X_RANDOM_SINK             => W_RANDOM,
      X_DEPTH                   => W_DEPTH
    )
    port map (
      clk                       => clk,
      reset                     => reset,
      sink_data                 => wpld,
      sink_valid                => wvalid,
      sink_ready                => wready,
      source_data               => iwpld,
      source_valid              => iwvalid,
      source_ready              => iwready
    );
  
  iwdata <= iwpld(WI(3)-1 downto WI(2));
  iwstrb <= iwpld(WI(2)-1 downto WI(1));
  iwlast <= iwpld(               WI(0));
  
  ibpld(BI(2)-1 downto BI(1)) <= ibid;
  ibpld(BI(1)-1 downto BI(0)) <= ibresp;
  
  b_randomizer: entity work.AxiSlaveMockRandomizer
    generic map (
      X_WIDTH                   => BI(BI'high),
      SEED                      => SEED + 3,
      X_RANDOM_SOURCE           => B_RANDOM,
      X_DEPTH                   => B_DEPTH
    )
    port map (
      clk                       => clk,
      reset                     => reset,
      sink_data                 => ibpld,
      sink_valid                => ibvalid,
      sink_ready                => ibready,
      source_data               => bpld,
      source_valid              => bvalid,
      source_ready              => bready
    );
  
  bid     <= bpld(BI(2)-1 downto BI(1));
  bresp   <= bpld(BI(1)-1 downto BI(0));
  
  arpld(ARI(5)-1 downto ARI(4)) <= arid;
  arpld(ARI(4)-1 downto ARI(3)) <= araddr;
  arpld(ARI(3)-1 downto ARI(2)) <= arlen;
  arpld(ARI(2)-1 downto ARI(1)) <= arsize;
  arpld(ARI(1)-1 downto ARI(0)) <= arburst;
  
  ar_randomizer: entity work.AxiSlaveMockRandomizer
    generic map (
      X_WIDTH                   => ARI(ARI'high),
      SEED                      => SEED + 1,
      X_RANDOM_SINK             => AR_RANDOM,
      X_DEPTH                   => AR_DEPTH
    )
    port map (
      clk                       => clk,
      reset                     => reset,
      sink_data                 => arpld,
      sink_valid                => arvalid,
      sink_ready                => arready,
      source_data               => iarpld,
      source_valid              => iarvalid,
      source_ready              => iarready
    );
  
  iarid     <= iarpld(ARI(5)-1 downto ARI(4));
  iaraddr   <= iarpld(ARI(4)-1 downto ARI(3));
  iarlen    <= iarpld(ARI(3)-1 downto ARI(2));
  iarsize   <= iarpld(ARI(2)-1 downto ARI(1));
  iarburst  <= iarpld(ARI(1)-1 downto ARI(0));
  
  irpld(RI(4)-1 downto RI(3)) <= irid;
  irpld(RI(3)-1 downto RI(2)) <= irdata;
  irpld(RI(2)-1 downto RI(1)) <= irresp;
  irpld(               RI(0)) <= irlast;
  
  r_randomizer: entity work.AxiSlaveMockRandomizer
    generic map (
      X_WIDTH                   => RI(RI'high),
      SEED                      => SEED + 5,
      X_RANDOM_SOURCE           => R_RANDOM,
      X_DEPTH                   => R_DEPTH
    )
    port map (
      clk                       => clk,
      reset                     => reset,
      sink_data                 => irpld,
      sink_valid                => irvalid,
      sink_ready                => irready,
      source_data               => rpld,
      source_valid              => rvalid,
      source_ready              => rready
    );
  
  rid   <= rpld(RI(4)-1 downto RI(3));
  rdata <= rpld(RI(3)-1 downto RI(2));
  rresp <= rpld(RI(2)-1 downto RI(1));
  rlast <= rpld(               RI(0));
  
  -- Address generator units. These convert the AXI (read|write) request stream
  -- into a stream of addresses and last flags that can be synchronized with
  -- the data streams.
  write_addr_gen_inst: entity work.AxiSlaveMockAddrGen
    generic map (
      ADDR_WIDTH                => ADDR_WIDTH,
      LEN_WIDTH                 => LEN_WIDTH,
      ID_WIDTH                  => ID_WIDTH
    )
    port map (
      clk                       => clk,
      reset                     => reset,
      aid                       => iawid,
      aaddr                     => iawaddr,
      alen                      => iawlen,
      asize                     => iawsize,
      aburst                    => iawburst,
      avalid                    => iawvalid,
      aready                    => iawready,
      xid                       => xwid,
      xaddr                     => xwaddr,
      xsize                     => xwsize,
      xlast                     => xwlast,
      xvalid                    => xwvalid,
      xready                    => xwready
    );
  
  read_addr_gen_inst: entity work.AxiSlaveMockAddrGen
    generic map (
      ADDR_WIDTH                => ADDR_WIDTH,
      LEN_WIDTH                 => LEN_WIDTH,
      ID_WIDTH                  => ID_WIDTH
    )
    port map (
      clk                       => clk,
      reset                     => reset,
      aid                       => iarid,
      aaddr                     => iaraddr,
      alen                      => iarlen,
      asize                     => iarsize,
      aburst                    => iarburst,
      avalid                    => iarvalid,
      aready                    => iarready,
      xid                       => xrid,
      xaddr                     => xraddr,
      xsize                     => xrsize,
      xlast                     => xrlast,
      xvalid                    => xrvalid,
      xready                    => xrready
    );
  
  -- Combine and split all the write streams. The address stream and data
  -- streams are synchronized to an internal write stream, and the response
  -- stream is output whenever iwlast is high.
  write_sync: StreamSync
    generic map (
      NUM_INPUTS                => 2,
      NUM_OUTPUTS               => 2
    )
    port map (
      clk                       => clk,
      reset                     => reset, 
      in_valid(1)               => xwvalid,
      in_valid(0)               => iwvalid,
      in_ready(1)               => xwready,
      in_ready(0)               => iwready,
      out_valid(1)              => ibvalid,
      out_valid(0)              => swvalid,
      out_ready(1)              => ibready,
      out_ready(0)              => '1',
      out_enable(1)             => iwlast,
      out_enable(0)             => '1'
    );
  
  -- Connect write response channel. Decode error reporting is currently not
  -- implemented, so the response code is hardwired to OK.
  ibid    <= xwid;
  ibresp  <= "00";
  
  -- Connect read response channel. Memory reads are handled asynchronously,
  -- so we can just tie the burst address channel handshake directly to the
  -- response channel. Decode error reporting is currently not implemented,
  -- so the response code is hardwired to OK.
  irid    <= xrid;
  irresp  <= "00";
  irlast  <= xrlast;
  irvalid <= xrvalid;
  xrready <= irready;
  
  -- Handle the memory accesses. Reads must be asynchronous from xraddr to
  -- irdata. Writes are synchronous to clk, active when swvalid is high,
  -- using xwaddr, xwsize, iwdata, and iwstrb.
  mem_proc: process is
    variable mem    : mem_state_type;
    variable addr   : mem_address_type;
    variable data   : std_logic_vector(DATA_WIDTH-1 downto 0);
    variable mask   : std_logic_vector(DATA_WIDTH/8-1 downto 0);
    variable i      : natural;
    variable l      : std.textio.line;
    variable c      : character;
  begin
    
    -- Reset the memory.
    mem_clear(mem, '0');
    if SREC_FILE /= "" then
      mem_loadSRec(mem, SREC_FILE);
    end if;
    
    handler: loop
      
      -- Wait for the next event.
      wait until rising_edge(clk) or xraddr'event or xrsize'event;
      exit handler when rising_edge(clk) and reset = '1';
      
      -- Handle writes.
      if rising_edge(clk) and swvalid = '1' then

        -- Handle application debug output.
        if std_logic_vector(resize(unsigned(xwaddr), 32)) = X"DEB00000" then
          c := character'val(to_integer(unsigned(iwdata(7 downto 0))));
          if c = LF then
            writeline(std.textio.output, l);
          else
            write(l, c);
          end if;
        end if;
        
        -- Load the address and align it to the bus width.
        addr := std_logic_vector(resize(unsigned(xwaddr), 64));
        if (DATA_WIDTH > 8) then
          addr(log2ceil(DATA_WIDTH)-4 downto 0) := (others => '0');
        end if;
        
        -- Figure out the mask, taking size into consideration.
        --  - Get the LSBs of the address indexing within the parallel bytes on
        --    the bus, giving us a byte address within the bus word.
        if (DATA_WIDTH > 8) then
          i := to_integer(unsigned(xwaddr(log2ceil(DATA_WIDTH)-4 downto 0)));
        else
          i := 0;
        end if;
        
        --  - Turn the address into a byte mask, such that the first byte
        --    selected by the current transfer is set.
        i := 2**i;
        
        --  - Multiply by the mask generated from xwsize to get the complete
        --    mask of all bytes selected in this beat.
        i := i * (2**(2**to_integer(unsigned(xwsize)))-1);
        
        --  - Combine the byte selection bits with the incoming strobe signal
        --    to get the complete mask.
        mask := iwstrb and std_logic_vector(to_unsigned(i, DATA_WIDTH/8));
        
        -- Load the data.
        data := iwdata;
        
        -- Perform the write.
        mem_write(mem, addr, data, mask);
        
      end if;
      
      -- Handle reads.
      if xraddr'event then
        
        -- Load the address and align it to the bus width.
        addr := std_logic_vector(resize(unsigned(xraddr), 64));
        if (DATA_WIDTH > 8) then
          addr(log2ceil(DATA_WIDTH)-4 downto 0) := (others => '0');
        end if;
        
        -- Perform the read.
        mem_read(mem, addr, data);
        
        -- Save the data.
        irdata <= data;
        
      end if;
      
    end loop;
    
  end process;
  
end Behavioral;

