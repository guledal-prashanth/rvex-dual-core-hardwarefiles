
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library work;
use work.Streams.all;
use work.Utils.all;
use work.Memory.all;

entity AxiSlaveMockRandomizer is
  generic (
    
    -- Width of the payload for this bus.
    X_WIDTH                     : natural;
    
    -- Randomization stuff.
    SEED                        : positive := 1;
    X_RANDOM_SINK               : boolean := false;
    X_RANDOM_SOURCE             : boolean := false;
    X_DEPTH                     : natural := 4
    
  );
  port (
    
    -- Global signals.
    clk                         : in  std_logic;
    reset                       : in  std_logic;
    
    -- Sink input.
    sink_data                   : in  std_logic_vector(X_WIDTH-1 downto 0);
    sink_valid                  : in  std_logic;
    sink_ready                  : out std_logic;
    
    -- Source output.
    source_data                 : out std_logic_vector(X_WIDTH-1 downto 0);
    source_valid                : out std_logic;
    source_ready                : in  std_logic
    
  );
end AxiSlaveMockRandomizer;

architecture Behavioral of AxiSlaveMockRandomizer is
  
  signal sink_valid_int         : std_logic;
  signal sink_ready_int         : std_logic;
  
  signal source_valid_int       : std_logic;
  signal source_ready_int       : std_logic;
  
begin
  
  random_sink_timing_gen: if X_RANDOM_SINK generate
    signal random_valid : std_logic;
    signal random_ready : std_logic;
  begin

    -- Response producer and synchronizer. These two units randomize the rate at
    -- which burst beats are generated.
    producer_inst: StreamTbProd
      generic map (
        SEED                      => SEED
      )
      port map (
        clk                       => clk,
        reset                     => reset,
        out_valid                 => random_valid,
        out_ready                 => random_ready
      );

    producer_sync: StreamSync
      generic map (
        NUM_INPUTS                => 2,
        NUM_OUTPUTS               => 1
      )
      port map (
        clk                       => clk,
        reset                     => reset,
        in_valid(1)               => random_valid,
        in_valid(0)               => sink_valid,
        in_ready(1)               => random_ready,
        in_ready(0)               => sink_ready,
        out_valid(0)              => sink_valid_int,
        out_ready(0)              => sink_ready_int
      );

  end generate;

  fast_sink_timing_gen: if not X_RANDOM_SINK generate
  begin
    sink_valid_int <= sink_valid;
    sink_ready <= sink_ready_int;
  end generate;
  
  buffer_inst: StreamBuffer
    generic map (
      MIN_DEPTH                 => X_DEPTH,
      DATA_WIDTH                => X_WIDTH
    )
    port map (
      clk                       => clk,
      reset                     => reset,
      
      in_valid                  => sink_valid_int,
      in_ready                  => sink_ready_int,
      in_data                   => sink_data,
      
      out_valid                 => source_valid_int,
      out_ready                 => source_ready_int,
      out_data                  => source_data
    );
  
  random_source_timing_gen: if X_RANDOM_SOURCE generate
    signal random_valid : std_logic;
    signal random_ready : std_logic;
  begin

    -- Response producer and synchronizer. These two units randomize the rate at
    -- which burst beats are generated.
    producer_inst: StreamTbProd
      generic map (
        SEED                      => SEED + 31415
      )
      port map (
        clk                       => clk,
        reset                     => reset,
        out_valid                 => random_valid,
        out_ready                 => random_ready
      );

    producer_sync: StreamSync
      generic map (
        NUM_INPUTS                => 2,
        NUM_OUTPUTS               => 1
      )
      port map (
        clk                       => clk,
        reset                     => reset,
        in_valid(1)               => random_valid,
        in_valid(0)               => source_valid_int,
        in_ready(1)               => random_ready,
        in_ready(0)               => source_ready_int,
        out_valid(0)              => source_valid,
        out_ready(0)              => source_ready
      );

  end generate;

  fast_source_timing_gen: if not X_RANDOM_SOURCE generate
  begin
    source_valid <= source_valid_int;
    source_ready_int <= source_ready;
  end generate;
  
end Behavioral;

