
# Compile the VHDL files.
do compile.do

# Give simulate command.
vsim -t ps -novopt -L unisim work.testbench

onerror {resume}

#Change radix to Hexadecimal#
radix hex

# Supress spam.
set NumericStdNoWarnings 1
set StdArithNoWarnings 1

# Add default wave
add wave sim:/testbench/rvex_inst/rv2sim
add wave sim:/testbench/sync_unit/*
run 70 us
