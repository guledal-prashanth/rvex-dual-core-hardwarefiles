-- r-VEX processor
-- Copyright (C) 2008-2018 by TU Delft.
-- All Rights Reserved.

-- THIS IS A LEGAL DOCUMENT, BY USING r-VEX,
-- YOU ARE AGREEING TO THESE TERMS AND CONDITIONS.

-- No portion of this work may be used by any commercial entity, or for any
-- commercial purpose, without the prior, written permission of TU Delft.
-- Nonprofit and noncommercial use is permitted as described below.

-- 1. r-VEX is provided AS IS, with no warranty of any kind, express
-- or implied. The user of the code accepts full responsibility for the
-- application of the code and the use of any results.

-- 2. Nonprofit and noncommercial use is encouraged. r-VEX may be
-- downloaded, compiled, synthesized, copied, and modified solely for nonprofit,
-- educational, noncommercial research, and noncommercial scholarship
-- purposes provided that this notice in its entirety accompanies all copies.
-- Copies of the modified software can be delivered to persons who use it
-- solely for nonprofit, educational, noncommercial research, and
-- noncommercial scholarship purposes provided that this notice in its
-- entirety accompanies all copies.

-- 3. ALL COMMERCIAL USE, AND ALL USE BY FOR PROFIT ENTITIES, IS EXPRESSLY
-- PROHIBITED WITHOUT A LICENSE FROM TU Delft (J.S.S.M.Wong@tudelft.nl).

-- 4. No nonprofit user may place any restrictions on the use of this software,
-- including as modified by the user, by any other authorized user.

-- 5. Noncommercial and nonprofit users may distribute copies of r-VEX
-- in compiled or binary form as set forth in Section 2, provided that
-- either: (A) it is accompanied by the corresponding machine-readable source
-- code, or (B) it is accompanied by a written offer, with no time limit, to
-- give anyone a machine-readable copy of the corresponding source code in
-- return for reimbursement of the cost of distribution. This written offer
-- must permit verbatim duplication by anyone, or (C) it is distributed by
-- someone who received only the executable form, and is accompanied by a
-- copy of the written offer of source code.

-- 6. r-VEX was developed by Stephan Wong, Thijs van As, Fakhar Anjam,
-- Roel Seedorf, Anthony Brandon, Jeroen van Straten. r-VEX is currently
-- maintained by TU Delft (J.S.S.M.Wong@tudelft.nl).

-- Copyright (C) 2008-2018 by TU Delft.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.common_pkg.all;
use work.utils_pkg.all;
use work.bus_addrConv_pkg.all;
use work.core_pkg.all;
use work.cache_pkg.all;

--=============================================================================
-- This package contains type definitions and constants relevant both in the
-- standalone system internally and for anything which wants to instantiate
-- the standalone system.
-------------------------------------------------------------------------------
package rvsys_cachedDualCore_pkg is
--=============================================================================
  
  -- rvex core configuration record.
  type rvex_cdc_generic_config_type is record
    
    -- Configuration for the rvex core. There are two, since this is a dualcore
    core_0                      : rvex_generic_config_type;
    core_1                      : rvex_generic_config_type;
    
    -- Cache configuration, there is always a cache. Can differ per core.
    -- Cache config 0 relates to core zero, 1 relates to core one.
    cache_config_0              : cache_generic_config_type;
    cache_config_1              : cache_generic_config_type;
    
    -- Address range for which the cache is bypassed for data accesses
    -- (peripheral space).
    cache_bypassRange           : addrRange_type;
    
    -- Depth of the memory, represented as log2(number_of_bytes). There is no imem/dmem split
    memDepthLog2B               : natural;
    
    -- Depth of the trace buffer, represented as log2(number_of_bytes).
    -- This is equal for both cores
    traceDepthLog2B             : natural;
    
    -- The following entries define the memory map as seen by the debug bus.
    debugBusMap_mem             : addrRangeAndMapping_type;
    debugBusMap_rvex0           : addrRangeAndMapping_type;
    debugBusMap_trace0          : addrRangeAndMapping_type;
    debugBusMap_rvex1           : addrRangeAndMapping_type;
    debugBusMap_trace1          : addrRangeAndMapping_type;
    
    -- Specifies whether the debug bus memory map is mutually exclusive. When
    -- set to false, bus commands can be routed to for instance both the data
    -- memory and instruction memory at once.
    debugBusMap_mutex           : boolean;
    
    -- The following entries define the memory map as seen by the rvex.
    -- CCreg is the core 0 context count register.
    -- This register is required to make sure that context 1 assigns the correct
    -- context soft ID to its contexts
    rvexDataMap_mem             : addrRangeAndMapping_type;
    rvexDataMap_ccReg           : addrRangeAndMapping_type;
    rvexDataMap_bus             : addrRangeAndMapping_type;
    
  end record;
  
  -- Default rvex core configuration.
  constant RVEX_CDC_DEFAULT_CONFIG  : rvex_cdc_generic_config_type := (
    core_0                      => RVEX_DEFAULT_CONFIG,
    core_1                      => RVEX_DEFAULT_CONFIG,
    cache_config_0              => CACHE_DEFAULT_CONFIG,
    cache_config_1              => CACHE_DEFAULT_CONFIG,
    cache_bypassRange           => addrRange(match => "1-------------------------------"),
    memDepthLog2B               => 16,
    traceDepthLog2B             => 13,
    debugBusMap_mem             => addrRangeAndMap(match => "001-----------------------------"),
    debugBusMap_rvex0           => addrRangeAndMap(match => "1100----------------------------"),
    debugBusMap_trace0          => addrRangeAndMap(match => "1101----------------------------"),
    debugBusMap_rvex1           => addrRangeAndMap(match => "1110----------------------------"),
    debugBusMap_trace1          => addrRangeAndMap(match => "1111----------------------------"),
    debugBusMap_mutex           => false,
    rvexDataMap_mem             => addrRangeAndMap(match => "00------------------------------"),
    rvexDataMap_ccReg           => addrRangeAndMap(match => "01------------------------------"),
    rvexDataMap_bus             => addrRangeAndMap(match => "1-------------------------------")
  );
  
  constant ADDR_RANGE_UNDEF     : addrRange_type := addrRange(match => "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU");
  constant ADDR_MAPPING_UNDEF   : addrRangeAndMapping_type := addrRangeAndMap(match => "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU");
  
  -- Generates a configuration for the dual core system. None of the
  -- parameters are required; just use named associations to set the parameters
  -- you want to affect, the rest of the parameters will take their value from
  -- base, which is itself set to the default configuration if not specified.
  -- To set boolean values, use 1 for true and 0 for false (-1 is used to
  -- detect when a parameter is not specified). By using this method to
  -- generate configurations, code instantiating the dual core system will be
  -- forward compatible when new configuration options are added.
  function rvex_cdc_cfg(
    base                        : rvex_cdc_generic_config_type := RVEX_CDC_DEFAULT_CONFIG;
    core_0                      : rvex_generic_config_type := RVEX_DEFAULT_CONFIG;
    core_1                      : rvex_generic_config_type := RVEX_DEFAULT_CONFIG;
    core_valid                  : boolean := false;
    cache_enable                : integer := -1;
    cache_config_0              : cache_generic_config_type := CACHE_DEFAULT_CONFIG;
    cache_config_1              : cache_generic_config_type := CACHE_DEFAULT_CONFIG;
    cache_config_valid          : boolean := false;
    cache_bypassRange           : addrRange_type := ADDR_RANGE_UNDEF;
    memDepthLog2B               : integer := -1;
    traceDepthLog2B             : integer := -1;
    debugBusMap_mem             : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF;
    debugBusMap_rvex0           : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF;
    debugBusMap_trace0          : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF;
    debugBusMap_rvex1           : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF;
    debugBusMap_trace1          : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF;
    debugBusMap_mutex           : integer := -1;
    rvexDataMap_mem             : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF;
    rvexDataMap_ccReg           : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF;
    rvexDataMap_bus             : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF
  ) return rvex_cdc_generic_config_type;
  
end rvsys_cachedDualCore_pkg;

--=============================================================================
package body rvsys_cachedDualCore_pkg is
--=============================================================================

  -- Generates a configuration for the standalone rvex system.
  function rvex_cdc_cfg(
    base                        : rvex_cdc_generic_config_type := RVEX_CDC_DEFAULT_CONFIG;
    core_0                      : rvex_generic_config_type := RVEX_DEFAULT_CONFIG;
    core_1                      : rvex_generic_config_type := RVEX_DEFAULT_CONFIG;
    core_valid                  : boolean := false;
    cache_enable                : integer := -1;
    cache_config_0              : cache_generic_config_type := CACHE_DEFAULT_CONFIG;
    cache_config_1              : cache_generic_config_type := CACHE_DEFAULT_CONFIG;
    cache_config_valid          : boolean := false;
    cache_bypassRange           : addrRange_type := ADDR_RANGE_UNDEF;
    memDepthLog2B               : integer := -1;
    traceDepthLog2B             : integer := -1;
    debugBusMap_mem             : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF;
    debugBusMap_rvex0           : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF;
    debugBusMap_trace0          : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF;
    debugBusMap_rvex1           : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF;
    debugBusMap_trace1          : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF;
    debugBusMap_mutex           : integer := -1;
    rvexDataMap_mem             : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF;
    rvexDataMap_ccReg           : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF;
    rvexDataMap_bus             : addrRangeAndMapping_type := ADDR_MAPPING_UNDEF
  ) return rvex_cdc_generic_config_type is
    variable cfg  : rvex_cdc_generic_config_type;
  begin
    cfg := base;
    if core_valid                               then cfg.core_0             := core_0;                         end if;
    if core_valid                               then cfg.core_1             := core_1;                         end if;
    if cache_config_valid                       then cfg.cache_config_0     := cache_config_0;                 end if;
    if cache_config_valid                       then cfg.cache_config_1     := cache_config_1;                 end if;
    if cache_bypassRange /= ADDR_RANGE_UNDEF    then cfg.cache_bypassRange  := cache_bypassRange;              end if;
    if memDepthLog2B >= 0                       then cfg.memDepthLog2B      := memDepthLog2B;                  end if;
    if traceDepthLog2B >= 0                     then cfg.traceDepthLog2B    := traceDepthLog2B;                end if;
    if debugBusMap_mem  /= ADDR_MAPPING_UNDEF   then cfg.debugBusMap_mem    := debugBusMap_mem;                end if;
    if debugBusMap_rvex0  /= ADDR_MAPPING_UNDEF then cfg.debugBusMap_rvex0  := debugBusMap_rvex0;              end if;
    if debugBusMap_trace0 /= ADDR_MAPPING_UNDEF then cfg.debugBusMap_trace0 := debugBusMap_trace0;             end if;
    if debugBusMap_rvex1  /= ADDR_MAPPING_UNDEF then cfg.debugBusMap_rvex1  := debugBusMap_rvex1;              end if;
    if debugBusMap_trace1 /= ADDR_MAPPING_UNDEF then cfg.debugBusMap_trace1 := debugBusMap_trace1;             end if;
    if debugBusMap_mutex /= -1                  then cfg.debugBusMap_mutex  := boolean'val(debugBusMap_mutex); end if;
    if rvexDataMap_mem  /= ADDR_MAPPING_UNDEF   then cfg.rvexDataMap_mem    := rvexDataMap_mem;                end if;
    if rvexDataMap_ccReg  /= ADDR_MAPPING_UNDEF then cfg.rvexDataMap_ccReg  := rvexDataMap_ccReg;              end if;
    if rvexDataMap_bus  /= ADDR_MAPPING_UNDEF   then cfg.rvexDataMap_bus    := rvexDataMap_bus;                end if;
    return cfg;
  end rvex_cdc_cfg;
    
end rvsys_cachedDualCore_pkg;
