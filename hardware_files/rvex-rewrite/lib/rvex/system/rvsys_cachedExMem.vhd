-- r-VEX processor
-- Copyright (C) 2008-2016 by TU Delft.
-- All Rights Reserved.

-- THIS IS A LEGAL DOCUMENT, BY USING r-VEX,
-- YOU ARE AGREEING TO THESE TERMS AND CONDITIONS.

-- No portion of this work may be used by any commercial entity, or for any
-- commercial purpose, without the prior, written permission of TU Delft.
-- Nonprofit and noncommercial use is permitted as described below.

-- 1. r-VEX is provided AS IS, with no warranty of any kind, express
-- or implied. The user of the code accepts full responsibility for the
-- application of the code and the use of any results.

-- 2. Nonprofit and noncommercial use is encouraged. r-VEX may be
-- downloaded, compiled, synthesized, copied, and modified solely for nonprofit,
-- educational, noncommercial research, and noncommercial scholarship
-- purposes provided that this notice in its entirety accompanies all copies.
-- Copies of the modified software can be delivered to persons who use it
-- solely for nonprofit, educational, noncommercial research, and
-- noncommercial scholarship purposes provided that this notice in its
-- entirety accompanies all copies.

-- 3. ALL COMMERCIAL USE, AND ALL USE BY FOR PROFIT ENTITIES, IS EXPRESSLY
-- PROHIBITED WITHOUT A LICENSE FROM TU Delft (J.S.S.M.Wong@tudelft.nl).

-- 4. No nonprofit user may place any restrictions on the use of this software,
-- including as modified by the user, by any other authorized user.

-- 5. Noncommercial and nonprofit users may distribute copies of r-VEX
-- in compiled or binary form as set forth in Section 2, provided that
-- either: (A) it is accompanied by the corresponding machine-readable source
-- code, or (B) it is accompanied by a written offer, with no time limit, to
-- give anyone a machine-readable copy of the corresponding source code in
-- return for reimbursement of the cost of distribution. This written offer
-- must permit verbatim duplication by anyone, or (C) it is distributed by
-- someone who received only the executable form, and is accompanied by a
-- copy of the written offer of source code.

-- 6. r-VEX was developed by Stephan Wong, Thijs van As, Fakhar Anjam,
-- Roel Seedorf, Anthony Brandon, Jeroen van Straten. r-VEX is currently
-- maintained by TU Delft (J.S.S.M.Wong@tudelft.nl).

-- Copyright (C) 2008-2016 by TU Delft.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

library work;
use work.common_pkg.all;
use work.utils_pkg.all;
use work.bus_pkg.all;
use work.bus_addrConv_pkg.all;
use work.core_pkg.all;
use work.rvsys_standalone_pkg.all;

--=============================================================================
-- This is the toplevel for a "standalone" rvex core. A standalone core has
-- its own local instruction memory and data memory. It has one slave bus
-- interface, which may be used to access the instruction memory, data memory
-- and debug interface of the core. It also has one master bus interface, which
-- the core will access when it does a memory operation which is out of the
-- range of the local memory.
-------------------------------------------------------------------------------
entity rvsys_cachedExMem is
--=============================================================================
  generic (
    
    -- Configuration.
    CFG                         : rvex_sa_generic_config_type := rvex_sa_cfg;
    
    -- This is used as the core index register in the global control registers.
    CORE_ID                     : natural := 0;
    
    -- Platform version tag. This is put in the global control registers of the
    -- processor.
    PLATFORM_TAG                : std_logic_vector(55 downto 0) := (others => '0');
    
    -- Initial contents for the memory.
    MEM_INIT                    : rvex_data_array := RVEX_DATA_ARRAY_NULL;
    
    -- Register consistency check configuration (see core.vhd).
    RCC_RECORD                  : string := "";
    RCC_CHECK                   : string := "";
    RCC_CTXT                    : natural := 0
    
  );
  port (
    
    ---------------------------------------------------------------------------
    -- System control
    ---------------------------------------------------------------------------
    -- Active high synchronous reset input.
    reset                       : in  std_logic;
    
    -- Clock input, registers are rising edge triggered.
    clk                         : in  std_logic;
    
    -- Active high global clock enable input.
    clkEn                       : in  std_logic;

    -- Active high cache flush input
    flushCache                  : in  std_logic;

    -- Active high flush sync unit register
    flushSyncUnit               : in  std_logic_vector(2**CFG.core.numLaneGroupsLog2 - 1 downto 0);
    
    ---------------------------------------------------------------------------
    -- Run control interface
    ---------------------------------------------------------------------------
    -- External interrupt request signal, active high.
    rctrl2rvsa_irq              : in  std_logic_vector(2**CFG.core.numContextsLog2-1 downto 0) := (others => '0');
    
    -- External interrupt identification. Guaranteed to be loaded in the trap
    -- argument register in the same clkEn'd cycle where irqAck is high.
    rctrl2rvsa_irqID            : in  rvex_address_array(2**CFG.core.numContextsLog2-1 downto 0) := (others => (others => '0'));
    
    -- External interrupt acknowledge signal, active high. Goes high for one
    -- clkEn'abled cycle.
    rvsa2rctrl_irqAck           : out std_logic_vector(2**CFG.core.numContextsLog2-1 downto 0);
    
    -- Active high run signal. When released, the context will stop running as
    -- soon as possible.
    rctrl2rvsa_run              : in  std_logic_vector(2**CFG.core.numContextsLog2-1 downto 0) := (others => '1');
    
    -- Active high idle output. This is asserted when the core is no longer
    -- doing anything.
    rvsa2rctrl_idle             : out std_logic_vector(2**CFG.core.numContextsLog2-1 downto 0);
    
    -- Active high break output. This is asserted when the core is waiting for
    -- an externally handled breakpoint, or the B flag in DCR is otherwise set.
    rvsa2rctrl_break            : out std_logic_vector(2**CFG.core.numContextsLog2-1 downto 0);
    
    -- Active high trace stall output. This can be used to stall other cores
    -- and timers simultaneously in order to be able to trace more accurately.
    rvsa2rctrl_traceStall       : out std_logic;
    
    -- Trace stall input. This just stalls all lane groups when asserted.
    rctrl2rvsa_traceStall       : in  std_logic := '0';
    
    -- Active high context reset input. When high, the context control
    -- registers (including PC, done and break flag) will be reset.
    rctrl2rvsa_reset            : in  std_logic_vector(2**CFG.core.numContextsLog2-1 downto 0) := (others => '0');
    
    -- Reset vector. When the context or the entire core is reset, the PC
    -- register will be set to this value.
    rctrl2rvsa_resetVect        : in  rvex_address_array(2**CFG.core.numContextsLog2-1 downto 0) := CFG.core.resetVectors(2**CFG.core.numContextsLog2-1 downto 0);
    
    -- Active high done output. This is asserted when the context encounters
    -- a stop syllable. Processing a stop signal also sets the BRK control
    -- register, which stops the core. This bit can be reset by issuing a core
    -- reset or by means of the debug interface.
    rvsa2rctrl_done             : out std_logic_vector(2**CFG.core.numContextsLog2-1 downto 0);
    
    ---------------------------------------------------------------------------
    -- Bus interfaces
    ---------------------------------------------------------------------------
    -- Master interface to whatever bus the rvex is connected to.
    rvsa2bus                    : out bus_mst2slv_type;
    bus2rvsa                    : in  bus_slv2mst_type;
    
    -- Debug interface.
    debug2rvsa                  : in  bus_mst2slv_type;
    rvsa2debug                  : out bus_slv2mst_type;

    -- (Data) memory interface
    rvsa2mem                    : out bus_mst2slv_type;
    mem2rvsa                    : in  bus_slv2mst_type
    
  );
end rvsys_cachedExMem;

--=============================================================================
architecture Behavioral of rvsys_cachedExMem is
--=============================================================================
  -- 
  -- The following network is instantiated when cache_enable is true.
  --
  -- . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  -- 
  --                  .-------.                          
  --                  |       |----------K--------------------- rvsa2bus
  --     rv2mem ---A--| demux |      .-----.             
  --                  |       |---E--|     |
  --                  '-------'      | arb |      
  --                  .-------.      |     |---I--------------- rvsa2mem
  --                  |       |---F--|     |      
  -- debug2rvsa ---B--| demux |      '-----'
  --                  |       |---G---------------------------- dbg2rv
  --                  |       |---M---------------------------- dbg2trace
  --                  '-------'
  -- 
  -- . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  -- 
  -- Bus A:
  signal rvexData_req           : bus_mst2slv_type;
  signal rvexData_res           : bus_slv2mst_type;
  --
  -- Bus B:
  signal debug_req              : bus_mst2slv_type;
  signal debug_res              : bus_slv2mst_type;
  --
  -- Bus E:
  signal rvexDataMem_req        : bus_mst2slv_type;
  signal rvexDataMem_res        : bus_slv2mst_type;
  --
  -- Bus F:
  signal debugDataMem_req       : bus_mst2slv_type;
  signal debugDataMem_res       : bus_slv2mst_type;
  --
  -- Bus G:
  signal debugRvex_req          : bus_mst2slv_type;
  signal debugRvex_res          : bus_slv2mst_type;
  --
  -- Bus I:
  signal dataMem_req            : bus_mst2slv_type;
  signal dataMem_res            : bus_slv2mst_type;
  --
  -- Bus K:
  signal dataBus_req            : bus_mst2slv_type;
  signal dataBus_res            : bus_slv2mst_type;
  --
  -- Bus M:
  signal debugTrace_req         : bus_mst2slv_type;
  signal debugTrace_res         : bus_slv2mst_type;
  
  -- Trace data interconnect signals between the core and the trace buffer.
  signal rv2trsink_push         : std_logic;
  signal rv2trsink_data         : rvex_byte_type;
  signal trsink2rv_busy         : std_logic;
  
--=============================================================================
begin -- architecture
--=============================================================================

  -- Check configuration.
  assert CFG.cache_enable
    report "rvsys_cachedExMem instantiated without cache_enable set; "
         & "this is illegal. Use rvsys_standalone_core instead."
    severity failure;

  -----------------------------------------------------------------------------
  -- Connect the external busses to internal signals
  -----------------------------------------------------------------------------
  -- (This is just to get the bus naming consistent.)
  rvsa2bus    <= dataBus_req;
  dataBus_res <= bus2rvsa;
  debug_req   <= debug2rvsa;
  rvsa2debug  <= debug_res;
  -- Since the debug bus has a different address for the dmem then the rvex, we need to slice off the head.
  -- We expect the user to set the dmemDepthLog2B correctly and use it to determine which bits should be overridden
  rvsa2mem    <= applyAddrMap(dataMem_req, mapConstant(rvsa2mem.address'left - CFG.dmemDepthLog2B, '0') & mapRange(CFG.dmemDepthLog2B, 0));
  dataMem_res <= mem2rvsa;
  
  -----------------------------------------------------------------------------
  -- Instantiate the rvex core and optionally the cache
  -----------------------------------------------------------------------------
    
    -- Instantiate the cached system.
  cached_core: entity work.rvsys_cachedExMem_cachedCore
  generic map (
    CFG                     => CFG,
    CORE_ID                 => CORE_ID,
    PLATFORM_TAG            => PLATFORM_TAG,
    RCC_RECORD              => RCC_RECORD,
    RCC_CHECK               => RCC_CHECK,
    RCC_CTXT                => RCC_CTXT
  )
  port map (

        -- System control.
    reset                   => reset,
    clk                     => clk,
    clkEn                   => clkEn,
    flushCache              => flushCache,
    flushSyncUnit           => flushSyncUnit,

        -- Run control interface.
    rctrl2rvsa_irq          => rctrl2rvsa_irq,
    rctrl2rvsa_irqID        => rctrl2rvsa_irqID,
    rvsa2rctrl_irqAck       => rvsa2rctrl_irqAck,
    rctrl2rvsa_run          => rctrl2rvsa_run,
    rvsa2rctrl_idle         => rvsa2rctrl_idle,
    rvsa2rctrl_break        => rvsa2rctrl_break,
    rvsa2rctrl_traceStall   => rvsa2rctrl_traceStall,
    rctrl2rvsa_traceStall   => rctrl2rvsa_traceStall,
    rctrl2rvsa_reset        => rctrl2rvsa_reset,
    rctrl2rvsa_resetVect    => rctrl2rvsa_resetVect,
    rvsa2rctrl_done         => rvsa2rctrl_done,

        -- Memory bus.
    rv2mem                  => rvexData_req,
    mem2rv                  => rvexData_res,

        -- Debug bus.
    dbg2rv                  => debugRvex_req,
    rv2dbg                  => debugRvex_res,

        -- Trace interface.
    rv2trsink_push          => rv2trsink_push,
    rv2trsink_data          => rv2trsink_data,
    trsink2rv_busy          => trsink2rv_busy

  );

  -----------------------------------------------------------------------------
  -- Instantiate the trace buffer
  -----------------------------------------------------------------------------
  -- This will be completely optimized away when the trace system is disabled
  -- in the core, because the bus cannot write to it, so nothing would be able
  -- to affect the state of the buffers.
  trace_buffer: entity work.periph_trace
    generic map (
      DEPTH_LOG2B               => CFG.traceDepthLog2B
    )
    port map (
      
      -- System control.
      reset                     => reset,
      clk                       => clk,
      clkEn                     => clkEn,
      
      -- Slave bus.
      bus2trace                 => debugTrace_req,
      trace2bus                 => debugTrace_res,
      
      -- Trace bytestream input.
      rv2trace_push             => rv2trsink_push,
      rv2trace_data             => rv2trsink_data,
      trace2rv_busy             => trsink2rv_busy
      
    );
  
  -----------------------------------------------------------------------------
  -- Instantiate the debug bus demux unit
  -----------------------------------------------------------------------------
  -- Instantiate the debug bus demuxer for the case where the instruction
  -- memory is disabled.
    debug_bus_demux_inst: entity work.bus_demux
    generic map (
      ADDRESS_MAP(0)            => CFG.debugBusMap_dmem,
      ADDRESS_MAP(1)            => CFG.debugBusMap_rvex,
      ADDRESS_MAP(2)            => CFG.debugBusMap_trace,
      MUTUALLY_EXCLUSIVE        => CFG.debugBusMap_mutex
    )
    port map (
      reset                     => reset,
      clk                       => clk,
      clkEn                     => clkEn,
      mst2demux                 => debug_req,
      demux2mst                 => debug_res,
      demux2slv(0)              => debugDataMem_req,
      demux2slv(1)              => debugRvex_req,
      demux2slv(2)              => debugTrace_req,
      slv2demux(0)              => debugDataMem_res,
      slv2demux(1)              => debugRvex_res,
      slv2demux(2)              => debugTrace_res
    );

  -----------------------------------------------------------------------------
  -- Instantiate connections between the rvex data memory ports and the
  -- external bus
  -----------------------------------------------------------------------------
  -- Instantiate the demuxing blocks.
  rvex_bus_demux_inst: entity work.bus_demux
  generic map (
    ADDRESS_MAP(0)        => CFG.rvexDataMap_bus,
    ADDRESS_MAP(1)        => CFG.rvexDataMap_dmem
  )
  port map (
    reset                 => reset,
    clk                   => clk,
    clkEn                 => clkEn,
    mst2demux             => rvexData_req,
    demux2mst             => rvexData_res,
    demux2slv(0)          => dataBus_req,
    demux2slv(1)          => rvexDataMem_req,
    slv2demux(0)          => dataBus_res,
    slv2demux(1)          => rvexDataMem_res
  );
  
  -----------------------------------------------------------------------------
  -- Data memory
  -----------------------------------------------------------------------------
  -- Instantiate an arbiter to merge rvexData and debugData
  dmem_arbiter: entity work.bus_arbiter
    generic map (
      NUM_MASTERS             => 2
    )
    port map (
      reset                   => reset,
      clk                     => clk,
      clkEn                   => clkEn,
      mst2arb(1)              => rvexDataMem_req,
      mst2arb(0)              => debugDataMem_req,
      arb2mst(1)              => rvexDataMem_res,
      arb2mst(0)              => debugDataMem_res,
      arb2slv                 => dataMem_req,
      slv2arb                 => dataMem_res
    );
end Behavioral;

