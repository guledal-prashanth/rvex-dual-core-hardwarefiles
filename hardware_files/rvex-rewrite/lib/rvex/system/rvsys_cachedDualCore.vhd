-- r-VEX processor
-- Copyright (C) 2008-2016 by TU Delft.
-- All Rights Reserved.

-- THIS IS A LEGAL DOCUMENT, BY USING r-VEX,
-- YOU ARE AGREEING TO THESE TERMS AND CONDITIONS.

-- No portion of this work may be used by any commercial entity, or for any
-- commercial purpose, without the prior, written permission of TU Delft.
-- Nonprofit and noncommercial use is permitted as described below.

-- 1. r-VEX is provided AS IS, with no warranty of any kind, express
-- or implied. The user of the code accepts full responsibility for the
-- application of the code and the use of any results.

-- 2. Nonprofit and noncommercial use is encouraged. r-VEX may be
-- downloaded, compiled, synthesized, copied, and modified solely for nonprofit,
-- educational, noncommercial research, and noncommercial scholarship
-- purposes provided that this notice in its entirety accompanies all copies.
-- Copies of the modified software can be delivered to persons who use it
-- solely for nonprofit, educational, noncommercial research, and
-- noncommercial scholarship purposes provided that this notice in its
-- entirety accompanies all copies.

-- 3. ALL COMMERCIAL USE, AND ALL USE BY FOR PROFIT ENTITIES, IS EXPRESSLY
-- PROHIBITED WITHOUT A LICENSE FROM TU Delft (J.S.S.M.Wong@tudelft.nl).

-- 4. No nonprofit user may place any restrictions on the use of this software,
-- including as modified by the user, by any other authorized user.

-- 5. Noncommercial and nonprofit users may distribute copies of r-VEX
-- in compiled or binary form as set forth in Section 2, provided that
-- either: (A) it is accompanied by the corresponding machine-readable source
-- code, or (B) it is accompanied by a written offer, with no time limit, to
-- give anyone a machine-readable copy of the corresponding source code in
-- return for reimbursement of the cost of distribution. This written offer
-- must permit verbatim duplication by anyone, or (C) it is distributed by
-- someone who received only the executable form, and is accompanied by a
-- copy of the written offer of source code.

-- 6. r-VEX was developed by Stephan Wong, Thijs van As, Fakhar Anjam,
-- Roel Seedorf, Anthony Brandon, Jeroen van Straten. r-VEX is currently
-- maintained by TU Delft (J.S.S.M.Wong@tudelft.nl).

-- Copyright (C) 2008-2016 by TU Delft.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

library work;
use work.common_pkg.all;
use work.utils_pkg.all;
use work.bus_pkg.all;
use work.bus_addrConv_pkg.all;
use work.core_pkg.all;
use work.rvsys_cachedDualCore_pkg.all;
use work.core_ctrlRegs_pkg.all;

--=============================================================================
entity rvsys_cachedDualCore is
--=============================================================================
  generic (

    -- Configuration.
    CFG                         : rvex_cdc_generic_config_type := rvex_cdc_cfg;

    -- Platform version tag. This is put in the global control registers of the
    -- processor.
    PLATFORM_TAG                : std_logic_vector(55 downto 0) := (others => '0');

    -- Register consistency check configuration (see core.vhd).
    RCC_RECORD                  : string := "";
    RCC_CHECK                   : string := "";
    RCC_CTXT                    : natural := 0

  );
  port (

    ---------------------------------------------------------------------------
    -- System control
    ---------------------------------------------------------------------------
    -- Active high synchronous reset input.
    reset                       : in  std_logic;

    -- Clock input, registers are rising edge triggered.
    clk                         : in  std_logic;

    -- Active high global clock enable input.
    clkEn                       : in  std_logic;

    -- Active high cache flush input
    flushCache                  : in  std_logic;

    -- Active high flush sync unit register
    flushSyncUnit               : in  std_logic_vector(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);

    ---------------------------------------------------------------------------
    -- Run control interface
    ---------------------------------------------------------------------------
    -- External interrupt request signal, active high.
    rctrl2rvsa_irq              : in  std_logic_vector(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0) := (others => '0');

    -- External interrupt identification. Guaranteed to be loaded in the trap
    -- argument register in the same clkEn'd cycle where irqAck is high.
    rctrl2rvsa_irqID            : in  rvex_address_array(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0) := (others => (others => '0'));

    -- External interrupt acknowledge signal, active high. Goes high for one
    -- clkEn'abled cycle.
    rvsa2rctrl_irqAck           : out std_logic_vector(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);

    -- Active high run signal. When released, the context will stop running as
    -- soon as possible.
    rctrl2rvsa_run              : in  std_logic_vector(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0) := (others => '1');

    -- Active high idle output. This is asserted when the core is no longer
    -- doing anything.
    rvsa2rctrl_idle             : out std_logic_vector(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);

    -- Active high break output. This is asserted when the core is waiting for
    -- an externally handled breakpoint, or the B flag in DCR is otherwise set.
    rvsa2rctrl_break            : out std_logic_vector(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);

    -- Active high trace stall output. This can be used to stall other cores
    -- and timers simultaneously in order to be able to trace more accurately.
    rvsa2rctrl_traceStall       : out std_logic;

    -- Trace stall input. This just stalls all lane groups when asserted.
    rctrl2rvsa_traceStall       : in  std_logic := '0';

    -- Active high context reset input. When high, the context control
    -- registers (including PC, done and break flag) will be reset.
    rctrl2rvsa_reset            : in  std_logic_vector(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0) := (others => '0');

    -- Reset vector. When the context or the entire core is reset, the PC
    -- register will be set to this value.
    rctrl2rvsa_resetVect        : in  rvex_address_array(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);

    -- Active high done output. This is asserted when the context encounters
    -- a stop syllable. Processing a stop signal also sets the BRK control
    -- register, which stops the core. This bit can be reset by issuing a core
    -- reset or by means of the debug interface.
    rvsa2rctrl_done             : out std_logic_vector(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);

    ---------------------------------------------------------------------------
    -- Bus interfaces
    ---------------------------------------------------------------------------
    -- Master interface to other periphials, such as IRQ or UART
    rvsa2bus                    : out bus_mst2slv_type;
    bus2rvsa                    : in  bus_slv2mst_type;

    -- Debug interface, runs from a debug module (UART, for example) into the rVex
    debug2rvsa                  : in  bus_mst2slv_type;
    rvsa2debug                  : out bus_slv2mst_type;

    -- Debug bus. This is connected straight to the rvex, but there are a
    -- couple write-only things added to the CR_AFF register (which is read
    -- only in the core so there's no conflicts):
    --
    --       |-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|
    -- AFF(w)|               | Sync flush    |  Data flush   | Instr. flush  |
    --       |-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|
    --
    -- Data flush: all bits high means all flush, everything else is ignored

    -- Instr. flush: same as data flush.

    -- Sync Flush: basically the same as data flush, but for the synchronization unit

    -- (Data) memory interface
    rvsa2mem                    : out bus_mst2slv_type;
    mem2rvsa                    : in  bus_slv2mst_type

  );
end rvsys_cachedDualCore;

--=============================================================================
architecture Behavioral of rvsys_cachedDualCore is
--=============================================================================
  --
  -- The following network is instantiated.
  --
  -- . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  --
  --               .-----.      .-------.
  -- core0 --C---- | arb |      |       |----------K--------------------- rvsa2bus
  --               |     |---A--| demux |      .-----.
  -- core1 --D---- | syn |      |       |---E--|     |
  --               '-----'      '-------'      | arb |
  --                            .-------.      |     |---I--------------- rvsa2mem
  --                            |       |---F--|     |
  --           debug2rvsa ---B--| demux |      '-----'
  --                            |       |---G---------------------------- dbg2rv0
  --                            |       |---H---------------------------- dbg2trace0
  --                            |       |---J---------------------------- dbg2rv1
  --                            |       |---K---------------------------- dbg2trace1
  --                            '-------'
  --
  -- . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
  --
  -- Bus A:
  signal rvexData_req             : bus_mst2slv_type;
  signal rvexData_res             : bus_slv2mst_type;
  --
  -- Bus B:
  signal debug_req                : bus_mst2slv_type;
  signal debug_res                : bus_slv2mst_type;
  --
  -- Bus C:
  signal rvexData0_req            : bus_mst2slv_array(2**CFG.core_0.numLaneGroupsLog2 - 1 downto 0);
  signal rvexData0_res            : bus_slv2mst_array(2**CFG.core_0.numLaneGroupsLog2 - 1 downto 0);
  --
  -- Bus D:
  signal rvexData1_req            : bus_mst2slv_array(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);
  signal rvexData1_res            : bus_slv2mst_array(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);
  --
  -- Bus E:
  signal rvexDataMem_req          : bus_mst2slv_type;
  signal rvexDataMem_res          : bus_slv2mst_type;
  --
  -- Bus F:
  signal debugDataMem_req         : bus_mst2slv_type;
  signal debugDataMem_res         : bus_slv2mst_type;
  --
  -- Bus G:
  signal debugRvex0_req           : bus_mst2slv_type;
  signal debugRvex0_res           : bus_slv2mst_type;
  --
  -- Bus I:
  signal dataMem_req              : bus_mst2slv_type;
  signal dataMem_res              : bus_slv2mst_type;
  --
  -- Bus K:
  signal dataBus_req              : bus_mst2slv_type;
  signal dataBus_res              : bus_slv2mst_type;
  --
  -- Bus H:
  signal debugTrace0_req          : bus_mst2slv_type;
  signal debugTrace0_res          : bus_slv2mst_type;
  --
  -- Bus J:
  signal debugRvex1_req           : bus_mst2slv_type;
  signal debugRvex1_res           : bus_slv2mst_type;
  --
  -- Bus K:
  signal debugTrace1_req          : bus_mst2slv_type;
  signal debugTrace1_res          : bus_slv2mst_type;

  -- Trace data interconnect signals between the core and the trace buffer.
  signal rv2trsink0_push          : std_logic;
  signal rv2trsink0_data          : rvex_byte_type;
  signal trsink2rv0_busy          : std_logic;

  signal rv2trsink1_push          : std_logic;
  signal rv2trsink1_data          : rvex_byte_type;
  signal trsink2rv1_busy          : std_logic;

  -- Internal tracestall signals
  signal rvsa2rctrl_traceStall_0  : std_logic;
  signal rvsa2rctrl_traceStall_1  : std_logic;

  -- Cache flush signals
  signal  dcache_invalAddr        : rvex_address_type;
  signal  dcache_invalSource      : std_logic_vector(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);
  signal  dcache_invalEnable      : std_logic;

  -- The lane that made the request
  signal merge_arb_source         : rvex_data_type;

  -- Interconnect between merger arbiter, delay unit and sync unit
  signal merge2delay_sourceless   : bus_mst2slv_type;
  signal merge2delay              : bus_mst2slv_type;
  signal delay2merge              : bus_slv2mst_type;
  signal delay2sync               : bus_mst2slv_type;
  signal sync2delay               : bus_slv2mst_type;

  -- These signals flush the dcache or icache
  signal  dcache_flush            : std_logic_vector(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);
  signal  icache_flush            : std_logic_vector(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);

  -- Cache flush generated by debug bus
  signal  dcache_flush_int        : std_logic_vector(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);
  signal  icache_flush_int        : std_logic_vector(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);

  -- Sync flush generated by debug bus
  signal  sync_flush_int          : std_logic_vector(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);
  signal  sync_flush_merge        : std_logic_vector(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);

  -- rvex_arbiter helper signals
  signal rvex_arbiter_mst2slv     : bus_mst2slv_array(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);
  signal rvex_arbiter_slv2mst     : bus_slv2mst_array(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);

  -- Context 0 context count register signals
  signal ccReg_req                : bus_mst2slv_type;
  signal ccReg_res                : bus_slv2mst_type;

--=============================================================================
begin -- architecture
--=============================================================================

  -----------------------------------------------------------------------------
  -- Connect the external busses to internal signals
  -----------------------------------------------------------------------------
  -- (This is just to get the bus naming consistent.)
  rvsa2bus    <= dataBus_req;
  dataBus_res <= bus2rvsa;
  debug_req   <= debug2rvsa;
  rvsa2debug  <= debug_res;
  -- Since the debug bus has a different address for the dmem then the rvex, we need to slice off the head.
  -- We expect the user to set the dmemDepthLog2B correctly and use it to determine which bits should be overridden
  rvsa2mem    <= applyAddrMap(dataMem_req, mapConstant(rvsa2mem.address'left - CFG.memDepthLog2B, '0') & mapRange(CFG.memDepthLog2B, 0));
  dataMem_res <= mem2rvsa;

  rvsa2rctrl_traceStall <= rvsa2rctrl_traceStall_0 or rvsa2rctrl_traceStall_1;

  rvex_arbiter_mst2slv(rvex_arbiter_mst2slv'LEFT downto 2**CFG.core_1.numLaneGroupsLog2) <= rvexData0_req;
  rvex_arbiter_mst2slv(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0) <= rvexData1_req;
  rvexData0_res <= rvex_arbiter_slv2mst(rvex_arbiter_slv2mst'LEFT downto 2**CFG.core_1.numLaneGroupsLog2);
  rvexData1_res <= rvex_arbiter_slv2mst(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0);

  -----------------------------------------------------------------------------
  -- Instantiate the rvex core
  -----------------------------------------------------------------------------

    -- Instantiate the cached systems.
  core_0: entity work.rvsys_cachedDualCore_core
  generic map (
    RCFG                    => CFG.core_0,
    CCFG                    => CFG.cache_config_0,
    CACHE_BYPASSRANGE       => CFG.cache_bypassRange,
    CORE_ID                 => 0,
    PLATFORM_TAG            => PLATFORM_TAG,
    RCC_RECORD              => RCC_RECORD,
    RCC_CHECK               => RCC_CHECK,
    RCC_CTXT                => RCC_CTXT
  )
  port map (

        -- System control.
    reset                   => reset,
    clk                     => clk,
    clkEn                   => clkEn,

        -- Run control interface.
    rctrl2rvsa_irq          => rctrl2rvsa_irq(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 2**CFG.core_1.numLaneGroupsLog2),
    rctrl2rvsa_irqID        => rctrl2rvsa_irqID(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 2**CFG.core_1.numLaneGroupsLog2),
    rvsa2rctrl_irqAck       => rvsa2rctrl_irqAck(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 2**CFG.core_1.numLaneGroupsLog2),
    rctrl2rvsa_run          => rctrl2rvsa_run(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 2**CFG.core_1.numLaneGroupsLog2),
    rvsa2rctrl_idle         => rvsa2rctrl_idle(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 2**CFG.core_1.numLaneGroupsLog2),
    rvsa2rctrl_break        => rvsa2rctrl_break(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 2**CFG.core_1.numLaneGroupsLog2),
    rvsa2rctrl_traceStall   => rvsa2rctrl_traceStall_0,
    rctrl2rvsa_traceStall   => rctrl2rvsa_traceStall,
    rctrl2rvsa_reset        => rctrl2rvsa_reset(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 2**CFG.core_1.numLaneGroupsLog2),
    rctrl2rvsa_resetVect    => rctrl2rvsa_resetVect(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 2**CFG.core_1.numLaneGroupsLog2),
    rvsa2rctrl_done         => rvsa2rctrl_done(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 2**CFG.core_1.numLaneGroupsLog2),

        -- Memory bus.
    rv2mem                  => rvexData0_req,
    mem2rv                  => rvexData0_res,

        -- Debug bus.
    dbg2rv                  => debugRvex0_req,
    rv2dbg                  => debugRvex0_res,

        -- Trace interface.
    rv2trsink_push          => rv2trsink0_push,
    rv2trsink_data          => rv2trsink0_data,
    trsink2rv_busy          => trsink2rv0_busy,

        -- Flush interface
    dcache_invalAddr        => dcache_invalAddr,
    dcache_invalSource      => dcache_invalSource(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 2**CFG.core_1.numLaneGroupsLog2),
    dcache_invalEnable      => dcache_invalEnable,
    dcache_flush            => dcache_flush(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 2**CFG.core_1.numLaneGroupsLog2),
    icache_flush            => icache_flush(2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 - 1 downto 2**CFG.core_1.numLaneGroupsLog2)

  );

  core_1: entity work.rvsys_cachedDualCore_core
  generic map (
    RCFG                    => CFG.core_1,
    CCFG                    => CFG.cache_config_1,
    CACHE_BYPASSRANGE       => CFG.cache_bypassRange,
    CORE_ID                 => 1,
    PLATFORM_TAG            => PLATFORM_TAG,
    RCC_RECORD              => RCC_RECORD,
    RCC_CHECK               => RCC_CHECK,
    RCC_CTXT                => RCC_CTXT
  )
  port map (

        -- System control.
    reset                   => reset,
    clk                     => clk,
    clkEn                   => clkEn,

        -- Run control interface.
    rctrl2rvsa_irq          => rctrl2rvsa_irq(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0),
    rctrl2rvsa_irqID        => rctrl2rvsa_irqID(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0),
    rvsa2rctrl_irqAck       => rvsa2rctrl_irqAck(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0),
    rctrl2rvsa_run          => rctrl2rvsa_run(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0),
    rvsa2rctrl_idle         => rvsa2rctrl_idle(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0),
    rvsa2rctrl_break        => rvsa2rctrl_break(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0),
    rvsa2rctrl_traceStall   => rvsa2rctrl_traceStall_1,
    rctrl2rvsa_traceStall   => rctrl2rvsa_traceStall,
    rctrl2rvsa_reset        => rctrl2rvsa_reset(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0),
    rctrl2rvsa_resetVect    => rctrl2rvsa_resetVect(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0),
    rvsa2rctrl_done         => rvsa2rctrl_done(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0),

        -- Memory bus.
    rv2mem                  => rvexData1_req,
    mem2rv                  => rvexData1_res,

        -- Debug bus.
    dbg2rv                  => debugRvex1_req,
    rv2dbg                  => debugRvex1_res,

        -- Trace interface.
    rv2trsink_push          => rv2trsink1_push,
    rv2trsink_data          => rv2trsink1_data,
    trsink2rv_busy          => trsink2rv1_busy,

        -- Flush interface
    dcache_invalAddr        => dcache_invalAddr,
    dcache_invalSource      => dcache_invalSource(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0),
    dcache_invalEnable      => dcache_invalEnable,
    dcache_flush            => dcache_flush(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0),
    icache_flush            => icache_flush(2**CFG.core_1.numLaneGroupsLog2 - 1 downto 0)

  );

  -----------------------------------------------------------------------------
  -- Instantiate the trace buffers
  -----------------------------------------------------------------------------
  -- This will be completely optimized away when the trace system is disabled
  -- in the core, because the bus cannot write to it, so nothing would be able
  -- to affect the state of the buffers.
  trace_buffer_0: entity work.periph_trace
    generic map (
      DEPTH_LOG2B               => CFG.traceDepthLog2B
    )
    port map (

      -- System control.
      reset                     => reset,
      clk                       => clk,
      clkEn                     => clkEn,

      -- Slave bus.
      bus2trace                 => debugTrace0_req,
      trace2bus                 => debugTrace0_res,

      -- Trace bytestream input.
      rv2trace_push             => rv2trsink0_push,
      rv2trace_data             => rv2trsink0_data,
      trace2rv_busy             => trsink2rv0_busy

    );

  trace_buffer_1: entity work.periph_trace
    generic map (
      DEPTH_LOG2B               => CFG.traceDepthLog2B
    )
    port map (

      -- System control.
      reset                     => reset,
      clk                       => clk,
      clkEn                     => clkEn,

      -- Slave bus.
      bus2trace                 => debugTrace1_req,
      trace2bus                 => debugTrace1_res,

      -- Trace bytestream input.
      rv2trace_push             => rv2trsink1_push,
      rv2trace_data             => rv2trsink1_data,
      trace2rv_busy             => trsink2rv1_busy

    );

  -----------------------------------------------------------------------------
  -- Handle external flush requests
  -----------------------------------------------------------------------------
  -- Snoop the debug bus to generate the cache flush signals.
  icache_flush_int <= (others => '1')
    when (debug_req.address(9 downto 2) = uint2vect(CR_AFF, 8))
    and  (debug_req.writeEnable = '1')
    and  (debug_req.writeMask(0) = '1')
    and  (debug_req.writeData(7 downto 0) = "11111111")
    else (others => '0');

  dcache_flush_int <= (others => '1')
    when (debug_req.address(9 downto 2) = uint2vect(CR_AFF, 8))
    and  (debug_req.writeEnable = '1')
    and  (debug_req.writeMask(0) = '1')
    and  (debug_req.writeData(15 downto 8) = "11111111")
    else (others => '0');

  -- Merge this signal with the other cache flush request
  icache_flush <= (others => '1') when flushCache = '1' else icache_flush_int;
  dcache_flush <= (others => '1') when flushCache = '1' else dcache_flush_int;

  -- Handle the sync unit flush requests
  sync_flush_int <= (others => '1')
    when (debug_req.address(9 downto 2) = uint2vect(CR_AFF, 8))
    and  (debug_req.writeEnable = '1')
    and  (debug_req.writeMask(0) = '1')
    and  (debug_req.writeData(23 downto 16) = "11111111")
    else (others => '0');
  -- Merge the debug flush request and the external flush request
  sync_flush_merge <= flushSyncUnit or sync_flush_int;

  -----------------------------------------------------------------------------
  -- Merge the data busses from the rVexes
  -- Also includes the synchronization unit, cache invalidation and a bus stage
  -----------------------------------------------------------------------------
  -- Also adds the sync units and controls the dcache flush signals
  rvex_arbiter: entity work.bus_arbiter
    generic map (
      NUM_MASTERS             => 2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2
    )
    port map (
      reset                   => reset,
      clk                     => clk,
      clkEn                   => clkEn,
      mst2arb                 => rvex_arbiter_mst2slv,
      arb2mst                 => rvex_arbiter_slv2mst,
      arb2slv                 => merge2delay_sourceless,
      slv2arb                 => delay2merge,

      -- Index of the master which is making the current bus request.
      arb2slv_source          => merge_arb_source );

  -- Add the source to rvexData_req
  merge2delay <= bus_setSource(merge2delay_sourceless, merge_arb_source);

  -- Add a bus stage to prevent timing issues
  delay: entity work.bus_halfStage
    port map(     reset           => reset,
                  clk             => clk,
                  clkEn           => clkEn,
                  mst2stage       => merge2delay,
                  stage2mst       => delay2merge,
                  stage2slv       => delay2sync,
                  slv2stage       => sync2delay );

  -- Add a sync unit
  sync_unit: entity work.sync_unit
    generic map(  SOURCECOUNT     => 2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 )
    port map(     clk             => clk,
                  clkEn           => clkEn,
                  reset           => reset,
                  bus_mst2slv_in  => delay2sync,
                  bus_mst2slv_out => rvexData_req,
                  bus_slv2mst_in  => rvexData_res,
                  bus_slv2mst_out => sync2delay,
                  flush           => sync_flush_merge );

  -- Add dcache invalidation signals
  -- Connect address and enable trivially.
  dcache_invalAddr   <= rvexData_req.address;
  dcache_invalEnable <= rvexData_req.writeEnable;

  -- Decode arb2slv_source to get the bus2cache_invalSource signal.
  inval_source_proc: process (rvexData_req) is
  begin
    dcache_invalSource <= (others => '0');
    -- A synchronization signal has no real source, since its own cache ignores it
    -- Therefore, make sure it is properly invalidated
    if rvexData_req.flags.synchronize = '0' then
      for laneGroup in 0 to 2**CFG.core_0.numLaneGroupsLog2 + 2**CFG.core_1.numLaneGroupsLog2 -1 loop
        if vect2uint(merge_arb_source) = laneGroup then
          dcache_invalSource(laneGroup) <= '1';
        end if;
      end loop;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Instantiate the debug bus demux unit
  -----------------------------------------------------------------------------
  -- Instantiate the debug bus demuxer for the case where the instruction
  -- memory is disabled.
    debug_bus_demux_inst: entity work.bus_demux
    generic map (
      ADDRESS_MAP(0)            => CFG.debugBusMap_mem,
      ADDRESS_MAP(1)            => CFG.debugBusMap_rvex0,
      ADDRESS_MAP(2)            => CFG.debugBusMap_trace0,
      ADDRESS_MAP(3)            => CFG.debugBusMap_rvex1,
      ADDRESS_MAP(4)            => CFG.debugBusMap_trace1,
      MUTUALLY_EXCLUSIVE        => CFG.debugBusMap_mutex
    )
    port map (
      reset                     => reset,
      clk                       => clk,
      clkEn                     => clkEn,
      mst2demux                 => debug_req,
      demux2mst                 => debug_res,
      demux2slv(0)              => debugDataMem_req,
      demux2slv(1)              => debugRvex0_req,
      demux2slv(2)              => debugTrace0_req,
      demux2slv(3)              => debugRvex1_req,
      demux2slv(4)              => debugTrace1_req,
      slv2demux(0)              => debugDataMem_res,
      slv2demux(1)              => debugRvex0_res,
      slv2demux(2)              => debugTrace0_res,
      slv2demux(3)              => debugRvex1_res,
      slv2demux(4)              => debugTrace1_res
    );

  -----------------------------------------------------------------------------
  -- Instantiate connections between the rvex data memory ports and the
  -- external bus
  -----------------------------------------------------------------------------
  -- Instantiate the demuxing blocks.
  rvex_bus_demux_inst: entity work.bus_demux
  generic map (
    ADDRESS_MAP(0)        => CFG.rvexDataMap_bus,
    ADDRESS_MAP(1)        => CFG.rvexDataMap_mem,
    ADDRESS_MAP(2)        => CFG.rvexDataMap_ccReg
  )
  port map (
    reset                 => reset,
    clk                   => clk,
    clkEn                 => clkEn,
    mst2demux             => rvexData_req,
    demux2mst             => rvexData_res,
    demux2slv(0)          => dataBus_req,
    demux2slv(1)          => rvexDataMem_req,
    demux2slv(2)          => ccReg_req,
    slv2demux(0)          => dataBus_res,
    slv2demux(1)          => rvexDataMem_res,
    slv2demux(2)          => ccReg_res
  );

  -----------------------------------------------------------------------------
  -- Memory: both the debug bus and the rVex can access the memory. Merge
  -- their requests
  -----------------------------------------------------------------------------
  -- Instantiate an arbiter to merge rvexData and debugData
  dmem_arbiter: entity work.bus_arbiter
    generic map (
      NUM_MASTERS             => 2
    )
    port map (
      reset                   => reset,
      clk                     => clk,
      clkEn                   => clkEn,
      mst2arb(1)              => rvexDataMem_req,
      mst2arb(0)              => debugDataMem_req,
      arb2mst(1)              => rvexDataMem_res,
      arb2mst(0)              => debugDataMem_res,
      arb2slv                 => dataMem_req,
      slv2arb                 => dataMem_res
    );

  -----------------------------------------------------------------------------
  -- Handle the ccReg interaction
  -----------------------------------------------------------------------------
  ccReg_handler: process(clk)
    variable resp             : bus_slv2mst_type;
  begin
    resp := BUS_SLV2MST_IDLE;
    resp.readData := std_logic_vector(to_unsigned(2**CFG.core_0.numLaneGroupsLog2, resp.readData'LENGTH));
    if rising_edge(clk) then
      if ccReg_req.writeEnable = '1' or ccReg_req.readEnable = '1' then
        resp.ack := '1';
      end if;
    end if;
    ccReg_res <= resp;
  end process;
end Behavioral;

