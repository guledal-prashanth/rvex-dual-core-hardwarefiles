library work;
use work.synchronize_pkg.all;
use work.common_pkg.all;
use work.bus_pkg.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

-- This is the toplevel for the synchronization unit. This unit is placed on the rvex bus which unifies all (ALL) level 1 caches.
-- The synhronization unit continuisly montiors the bus. Any normal read request is ignored.
-- A read request with the synchronization flag enabled is the result of a load-linked.
-- The source core/lanegroup and the request address are noted and the read happens uninterrupted.
-- A normal write is ignored.
-- A write request with the synchronization flag enabled is the result of store-conditional
-- The store is interrupted for one cycle and might even be discontinued if it does not check out

-- To make a note about the source: the signal bus_mst2slv_in.requestSource tells from which source the request came.
-- The amount of possible sources is specified in SOURCECOUNT, every source between 0 and SOURCECOUNT-1 is valid.
-- All other requestSource values are ignored.

entity sync_unit is
  generic(
            -- The amount of sources, directly related to bus_mst2slv_in.requestSource.
            SOURCECOUNT         : positive;

            -- The granularity is the size of the memory section that gets monitored, or more specifically: the amount of LSB's that is ignored when a load-linked is executed
            -- Per default this is 2, meaning that if 0x0 is load-linked, a store-conditional to 0x0, 0x1, 0x2 and 0x3 will invalidate the load-linked.
            GRANULARITY         : natural range 0 to 31 := 2 );

  port(     -- Clock signal
            clk                 : in  std_logic;

            -- Clock enable, active high
            clkEn               : in  std_logic;

            -- Reset signal, can be used to flush all internal registers.
            reset               : in  std_logic;

            -- mst2slv_in signal: comes from the arbiter. RequestSource should be set correctly.
            bus_mst2slv_in      : in  bus_mst2slv_type;

            -- mst2slv_out signal: connected to the rest of the memory hiarchy
            bus_mst2slv_out     : out bus_mst2slv_type;

            -- slv2mst_in: communication from the rest of the memory hiarchy
            bus_slv2mst_in      : in  bus_slv2mst_type;

            -- slv2mst_out: communication back to L1 caches
            bus_slv2mst_out     : out bus_slv2mst_type;
            
            -- Flush specific internal registers. Should be used to respond to interrupts on certain contexts
            flush               : in std_logic_vector(SOURCECOUNT - 1 downto 0) );
end sync_unit;

--=============================================================================
architecture Behavioral of sync_unit is
--=============================================================================

  -- Defines the state in the FSM
  type stateType is (idle, sync_allowWrite_busy, sync_allowWrite, sync_rejectWrite);

  -- Defines the current communcation back to the master (handles the bus_slv2mst_out signal)
  type slaveOutputType is (input, inputMod, busy, rject);

  -- The reject signal that will flow back to the bus master
  constant REJECTSLV2MST        : bus_slv2mst_type := (
    readData    => (others => '1'),
    fault       => '0',
    busy        => '0',
    ack         => '1'
  );
  -- The busy signal that will flow back to the bus master.
  constant BUSYSLV2MST          : bus_slv2mst_type := (
    readData    => (others => '0'),
    fault       => '0',
    busy        => '1',
    ack         => '0'
  );

  -- FSM state signals
  signal curState               : stateType := idle;
  signal nextState              : stateType := idle;

  -- If a synchronize/write (store-conditional) comes in, it is interrupted per default.
  -- This signal will alow the synchronize/write to move forward
  signal allowSyncWrite         : boolean;

  -- This signal controls bus_mst2slv_out, makes it chose between idle and input
  signal holdOutput             : boolean;

  -- Signals to the membank that it should not current address/source combination
  signal write                  : std_logic;

  -- Points to the correct membank

  -- Signals from the membank that the current source/address combination is known
  signal hit                    : std_logic;

  -- The clear signal resets all lines in all membanks that currently have a hit
  signal clear                  : std_logic;

  -- The signal that handles bus_slv2mst_out
  signal slaveOutput            : slaveOutputType;

  -- A modified signal from slave to master
  signal modifiedSlaveAnswer    : bus_slv2mst_type;

  -- Indicates if the input bus currently requests a synchronized write or read
  signal syncWriteReq           : boolean;
  signal syncReadReq            : boolean;

begin

  -- Sets up the memory banks.
  -- For abstraction purposes, there is one membank per rVex core
  -- Every membank contains an amount of lines equal to the amount of lanegroups for that core.
  mem: entity work.sync_membank
  generic map ( SOURCECOUNT => SOURCECOUNT) 
  port map    ( clk => clk,
                clkEn => clkEn,
                reset => reset,
                write => write,
                clear => clear,
                addr_in => bus_mst2slv_in.address,
                source => bus_mst2slv_in.requestSource,
                hit => hit,
                flush => flush );

  -- Combinatorial part
  syncWriteReq <= true when bus_mst2slv_in.writeEnable = '1' and bus_mst2slv_in.flags.synchronize = '1' else false;
  syncReadReq <= true when bus_mst2slv_in.readEnable = '1' and bus_mst2slv_in.flags.synchronize = '1' else false;

  -- Decides wether or not to connect mst2slv_in to mst2slv_out.
  -- We hold the output if allowSyncWrite is false and there is a synchronized write request
  -- We also hold the output if there is a synchronization write and ack is 1.
  -- If that is the case then we just finished a sync write and should not automitcally allow the next one.
  holdOutput <= (not allowSyncWrite or bus_slv2mst_in.ack = '1')
                and syncWriteReq;
  bus_mst2slv_out <= BUS_MST2SLV_IDLE when holdOutput else bus_mst2slv_in;
  -- If there was no fault but there was an ack, then strip away the readData to prevent issues
  detModSlaveMaster: process(bus_slv2mst_in, bus_slv2mst_in.fault)
    variable ret      : bus_slv2mst_type;
  begin
    ret := bus_slv2mst_in;
    if bus_slv2mst_in.fault = '0' then
      ret.readData := (others => '0');
    end if;
    modifiedSlaveAnswer <= ret;
  end process;

  -- Determination of coreSource, hit determination
  clear <= '1' when syncWriteReq and allowSyncWrite else '0';
  write <= '1' when syncReadReq else '0';

  -- Determination of slv2mst_out depends on FSM
  bus_slv2mst_out <= bus_slv2mst_in when slaveOutput = input else
                     modifiedSlaveAnswer when slaveOutput = inputMod else
                     BUSYSLV2MST when slaveOutput = busy else
                     REJECTSLV2MST when slaveOutput = rject;

  -- Finite State Machine

  -- The transition process: if clkEn is false, the FSM halts in its current state
  state_transition: process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        curState <= idle;
      elsif clkEn = '1' then
        curState <= nextState;
      end if;
    end if;
  end process;

  -- State decision process
  state_decider: process(curState, bus_slv2mst_in.busy, hit, syncWriteReq)
  begin
    nextState <= curState;
    case curState is
      when idle|sync_allowWrite =>
        if bus_slv2mst_in.busy = '0'  then
          nextState <= idle;
          if syncWriteReq then
            if hit = '1' then
              nextState <= sync_allowWrite_busy;
            else
              nextState <= sync_rejectWrite;
            end if;
          end if;
        end if;
      when sync_allowWrite_busy =>
        nextState <= sync_allowWrite;
      when sync_rejectWrite =>
        nextState <= idle;
    end case;
  end process;

  -- Output decider process
  output_decider: process(curState, hit)
  begin
    case curState is
      when idle =>
        allowSyncWrite <= false;
        slaveOutput <= input;
      when sync_allowWrite_busy =>
        allowSyncWrite <= true;
        slaveOutput <= busy;
      when sync_allowWrite =>
        allowSyncWrite <= true;
        slaveOutput <= inputMod;
      when sync_rejectWrite =>
        allowSyncWrite <= false;
        slaveOutput <= rject;
    end case;
  end process;
end Behavioral;
