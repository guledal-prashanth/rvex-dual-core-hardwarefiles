library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.common_pkg.all;
use work.synchronize_pkg.all;
use work.bus_pkg.all;

entity tb_sync_unit is
  generic ( CLOCK_PERIOD        : time );
  port ( clk                    : in  std_logic;
         test_done              : out boolean );
end tb_sync_unit;

architecture Behavioral of tb_sync_unit is
  constant SOURCECOUNT          : positive := 2;
  signal reset_1                : std_logic;
  signal mst2slv_in_1           : bus_mst2slv_type;
  signal mst2slv_out_1          : bus_mst2slv_type;
  signal slv2mst_in_1           : bus_slv2mst_type;
  signal slv2mst_out_1          : bus_slv2mst_type;
  signal test_done_1            : boolean := false;
  signal flush_1                : std_logic_vector(SOURCECOUNT - 1 downto 0) := (others => '0');
begin
  test_done <= test_done_1;
  -- Sync_unit 1
  unit_1: entity work.sync_unit
  generic map(  SOURCECOUNT => SOURCECOUNT)
  port map(     clk => clk,
                clkEn => '1',
                reset => reset_1,
                bus_mst2slv_in => mst2slv_in_1,
                bus_mst2slv_out => mst2slv_out_1,
                bus_slv2mst_in => slv2mst_in_1,
                bus_slv2mst_out => slv2mst_out_1,
                flush => flush_1 );
  test_1: process
  begin
    -- Sensible defaults
    mst2slv_in_1 <= BUS_MST2SLV_IDLE;
    slv2mst_in_1 <= BUS_SLV2MST_IDLE;
    reset_1 <= '1';
    wait for 2*CLOCK_PERIOD;
    reset_1 <= '0';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1 = BUS_MST2SLV_IDLE;
    assert slv2mst_out_1 = BUS_SLV2MST_IDLE;
    -- Common read
    mst2slv_in_1.address <= X"A0000000";
    mst2slv_in_1.readEnable <= '1';
    slv2mst_in_1.busy <= '1';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1.address = X"A0000000";
    assert mst2slv_out_1.readEnable = '1';
    assert slv2mst_out_1.busy = '1';
    slv2mst_in_1.busy <= '0';
    slv2mst_in_1.ack <= '1';
    mst2slv_in_1.readEnable <= '0';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1.address = X"A0000000";
    assert mst2slv_out_1.readEnable = '0';
    assert slv2mst_out_1.busy = '0';
    -- Common write
    mst2slv_in_1.address <= X"A0000000";
    mst2slv_in_1.writeEnable <= '1';
    slv2mst_in_1.busy <= '1';
    slv2mst_in_1.ack <= '0';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1.address = X"A0000000";
    assert mst2slv_out_1.writeEnable = '1';
    assert slv2mst_out_1.busy = '1';
    slv2mst_in_1.busy <= '0';
    slv2mst_in_1.ack <= '1';
    mst2slv_in_1.writeEnable <= '0';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1.address = X"A0000000";
    assert mst2slv_out_1.writeEnable = '0';
    assert slv2mst_out_1.busy = '0';
    -- Store conditional: should be rejected
    slv2mst_in_1.ack <= '0';
    mst2slv_in_1.address <= X"A0000000";
    mst2slv_in_1.writeEnable <= '1';
    mst2slv_in_1.flags.synchronize <= '1';
    wait for CLOCK_PERIOD;
    assert slv2mst_out_1.ack = '1';
    assert slv2mst_out_1.readData = (slv2mst_out_1.readData'RANGE => '1');
    -- Loadlinked
    mst2slv_in_1.writeEnable <= '0';
    mst2slv_in_1.address <= X"A0000000";
    mst2slv_in_1.readEnable <= '1';
    slv2mst_in_1.busy <= '1';
    mst2slv_in_1.flags.synchronize <= '1';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1.address = X"A0000000";
    assert mst2slv_out_1.readEnable = '1';
    assert slv2mst_out_1.busy = '1';
    slv2mst_in_1.busy <= '0';
    slv2mst_in_1.ack <= '1';
    wait for CLOCK_PERIOD;
    -- Store conditional
    slv2mst_in_1.ack <= '0';
    mst2slv_in_1.address <= X"A0000000";
    mst2slv_in_1.readEnable <= '0';
    mst2slv_in_1.writeEnable <= '1';
    mst2slv_in_1.flags.synchronize <= '1';
    slv2mst_in_1.busy <= '1';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1.address = X"A0000000";
    assert mst2slv_out_1.writeEnable = '1';
    assert slv2mst_out_1.busy = '1';
    slv2mst_in_1.ack <= '1';
    slv2mst_in_1.busy <= '0';
    wait for CLOCK_PERIOD;
    assert slv2mst_out_1.ack = '1';
    slv2mst_in_1.ack <= '0';
    -- Store conditional, repeated
    slv2mst_in_1.ack <= '0';
    mst2slv_in_1.address <= X"A0000000";
    mst2slv_in_1.readEnable <= '0';
    mst2slv_in_1.writeEnable <= '1';
    mst2slv_in_1.flags.synchronize <= '1';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1 = BUS_MST2SLV_IDLE;
    assert slv2mst_out_1.busy = '0';
    assert slv2mst_out_1.ack = '1';
    assert slv2mst_out_1.readData = (slv2mst_out_1.readData'RANGE => '1');
    -- Loadlinked
    mst2slv_in_1.writeEnable <= '0';
    mst2slv_in_1.address <= X"A0000004";
    mst2slv_in_1.readEnable <= '1';
    mst2slv_in_1.flags.synchronize <= '1';
    mst2slv_in_1.requestSource <= X"00000001";
    slv2mst_in_1.busy <= '1';
    slv2mst_in_1.ack <= '0';
    wait for CLOCK_PERIOD;
    assert slv2mst_out_1.busy = '1';
    assert slv2mst_out_1.ack = '0';
    slv2mst_in_1.busy <= '0';
    slv2mst_in_1.ack <= '1';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1.address = X"A0000004";
    assert slv2mst_out_1.busy = '0';
    assert slv2mst_out_1.ack = '1';
    -- Common write
    mst2slv_in_1.address <= X"A0000004";
    mst2slv_in_1.flags.synchronize <= '0';
    mst2slv_in_1.writeEnable <= '1';
    slv2mst_in_1.busy <= '1';
    slv2mst_in_1.ack <= '0';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1.address = X"A0000004";
    assert slv2mst_out_1.busy = '1';
    assert slv2mst_out_1.ack = '0';
    slv2mst_in_1.busy <= '0';
    slv2mst_in_1.ack <= '1';
    wait for CLOCK_PERIOD;
    assert slv2mst_out_1.busy = '0';
    assert slv2mst_out_1.ack = '1';
    -- Store conditional
    -- Should commit anyway, since this only responds to ll/sc
    slv2mst_in_1.ack <= '0';
    mst2slv_in_1.address <= X"A0000004";
    mst2slv_in_1.readEnable <= '0';
    mst2slv_in_1.writeEnable <= '1';
    mst2slv_in_1.flags.synchronize <= '1';
    slv2mst_in_1.busy <= '1';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1.address = X"A0000004";
    assert mst2slv_out_1.writeEnable = '1';
    assert slv2mst_out_1.busy = '1';
    slv2mst_in_1.ack <= '1';
    slv2mst_in_1.busy <= '0';
    wait for CLOCK_PERIOD;
    assert slv2mst_out_1.ack = '1';
    slv2mst_in_1.ack <= '0';
    -- Load-linked
    mst2slv_in_1.writeEnable <= '0';
    mst2slv_in_1.address <= X"A0000004";
    mst2slv_in_1.readEnable <= '1';
    mst2slv_in_1.flags.synchronize <= '1';
    mst2slv_in_1.requestSource <= X"00000000";
    slv2mst_in_1.busy <= '1';
    slv2mst_in_1.ack <= '0';
    wait for CLOCK_PERIOD;
    assert slv2mst_out_1.busy = '1';
    assert slv2mst_out_1.ack = '0';
    slv2mst_in_1.busy <= '0';
    slv2mst_in_1.ack <= '1';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1.address = X"A0000004";
    assert slv2mst_out_1.busy = '0';
    assert slv2mst_out_1.ack = '1';
    -- Load-linked
    mst2slv_in_1.writeEnable <= '0';
    mst2slv_in_1.address <= X"A0000004";
    mst2slv_in_1.readEnable <= '1';
    mst2slv_in_1.flags.synchronize <= '1';
    mst2slv_in_1.requestSource <= X"00000001";
    slv2mst_in_1.busy <= '1';
    slv2mst_in_1.ack <= '0';
    wait for CLOCK_PERIOD;
    assert slv2mst_out_1.busy = '1';
    assert slv2mst_out_1.ack = '0';
    slv2mst_in_1.busy <= '0';
    slv2mst_in_1.ack <= '1';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1.address = X"A0000004";
    assert slv2mst_out_1.busy = '0';
    assert slv2mst_out_1.ack = '1';
    -- Store Contidional
    slv2mst_in_1.ack <= '0';
    mst2slv_in_1.address <= X"A0000004";
    mst2slv_in_1.requestSource <= X"00000000";
    mst2slv_in_1.readEnable <= '0';
    mst2slv_in_1.writeEnable <= '1';
    mst2slv_in_1.flags.synchronize <= '1';
    slv2mst_in_1.busy <= '1';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1.address = X"A0000004";
    assert mst2slv_out_1.writeEnable = '1';
    assert slv2mst_out_1.busy = '1';
    slv2mst_in_1.ack <= '1';
    slv2mst_in_1.busy <= '0';
    wait for CLOCK_PERIOD;
    assert slv2mst_out_1.ack = '1';
    slv2mst_in_1.ack <= '0';
    -- Store Contidional
    slv2mst_in_1.ack <= '0';
    mst2slv_in_1.address <= X"A0000004";
    mst2slv_in_1.requestSource <= X"00000001";
    mst2slv_in_1.readEnable <= '0';
    mst2slv_in_1.writeEnable <= '1';
    mst2slv_in_1.flags.synchronize <= '1';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1 = BUS_MST2SLV_IDLE;
    assert slv2mst_out_1.busy = '0';
    assert slv2mst_out_1.ack = '1';
    assert slv2mst_out_1.readData = (slv2mst_out_1.readData'RANGE => '1');
    -- Load-linked
    mst2slv_in_1.writeEnable <= '0';
    mst2slv_in_1.address <= X"A0000004";
    mst2slv_in_1.readEnable <= '1';
    mst2slv_in_1.flags.synchronize <= '1';
    mst2slv_in_1.requestSource <= X"00000000";
    slv2mst_in_1.busy <= '1';
    slv2mst_in_1.ack <= '0';
    wait for CLOCK_PERIOD;
    assert slv2mst_out_1.busy = '1';
    assert slv2mst_out_1.ack = '0';
    slv2mst_in_1.busy <= '0';
    slv2mst_in_1.ack <= '1';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1.address = X"A0000004";
    assert slv2mst_out_1.busy = '0';
    assert slv2mst_out_1.ack = '1';
    -- Store Contidional
    slv2mst_in_1.ack <= '0';
    mst2slv_in_1.address <= X"A0000000";
    mst2slv_in_1.requestSource <= X"00000000";
    mst2slv_in_1.readEnable <= '0';
    mst2slv_in_1.writeEnable <= '1';
    mst2slv_in_1.flags.synchronize <= '1';
    slv2mst_in_1.busy <= '1';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1 = BUS_MST2SLV_IDLE;
    assert slv2mst_out_1.busy = '0';
    assert slv2mst_out_1.ack = '1';
    assert slv2mst_out_1.readData = (slv2mst_out_1.readData'RANGE => '1');
    -- Store Contidional
    slv2mst_in_1.ack <= '0';
    mst2slv_in_1.address <= X"A0000004";
    mst2slv_in_1.requestSource <= X"00000000";
    mst2slv_in_1.readEnable <= '0';
    mst2slv_in_1.writeEnable <= '1';
    mst2slv_in_1.flags.synchronize <= '1';
    slv2mst_in_1.busy <= '1';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1.address = X"A0000004";
    assert mst2slv_out_1.writeEnable = '1';
    assert slv2mst_out_1.busy = '1';
    slv2mst_in_1.ack <= '1';
    slv2mst_in_1.busy <= '0';
    wait for CLOCK_PERIOD;
    assert slv2mst_out_1.ack = '1';
    slv2mst_in_1.ack <= '0';
    mst2slv_in_1.writeEnable <= '0';
    -- Load-linked
    mst2slv_in_1.writeEnable <= '0';
    mst2slv_in_1.address <= X"A0000004";
    mst2slv_in_1.readEnable <= '1';
    mst2slv_in_1.flags.synchronize <= '1';
    mst2slv_in_1.requestSource <= X"00000000";
    slv2mst_in_1.busy <= '1';
    slv2mst_in_1.ack <= '0';
    wait for CLOCK_PERIOD;
    assert slv2mst_out_1.busy = '1';
    assert slv2mst_out_1.ack = '0';
    slv2mst_in_1.busy <= '0';
    slv2mst_in_1.ack <= '1';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1.address = X"A0000004";
    assert slv2mst_out_1.busy = '0';
    assert slv2mst_out_1.ack = '1';
    -- flush
    flush_1(0) <= '1';
    wait for CLOCK_PERIOD;
    flush_1(0) <= '0';
    -- Store Contidional
    slv2mst_in_1.ack <= '0';
    mst2slv_in_1.address <= X"A0000004";
    mst2slv_in_1.requestSource <= X"00000000";
    mst2slv_in_1.readEnable <= '0';
    mst2slv_in_1.writeEnable <= '1';
    mst2slv_in_1.flags.synchronize <= '1';
    slv2mst_in_1.busy <= '1';
    wait for CLOCK_PERIOD;
    assert mst2slv_out_1 = BUS_MST2SLV_IDLE;
    assert slv2mst_out_1.busy = '0';
    assert slv2mst_out_1.ack = '1';
    assert slv2mst_out_1.readData = (slv2mst_out_1.readData'RANGE => '1');
    mst2slv_in_1.writeEnable <= '0';



    test_done_1 <= true;
    wait;
  end process;
end Behavioral;
