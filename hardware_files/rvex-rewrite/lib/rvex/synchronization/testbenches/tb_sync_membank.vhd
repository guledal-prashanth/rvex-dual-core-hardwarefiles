library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.common_pkg.all;

entity tb_sync_membank is
  generic ( CLOCK_PERIOD        : time );
  port ( clk                    : in  std_logic;
         test_done              : out boolean );
end tb_sync_membank;

architecture Behavioral of tb_sync_membank is
 constant SOURCECOUNT           : positive := 2;
 signal reset                   : std_logic := '0';
 signal write                   : std_logic := '0';
 signal clear                   : std_logic := '0';
 signal addr_in                 : rvex_address_type := (others => '0');
 signal source                  : rvex_data_type := (others => '0');
 signal hit                     : std_logic;
 signal flush                   : std_logic_vector(SOURCECOUNT - 1 downto 0) := (others => '0');
begin
  membank: entity work.sync_membank
  generic map ( SOURCECOUNT => SOURCECOUNT )
  port map    ( clk => clk,
                clkEn => '1',
                reset => reset,
                write => write,
                clear => clear,
                addr_in => addr_in,
                source => source,
                hit => hit,
                flush => flush
              );

  test: process
  begin
    reset <= '1';
    wait for 2*CLOCK_PERIOD;
    reset <= '0';
    wait for CLOCK_PERIOD;
    assert hit = '0';
    addr_in <= X"00000001";
    source <= X"00000000";
    write <= '1';
    wait for CLOCK_PERIOD;
    write <= '0';
    assert hit = '1';
    source <= X"0000000F";
    wait for CLOCK_PERIOD;
    assert hit = '0';
    source <= X"F0000000";
    wait for CLOCK_PERIOD;
    assert hit = '1';
    addr_in <= X"F0000001";
    wait for CLOCK_PERIOD;
    assert hit = '0';
    clear <= '1';
    wait for CLOCK_PERIOD;
    assert hit = '0';
    clear <= '0';
    addr_in <= X"00000001";
    source <= X"00000000";
    wait for CLOCK_PERIOD;
    assert hit = '1';
    addr_in <= X"00000000";
    source <= X"00000000";
    write <= '1';
    wait for CLOCK_PERIOD;
    assert hit = '1';
    write <= '0';
    addr_in <= X"00000002";
    source <= X"00000001";
    wait for CLOCK_PERIOD;
    assert hit = '0';
    addr_in <= X"00000004";
    source <= X"00000002";
    wait for CLOCK_PERIOD;
    assert hit = '0';
    addr_in <= X"00000006";
    source <= X"00000003";
    wait for CLOCK_PERIOD;
    assert hit = '0';
    write <= '0';
    addr_in <= X"00000000";
    source <= X"00000000";
    wait for CLOCK_PERIOD;
    assert hit = '1';
    clear <= '1';
    wait for CLOCK_PERIOD;
    assert hit = '0';

    test_done <= true;
    wait;
  end process;
end Behavioral;
