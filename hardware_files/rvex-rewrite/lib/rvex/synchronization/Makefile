WORKDIR := work/
WORKNAMEPREFIX := work
STD := 08
WORKNAMESUFFIX := obj08.cf
MAXRUNTIME := 50000ns
GHDL := ghdl
VIEWER := gtkwave
CURDIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))/

INCLUDEFLAGS = -i --work=$(WORKNAMEPREFIX) --std=$(STD) --workdir=$(WORKDIR)
BUILDFLAGS = -m --work=$(WORKNAMEPREFIX) --std=$(STD) --workdir=$(WORKDIR)
RUNFLAGS = -r --work=$(WORKNAMEPREFIX) --std=$(STD) --workdir=$(WORKDIR)
BENCHFLAGS = --wave=$@ --ieee-asserts=disable
VIEWFLAGS = --stdout
CLEANFLAGS = --remove --workdir=$(WORKDIR) --std=$(STD)

TESTBENCH := tb_main
TESTBENCHDIR := testbenches
WORKFILE := $(WORKDIR)$(WORKNAMEPREFIX)-$(WORKNAMESUFFIX)
TARGET := $(WORKDIR)$(TESTBENCH).ghw
SRC := $(wildcard *.vhd)
SRC += $(wildcard $(TESTBENCHDIR)/*.vhd)
SRC += ../common/common_pkg.vhd
SRC += ../bus/bus_pkg.vhd
SRC += ../utils/utils_pkg.vhd

.PHONY: build clean run pre-build distclean timedRun view
.DELETE_ON_ERROR:

build: $(WORKFILE)
run: $(TARGET)

timedRun: BENCHFLAGS += --stop-time=$(MAXRUNTIME)
timedRun: $(TARGET)

$(WORKDIR) $(RVEXWORKDIR) :
	@mkdir -p $@

$(WORKFILE) : $(SRC) | $(WORKDIR)
	$(GHDL) $(INCLUDEFLAGS) $?
	$(GHDL) $(BUILDFLAGS) $(TESTBENCH)

$(TARGET) : $(WORKFILE)
	$(GHDL) $(RUNFLAGS) $(TESTBENCH) $(BENCHFLAGS)

view: $(TARGET)
	$(VIEWER) $(TARGET) &>/dev/null &

clean:
	$(GHDL) --remove --workdir=$(WORKDIR) --std=$(STD)

distclean :
	rm -rf $(WORKDIR)
	rm *.o
	rm $(TESTBENCH)
