library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_main is
  end tb_main;


architecture Behavioral of tb_main is
  constant CLOCK_PERIOD             : time := 20 ns;

  signal clk                        : std_logic                     := '1';
  signal testDone                   : boolean;

  signal syncmembankTestDone        : boolean                       := false;
  constant SYNCMEMBANKTESTENABLED   : boolean                       := true;

  signal syncunitTestDone           : boolean                       := false;
  constant SYNCUNITTESTENABLED      : boolean                       := true;
Begin

  testDone <= (syncmembankTestDone or not SYNCMEMBANKTESTENABLED) and
              (syncunitTestDone or not SYNCUNITTESTENABLED);
  -- Clk generator, simply switch flanks every half period
  clock_gen : process
  begin
    if not (testDone) then
      -- 1/2 duty cycle
      clk <= not clk;
      wait for CLOCK_PERIOD/2;
    else
      wait;
    end if;
  end process;

  syncMembank: if SYNCMEMBANKTESTENABLED generate
    syncMembank: entity work.tb_sync_membank
    generic map ( CLOCK_PERIOD => CLOCK_PERIOD )
    port map (  clk => clk,
                test_done => syncmembankTestDone );
  end generate syncMembank;

  syncUnit: if SYNCUNITTESTENABLED generate
    syncUnit: entity work.tb_sync_unit
    generic map ( CLOCK_PERIOD => CLOCK_PERIOD )
    port map (  clk => clk,
                test_done => syncunitTestDone );
  end generate syncUnit;
end Behavioral;
