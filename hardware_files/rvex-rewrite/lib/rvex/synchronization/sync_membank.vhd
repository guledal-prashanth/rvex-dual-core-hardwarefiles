library work;
use work.synchronize_pkg.all;
use work.common_pkg.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
-- This is a membank.
-- There is one membank per core and it contains one line per lanegroup in that core.
-- It will capture the required information for synchronization purposes

-- The granularity is about how big of a memory range will be reserved.
-- Basically, the lowest n bits are ignored. Default is 2, so 4 byte (=32 bit) words are reserved by this unit

entity sync_membank is
  generic ( -- The number of lanegroups in the related core.
            SOURCECOUNT         : positive;
            
            -- See sync_unit.vhd
            GRANULARITY         : natural range 0 to 31 := 2 );

  port    ( -- The clock
            clk                 : in  std_logic;

            -- The clock enable signal, active high
            clkEn               : in  std_logic;

            -- Reset signals that all currently saved data should be discarted
            reset               : in  std_logic;

            -- Write signals that the location referenced by source should be updated with current addr_in
            write               : in  std_logic;

            -- Clear signals that all locations which contain addr_in should be reset
            clear               : in  std_logic;

            -- This is the address as passing by on the rVex bus
            addr_in             : in  rvex_address_type;

            -- This is the current sender: combination of core and lanegroup. Only lanegroup is used here
            source              : in  rvex_data_type;

            -- This signal is 1 if addr_in is the value of the location referenced to by source, 0 else
            hit                 : out std_logic;
            
            -- Flush specific registers
            flush               : std_logic_vector(SOURCECOUNT - 1 downto 0) );
end sync_membank;

architecture Behavioral of sync_membank is
  -- Type definitions
  subtype addrLine is std_logic_vector(rvex_address_type'LEFT downto GRANULARITY);
  type addrLine_array is array (natural range <>) of addrLine;
  
  -- Constant definitions
  constant SOURCECOUNTL2        : natural := integer(ceil(log2(real(SOURCECOUNT))));

  -- The datastore
  signal membank                : addrLine_array(SOURCECOUNT - 1 downto 0) := (others => (others => 'X'));
  -- Every element in the datastore can be dirty
  signal dirtyline              : std_logic_vector(SOURCECOUNT - 1 downto 0) := (others => '1');
  -- This is basically the memory address for the datastore. It is derived from source
  signal membank_lane           : natural range 0 to SOURCECOUNT - 1;
  -- Every element in the datastore can hit. Hit is interpeted differently dependend on context.
  -- For the clear signal, every memory element with the correct addr is considered a hit
  -- For the hit signal, only if both source and addr_in match it is considered a hit
  signal hitLine                : std_logic_vector(SOURCECOUNT-1 downto 0);
begin

  -- Combinatorial part
  membank_lane <= to_integer(unsigned(source(SOURCECOUNTL2 - 1 downto 0)));
  hit <= hitLine(membank_lane) and not dirtyLine(membank_lane);

  -- In every cycle, every line must be read, since maybe we want to discart them all.
  hit_line: for I in 0 to SOURCECOUNT - 1 generate
    hitLine(I) <= '1' when membank(I) = addr_in(rvex_address_type'LEFT downto GRANULARITY) else '0';
  end generate hit_line;

  -- Writes are handled one write per cycle
  membank_write: process(clk)
  begin
    if rising_edge(clk) then
      if write = '1' and clkEn = '1' then
        membank(membank_lane) <= addr_in(rvex_address_type'LEFT downto GRANULARITY);
      end if;
    end if;
  end process;

  -- Handles dirtyline
  membank_dirtyline: process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        dirtyLine <= (others => '1');
      elsif clkEn = '1' then
        -- If write is 1, then we want to update a register
        if write = '1' then
          dirtyLine(membank_lane) <= '0';
        end if;
        -- Possibly override the update: clear lines if required
        for I in 0 to SOURCECOUNT - 1 loop
          if (hitLine(I) = '1' and clear = '1') or flush(I) = '1' then
            dirtyLine(I) <= '1';
          end if;
        end loop;
      end if;
    end if;
  end process;
end Behavioral;
