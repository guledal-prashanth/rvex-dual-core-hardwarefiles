library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

package synchronize_pkg is
  type core_laneGroupCount_array is array(natural range <>) of natural;
end synchronize_pkg;
