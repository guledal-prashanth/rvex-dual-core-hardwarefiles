library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- =====================================
-- The translator translates addresses. It reads every 
-- If set_address is high, addr_in is interpeted as the base addresses.
-- On the rising edge of the clock, these addresses are stored internally
-- addr_out is defined as addr_in + offset.
-- The system does not care for overflow, so 2's complement encoding will work
-- =====================================

entity address_translator is
  generic (
    ADDRESSWIDTHL2B           : natural range 5 to 8
  );
  port (
    clk                       : in  std_logic;
    set_address               : in  std_logic;
    addr_in                   : in  std_logic_vector(2**ADDRESSWIDTHL2B - 1 downto 0);
    addr_out                  : out std_logic_vector(2**ADDRESSWIDTHL2B - 1 downto 0)
  );
end address_translator;

architecture Behavioral of address_translator is
  signal store                : std_logic_vector(2**ADDRESSWIDTHL2B - 1 downto 0) := (others => '0');
begin
  addr_out <= std_logic_vector(unsigned(addr_in) + unsigned(store));

  address_storage: process(clk)
  begin
    if rising_edge(clk) then
      if set_address = '1' then
        store <= addr_in;
      end if;
    end if;
  end process;


end Behavioral;
