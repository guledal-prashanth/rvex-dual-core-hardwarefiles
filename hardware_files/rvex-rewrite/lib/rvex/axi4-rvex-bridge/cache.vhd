library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

--- ##########
-- Its a cache!
-- The AXI_WORDSIZE needs to be >= RVEX wordsize
-- The cache can handle 3 types of request: read, write and lineWrite
-- A read request is always handled within one cycle:
-- On the first rising edge the data should be stable and if hit = '0' on the second rising edge, then there was no hit.
-- Else, there was.
-- For a word_write request there is a busy flag. At the first rising edge, one sets address, data_in_rvex and write.
-- Then the next rising edge busy might be high. If it is, then the source has to keep address and data_in_rvex stable until the first rising edge on which busy is no longer high.
-- Basically the same flow goes for write_line.
--
-- It is important to not that the control signals write, read and write_line only need to be asserted for one cycle, after that the system will have changed state
-- Then it is only dependend on the data signals, which need to remain stable until busy falls
-- Read is always implied and asserting the read signal only serves to start a wait until the word you want to read becomes available
--- ##########

entity axi_4_cache is
  generic ( LINESIZELOG2B       : natural range 6 to 255;
            RVEXWORDSIZELOG2B   : natural range 5 to 255;
            AXIWORDSIZELOG2B    : natural range 5 to 255;
            ADDRWIDTHLOG2       : natural range 5 to 255;
            LINECOUNTLOG2       : natural range 0 to 64;
            BLOCKCOUNTLOG2      : natural range 0 to 255);
  port (  clk                   : in  std_logic;
          reset                 : in  std_logic;
          address               : in  std_logic_vector(2**ADDRWIDTHLOG2 - 1 downto 0);
          data_in_axi           : in  std_logic_vector(2**AXIWORDSIZELOG2B - 1 downto 0);
          data_in_rvex          : in  std_logic_vector(2**RVEXWORDSIZELOG2B - 1 downto 0);
          mask_rvex             : in  std_logic_vector(2**(RVEXWORDSIZELOG2B - 3) - 1 downto 0);
          data_out              : out std_logic_vector(2**RVEXWORDSIZELOG2B - 1 downto 0);
          ready                 : out std_logic;
          valid                 : in  std_logic;
          read                  : in  std_logic;
          write                 : in  std_logic;
          write_line            : in  std_logic;
          hit                   : out std_logic;
          busy                  : out std_logic;
          prng_newSeed          : in  std_logic_vector(127 downto 0) := (others => '0');
          prng_reseed           : in  std_logic := '0' );
end axi_4_cache;

architecture Behavioral of axi_4_cache is
  function oneHotToBinary( oneHot: std_logic_vector)
  return std_logic_vector is
  variable retval : std_logic_vector(integer(ceil(log2(real(oneHot'LENGTH)))) - 1 downto 0) := (others => '0');
  begin
    for I in oneHot'RANGE loop
      if oneHot(I) = '1' then
        retval := std_logic_vector(to_unsigned(I, retval'LENGTH)) or retval;
      end if;
    end loop;
    return retval;
  end function;

  function oneHotToBinaryLowestIndex( oneHot: std_logic_vector)
  return std_logic_vector is
  variable retval : std_logic_vector(integer(ceil(log2(real(oneHot'LENGTH)))) - 1 downto 0) := (others => '0');
  begin
    for I in oneHot'RANGE loop
      if oneHot(I) = '1' then
        retval := std_logic_vector(to_unsigned(I, retval'LENGTH));
        return retVal;
      end if;
    end loop;
    return retval;
  end function;

  function checkOneCount( oneHot: std_logic_vector)
  return natural is
  variable retval : natural := 0;
  begin
    for I in oneHot'RANGE loop
      if oneHot(I) = '1' then
        retval := retval + 1;
      end if;
    end loop;
    return retval;
  end function;

  constant RANDOM_SEED          : std_logic_vector(127 downto 0) := std_logic_vector(to_unsigned(31253120, 128));
  constant BLOCKCOUNT           : natural := 2**BLOCKCOUNTLOG2;

  subtype rvex_word is std_logic_vector(2**RVEXWORDSIZELOG2B - 1 downto 0);
  type rvex_word_array is array (natural range <>) of rvex_word;
  -- Most of the signals are needed per block, causing quite the explosion of signals
  -- All of these ".*Lines" are bundles of the same signals
  signal readyLine              : std_logic_vector(BLOCKCOUNT - 1 downto 0);
  signal writeLineLine          : std_logic_vector(BLOCKCOUNT - 1 downto 0);
  signal hitLine                : std_logic_vector(BLOCKCOUNT - 1 downto 0);
  signal busyLine               : std_logic_vector(BLOCKCOUNT - 1 downto 0);
  signal dataOutLine            : rvex_word_array(BLOCKCOUNT - 1 downto 0);

  -- Internal and control signals
  signal linewriterBusyLine     : std_logic_vector(BLOCKCOUNT - 1 downto 0);
  signal getNextEvictLine       : std_logic;
  signal prngOutput             : std_logic_vector(63 downto 0);
  signal evictBlock             : std_logic_vector(BLOCKCOUNTLOG2 - 1 downto 0);
  signal hitBlock               : std_logic_vector(BLOCKCOUNTLOG2 - 1 downto 0);
  signal linewriterBusy         : std_logic;
  signal curLineDirtyLine       : std_logic_vector(BLOCKCOUNT - 1 downto 0);

begin
  -- A number of asserts, to aid debugging
  -- At most one block can hit or the cache is in an invalid state
  assert(checkOneCount(hitLine) <= 1);

  -- The generate part. A cache is an interconnected set of cache blocks with a pseudorandomgenerator added into it.
  -- Generate all the cache blocks
  cacheBlocks: for I in 0 to BLOCKCOUNT - 1 generate
    cacheBlock: entity work.cache_block
    generic map ( LINESIZELOG2B => LINESIZELOG2B,
                  RVEXWORDSIZELOG2B => RVEXWORDSIZELOG2B,
                  AXIWORDSIZELOG2B => AXIWORDSIZELOG2B,
                  ADDRWIDTHLOG2 => ADDRWIDTHLOG2,
                  LINECOUNTLOG2 => LINECOUNTLOG2 )
    port map (  clk => clk,
                reset => reset,
                address => address,
                data_in_axi => data_in_axi,
                data_in_rvex => data_in_rvex,
                mask_rvex => mask_rvex,
                data_out => dataOutLine(I),
                ready => readyLine(I),
                valid => valid,
                read => read,
                write => write,
                write_line => writeLineLine(I),
                hit => hitLine(I),
                busy => busyLine(I),
                curLineDirty => curLineDirtyLine(I),
                linewriter_busy => linewriterBusyLine(I),
                linewriter_active => linewriterBusy );
  end generate cacheBlocks;
  -- Connect the pseudorandomgenerator
  prng: if BLOCKCOUNTLOG2 > 0 generate
    prng_1: entity work.rng_xoroshiro128plus
    generic map (
      init_seed => RANDOM_SEED
    )
    port map (
      clk => clk,
      rst => reset,
      out_ready => getNextEvictLine,
      out_data => prngOutput,
      newseed => prng_newSeed,
      reseed => prng_reseed );

  -- This process updates the PRNG after every lineWriterBusy rise
    prng_update: process(clk)
      variable lastKnownLWB       : std_logic := '0';
    begin
      if rising_edge(clk) then
        if lastKnownLWB /= linewriterBusy and lineWriterBusy = '1' then
          getNextEvictLine <= '1';
        else
          getNextEvictLine <= '0';
        end if;
        lastKnownLWB := linewriterBusy;
      end if;
    end process;

  end generate prng;
  -- Do not generate anything related to the prng if its not required
  no_prng: if BLOCKCOUNTLOG2 = 0 generate
    prngOutput <= (others => '0');
  end generate no_prng;
  -- Combinatorial logic
  evictBlock <= oneHotToBinaryLowestIndex(curLineDirtyLine) when unsigned(curLineDirtyLine) > 0 else prngOutput(prngOutput'left downto prngOutput'left - evictBlock'left);
  hitBlock <= oneHotToBinary(hitLine);
  linewriterBusy <= '1' when unsigned(linewriterBusyLine) > 0 else '0';
  ready <= '1' when unsigned(readyLine) > 0 else '0';
  hit <= '1' when unsigned(hitLine) > 0 else '0';
  busy <= '1' when unsigned(busyLine) > 0 else '0';
  -- Slight annoance: if there is exactly one block, none of the selection logic makes any sense
  -- If there is exactly one block, just connect it directly
  singleBlockCase: if BLOCKCOUNTLOG2 = 0 generate
    writeLineLine(0) <= write_line;
    data_out <= dataOutLine(0);
  end generate singleBlockCase;

  multiBlockCase: if BLOCKCOUNTLOG2 > 0 generate
    data_out <= dataOutLine(to_integer(unsigned(hitBlock)));
    inputLoop: for I in 0 to BLOCKCOUNT - 1 generate
      writeLineLine(I) <= write_line when to_unsigned(I, evictBlock'LENGTH) = unsigned(evictBlock) else '0';
    end generate inputLoop;
  end generate multiBlockCase;
end Behavioral;
