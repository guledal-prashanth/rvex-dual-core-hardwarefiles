library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common_pkg.all;
use work.bus_pkg.all;

entity read_manager is
  generic ( ADDRESSALIGNMENTMASK  : natural range 0 to 128 := 13;
            AXI_ADDRESSWIDTHLOG2B : natural range 5 to 7 := 5;
            AXI_DATAWIDTHLOG2B    : natural range 5 to 255 := 6;
            CACHE_LINECOUNTLOG2   : natural range 0 to 64 := 4;
            CACHE_LINESIZELOG2B   : natural range 6 to 255 := 8;
            CACHE_BLOCKCOUNTLOG2  : natural range 0 to 255 := 2 );
  port (  clk                   : in std_logic;
          rst                   : in std_logic;
          -- interconn: interconnect between read and write on AXI
          isBlocked             : in std_logic;
          curAddress            : out std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
          -- Read address channel signals
          maxi_araddr           : out std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
          maxi_arlen            : out std_logic_vector(3 downto 0);
          maxi_arsize           : out std_logic_vector(2 downto 0);
          maxi_arburst          : out std_logic_vector(1 downto 0);
          maxi_arcache          : out std_logic_vector(3 downto 0);
          maxi_aruser           : out std_logic_vector(4 downto 0);
          maxi_arvalid          : out std_logic;
          maxi_arready          : in  std_logic;
          -- Read data channel signals
          maxi_rdata            : in  std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0);
          maxi_rresp            : in  std_logic_vector(1 downto 0);
          maxi_rlast            : in  std_logic;
          maxi_rvalid           : in  std_logic;
          maxi_rready           : out std_logic;
          -- Signals specific to this unit
          addrIn                : in  rvex_address_type;
          writeData             : in  rvex_data_type;
          readData              : out rvex_data_type;
          writeMask             : in  rvex_mask_type;
          writeEnable           : in  std_logic;
          readEnable            : in  std_logic;
          fault                 : out std_logic;
          busy                  : out std_logic;
          -- Write busy is to signal the writer that currently the cache is updating.
          -- The axi4 writer should not ack until this signal is 0
          cache_writeBusy       : out std_logic;
          ack                   : out std_logic;
          hit                   : out std_logic;
          -- Signals for reseeding the PRNG
          prng_newSeed          : in  std_logic_vector(127 downto 0);
          prng_reseed           : in  std_logic );
end entity;

architecture Behavioral of read_manager is
  constant AXIWORDSPERLINE      : natural := 2**(CACHE_LINESIZELOG2B - AXI_DATAWIDTHLOG2B);
  constant AXIWORDSPERLINE_SLV  : std_logic_vector(3 downto 0) := std_logic_vector(to_unsigned(AXIWORDSPERLINE - 1, 4));
  constant RVEX_ADDRWIDTHLOG2B  : natural := natural(ceil(log2(real(rvex_address_type'LENGTH))));
  constant RVEX_WORDWIDTHLOG2B  : natural := natural(ceil(log2(real(rvex_data_type'LENGTH))));

  type stateType is ( readyColdState, readyHotState, readyFaultState, readyAXIState, readBusyState, readWaitState, writeWaitHitState,
                      writeBusyState, writeLineCacheInitState, writeLineDinState, writeLineWaitForDout);

  signal curState               : stateType;
  signal nextState              : stateType;
  -- Interaction signals for axi4_reader
  signal din_valid              : std_logic;
  signal din_ready              : std_logic;
  signal dout_data              : std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0);
  signal dout_result            : std_logic_vector(1 downto 0);
  signal dout_valid             : std_logic;
  signal dout_ready             : std_logic;
  -- Interaction signals for the cache
  signal cache_writeLine        : std_logic;
  signal cache_hit              : std_logic;
  signal cache_busy             : std_logic;
  signal cache_dataOut          : std_logic_vector(readData'RANGE);
  signal cache_reset            : std_logic;
  signal reset_cache            : std_logic;
  signal cache_writeEnable      : std_logic;
  signal cache_readEnable       : std_logic;
  -- Interaction signals for the address translator
  signal translator_inputAddr   : std_logic_vector(2**RVEX_ADDRWIDTHLOG2B - 1 downto 0);

  -- Internal signals
  -- readData related signals: storage unit and output selector
  signal dataOutFromCache       : std_logic;
  signal storedAXIData          : std_logic_vector(readData'RANGE)          := (others => '0');
  signal storedAXIError         : std_logic                                 := '0';
  signal storeAXIData           : std_logic;
  signal storedCacheData        : std_logic_vector(readData'RANGE)          := (others => '0');
begin
  -- Concurrent signal assignment
  readData <= storedCacheData when dataOutFromCache = '1' else storedAXIData;
  hit <= cache_hit;
  reset_cache <= cache_reset or rst;

  -- Cache
  cache: entity work.axi_4_cache
  generic map(  LINESIZELOG2B => CACHE_LINESIZELOG2B,
                RVEXWORDSIZELOG2B => RVEX_WORDWIDTHLOG2B,
                AXIWORDSIZELOG2B => AXI_DATAWIDTHLOG2B,
                ADDRWIDTHLOG2 => AXI_ADDRESSWIDTHLOG2B,
                LINECOUNTLOG2 => CACHE_LINECOUNTLOG2,
                BLOCKCOUNTLOG2 => CACHE_BLOCKCOUNTLOG2)
  port map( clk => clk,
            reset => reset_cache,
            address => addrIn,
            data_in_rvex => writeData,
            data_in_axi => dout_data,
            mask_rvex => writeMask,
            data_out => cache_dataOut,
            ready => dout_ready,
            valid => dout_valid,
            read => cache_readEnable,
            write => cache_writeEnable,
            write_line => cache_writeLine,
            hit => cache_hit,
            busy => cache_busy,
            prng_newSeed => prng_newSeed,
            prng_reseed => prng_reseed );
  -- AXI4 reader
  axi4_reader: entity work.axi4_reader
  generic map (AXI_DATAWIDTHLOG2B => AXI_DATAWIDTHLOG2B,
               AXI_ADDRESSWIDTHLOG2B => AXI_ADDRESSWIDTHLOG2B )
  port map    (clk => clk,
              rst => rst,
              read_din_addr => addrIn(addrIn'left downto 2),
              read_din_valid => din_valid,
              read_din_ready => din_ready,
              read_din_length => AXIWORDSPERLINE_SLV,
              read_dout_data => dout_data,
              read_dout_result => dout_result,
              read_dout_valid => dout_valid,
              read_dout_ready => dout_ready,
              isBlocked => isBlocked,
              curAddress => curAddress,
              maxi_araddr => maxi_araddr,
              maxi_arlen => maxi_arlen,
              maxi_arsize => maxi_arsize,
              maxi_arburst => maxi_arburst,
              maxi_arcache => maxi_arcache,
              maxi_aruser => maxi_aruser,
              maxi_arvalid => maxi_arvalid,
              maxi_arready => maxi_arready,
              maxi_rdata => maxi_rdata,
              maxi_rresp => maxi_rresp,
              maxi_rlast => maxi_rlast,
              maxi_rvalid => maxi_rvalid,
              maxi_rready => maxi_rready );
  -- process based logic
  -- This part will save the input from the AXI4 bus if required
  -- It is used to forward the data to the rVex bus whilst cache and AXI4 reader comminucation is not interrupted
  axiReadSafe: process(clk)
    variable helper_partialAssigner : natural := 0;
  begin
    if rising_edge(clk) then
      if rst = '1' then
        storedAXIData <= (others => '0');
        storedAXIError <= '0';
      elsif storeAXIData = '1' then
        if unsigned(maxi_rresp) > 0 then
          storedAXIData <= (storedAXIData'left downto maxi_rresp'left + 1 => '0') & (maxi_rresp);
          storedAXIError <= '1';
        else
          storedAXIError <= '0';
          -- Which part of the input data needs to be forwarded to the output data?
          if AXI_DATAWIDTHLOG2B > RVEX_WORDWIDTHLOG2B then
            helper_partialAssigner := to_integer(unsigned(addrIn(AXI_DATAWIDTHLOG2B - 4 downto RVEX_WORDWIDTHLOG2B - 3)));
            storedAXIData <= dout_data(helper_partialAssigner*storedAXIData'LENGTH + storedAXIData'LEFT downto helper_partialAssigner*storedAXIData'LENGTH);
          else
            storedAXIData <= dout_data;
          end if;
        end if;
      end if;
    end if;
  end process;

  -- This acts as a register for the cache data out, to split the datapath in 2
  cacheReadSafe: process(clk)
  begin
    if rising_edge(clk) then
      storedCacheData <= cache_dataOut;
    end if;
  end process;

  -- State switcher
  stateTransition: process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        curState <= readyColdState;
      else
        curState <= nextState;
      end if;
    end if;
  end process;
  -- State decider
  stateDecider: process(curState, readEnable, writeEnable, cache_hit, din_ready, dout_valid, cache_busy, storedAXIError)
  begin
    nextState <= curState;
    case curState is
      when readyColdState =>
        if readEnable = '1' then
          nextState <= readWaitState;
        elsif writeEnable = '1' then
          nextState <= writeWaitHitState;
        end if;
      when readyHotState|readyFaultState|readyAXIState =>
        if readEnable = '0' and writeEnable = '0' then
          nextState <= readyColdState;
        elsif readEnable = '1' then
          nextState <= readWaitState;
        elsif writeEnable = '1' then
          nextState <= writeWaitHitState;
        end if;
      when readWaitState =>
        if cache_hit = '1' then
          nextState <= readyHotState;
        elsif cache_busy = '1' then
          nextState <= readBusyState;
        else
          nextState <= writeLineDinState;
        end if;
      when writeWaitHitState =>
        -- Cache hit means that the block is cached
        -- Cache busy means that the block is in the pipeline and thus will need to be updated (which is also a form of a hit)
        if cache_hit = '1' or cache_busy = '1' then
          nextState <= writeBusyState;
        else
          nextState <= readyColdState;
        end if;
      when readBusyState =>
        if cache_busy = '0' then
          -- This handles the situation where a line is still being written and the data requested is from that line
          -- In that case, cache_busy will be 1 for a while and after that there is a proper hit
          if cache_hit = '1' then
            nextState <= readyHotState;
          else
            nextState <= writeLineDinState;
          end if;
        end if;
      when writeLineDinState =>
        if din_ready = '1' then
          nextState <= writeLineCacheInitState;
        end if;
      when writeLineCacheInitState =>
        nextState <= writeLineWaitForDout;
      when writeLineWaitForDout =>
        if dout_valid = '1' then
          nextState <= readyAXIState;
        end if;
      when writeBusyState =>
        if cache_busy = '0' then
          nextState <= readyColdState;
        end if;
    end case;
  end process;
  -- Output decider
  outputDecider: process(curState, readEnable, writeEnable, cache_busy)
  begin
    case curState is
      when readyColdState =>
        fault <= '0';
        busy <= '0';
        ack <= '0';
        din_valid <= '0';
        cache_writeLine <= '0';
        dataOutFromCache <= '1';
        storeAXIData <= '1';
        cache_reset <= '0';
        cache_readEnable <= readEnable;
        cache_writeEnable <= writeEnable;
        cache_writeBusy <= '0';
      when readyHotState =>
        fault <= '0';
        busy <= '0';
        ack <= '1';
        din_valid <= '0';
        cache_writeLine <= '0';
        dataOutFromCache <= '1';
        storeAXIData <= '1';
        cache_reset <= '0';
        cache_readEnable <= readEnable;
        cache_writeEnable <= writeEnable;
        cache_writeBusy <= '0';
      when readyFaultState =>
        fault <= '1';
        busy <= '0';
        ack <= '1';
        din_valid <= '0';
        cache_writeLine <= '0';
        dataOutFromCache <= '0';
        storeAXIData <= '0';
        cache_reset <= '1';
        cache_readEnable <= readEnable;
        cache_writeEnable <= writeEnable;
        cache_writeBusy <= '0';
      when readyAXIState =>
        fault <= '0';
        busy <= '0';
        ack <= '1';
        din_valid <= '0';
        cache_writeLine <= '0';
        dataOutFromCache <= '0';
        storeAXIData <= '0';
        cache_reset <= '0';
        cache_readEnable <= readEnable;
        cache_writeEnable <= writeEnable;
        cache_writeBusy <= '0';
      when readWaitState =>
        fault <= '0';
        busy <= '1';
        ack <= '0';
        din_valid <= '0';
        cache_writeLine <= '0';
        dataOutFromCache <= '1';
        storeAXIData <= '1';
        cache_reset <= '0';
        cache_readEnable <= '0';
        cache_writeEnable <= '0';
        cache_writeBusy <= '0';
      when readBusyState =>
        fault <= '0';
        busy <= '1';
        ack <= '0';
        din_valid <= '0';
        cache_writeLine <= '0';
        dataOutFromCache <= '1';
        storeAXIData <= '1';
        cache_reset <= '0';
        cache_readEnable <= '0';
        cache_writeEnable <= '0';
        cache_writeBusy <= '0';
      when writeWaitHitState =>
        fault <= '0';
        busy <= '0';
        ack <= '0';
        din_valid <= '0';
        cache_writeLine <= '0';
        dataOutFromCache <= '0';
        storeAXIData <= '1';
        cache_reset <= '0';
        cache_readEnable <= '0';
        cache_writeEnable <= '1';
        cache_writeBusy <= cache_busy;
      when writeBusyState =>
        fault <= '0';
        busy <= '0';
        ack <= '0';
        din_valid <= '0';
        cache_writeLine <= '0';
        dataOutFromCache <= '0';
        storeAXIData <= '1';
        cache_reset <= '0';
        cache_readEnable <= '0';
        cache_writeEnable <= '0';
        cache_writeBusy <= cache_busy;
      when writeLineWaitForDout =>
        fault <= '0';
        busy <= '1';
        ack <= '0';
        din_valid <= '0';
        cache_writeLine <= '0';
        dataOutFromCache <= '0';
        storeAXIData <= '1';
        cache_reset <= '0';
        cache_readEnable <= '0';
        cache_writeEnable <= '0';
        cache_writeBusy <= '0';
      when writeLineCacheInitState =>
        fault <= '0';
        busy <= '1';
        ack <= '0';
        din_valid <= '0';
        cache_writeLine <= '1';
        dataOutFromCache <= '0';
        storeAXIData <= '1';
        cache_reset <= '0';
        cache_readEnable <= '0';
        cache_writeEnable <= '0';
        cache_writeBusy <= '0';
      when writeLineDinState =>
        fault <= '0';
        busy <= '1';
        ack <= '0';
        din_valid <= '1';
        cache_writeLine <= '0';
        dataOutFromCache <= '0';
        storeAXIData <= '1';
        cache_reset <= '0';
        cache_readEnable <= '0';
        cache_writeEnable <= '0';
        cache_writeBusy <= '0';
    end case;
  end process;
end Behavioral;
