library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common_pkg.all;
use work.bus_pkg.all;

--=================
-- This entity connects the writing part of the pVEX2AXI4.
-- It can buffer one request.
-- If there is no request, this unit will accept the request and pretend like it is done immidiately.
-- On the background, the AXI4 bus actually handles the request.
-- If there currently is a request happening and a second one arrives, the second one has to wait for the first one to finish.
-- This unit will lock until that one is finished and this one can be forwarded to the AXI4 writer.
--
-- This unit hides both the AXI4_writer and an address translator
--=================

entity write_manager is
  generic (
    ADDRESSALIGNMENTMASK        : natural range 0 to 128 := 13;
    AXI_ADDRESSWIDTHLOG2B       : natural range 5 to 7 := 5;
    AXI_DATAWIDTHLOG2B          : natural range 5 to 255 := 6;
    SUPPRESSFAULT               : boolean := false
  );
  port (
    -- Standard and configuration signals
    clk                         : in  std_logic;
    rst                         : in  std_logic;
    -- AXI4 related signals
    -- Interconnection/protection signals
    isActive                    : out std_logic;
    curAddress                  : out std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
    -- Write address channel signals
    maxi_awaddr                 : out std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
    maxi_awlen                  : out std_logic_vector(3 downto 0);
    maxi_awsize                 : out std_logic_vector(2 downto 0);
    maxi_awcache                : out std_logic_vector(3 downto 0);
    maxi_awuser                 : out std_logic_vector(4 downto 0);
    maxi_awburst                : out std_logic_vector(1 downto 0);
    maxi_awvalid                : out std_logic;
    maxi_awready                : in  std_logic;
    -- Write data channel signals
    maxi_wdata                  : out std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0);
    maxi_wstrb                  : out std_logic_vector(2**(AXI_DATAWIDTHLOG2B - 3) - 1 downto 0);
    maxi_wlast                  : out std_logic;
    maxi_wvalid                 : out std_logic;
    maxi_wready                 : in  std_logic;
    --  Write response channel signals
    maxi_bresp                  : in  std_logic_vector(1 downto 0);
    maxi_bvalid                 : in  std_logic;
    maxi_bready                 : out std_logic;
    -- Actual control signals for this unit
    addrIn                      : in  std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
    writeData                   : in  rvex_data_type;
    readData                    : out rvex_data_type;
    writeMask                   : in  rvex_mask_type;
    writeEnable                 : in  std_logic;
    fault                       : out std_logic;
    busy                        : out std_logic;
    cache_writeBusy             : in  std_logic;
    ack                         : out std_logic;
    cache_hit                   : in  std_logic
  );
end write_manager;

architecture Behavioral of write_manager is
  -- Communication towards and from the axi4 writer
  signal write_din_addr         : std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
  signal write_din_data         : std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0)      := (others => '0');
  signal write_din_sizelog2b    : std_logic_vector(2 downto 0)                              := (others => '0');
  signal write_din_mask         : std_logic_vector(2**(AXI_DATAWIDTHLOG2B - 3) - 1 downto 0):= (others => '0');
  signal write_din_valid        : std_logic                                                 := '0';
  signal write_din_ready        : std_logic;
  signal write_dout_resp        : std_logic_vector(1 downto 0);
  signal write_dout_valid       : std_logic;
  signal write_dout_ready       : std_logic                                                 := '0';
  -- Communicator to the storage
  signal inputReadEnable        : boolean                                                   := false;
  -- There are two independend FSMs in here.
  -- The first one communicates with the pvex bus, the second one handles the actual read.
  -- The first one is more of a wrapper, while the second does the actual work.
  -- We call the wrapper the master and the worker the slave.
  type masterStateType is (ready, readyAck, readyFault, axiBusyState_cacheHitCheck, axiBusyState_cacheBusyCheck, cacheHitWaitState, cacheBusyState);
  type slaveStateType is (ready, dinCommunication, doutCommunication);

  signal master_nextState       : masterStateType                                           := ready;
  signal master_curState        : masterStateType                                           := ready;
  signal slave_nextState        : slaveStateType                                            := ready;
  signal slave_curState         : slaveStateType                                            := ready;

  signal slaveBusy              : boolean                                                   := false;
  signal masterBusy             : boolean                                                   := false;
  signal faultDetected          : boolean                                                   := false;
begin
  -- Basically a constant, only dependend on the exact size of rvex_data_type
  write_din_sizelog2b <= std_logic_vector(to_unsigned(integer(ceil(log2(real(rvex_data_type'LENGTH / 8)))), write_din_sizelog2b'LENGTH));

  axi4Writer: entity work.axi4_writer
  generic map (
    AXI_DATAWIDTHLOG2B => AXI_DATAWIDTHLOG2B,
    AXI_ADDRESSWIDTHLOG2B => AXI_ADDRESSWIDTHLOG2B
  )
  port map (
    clk                         => clk,
    rst                         => rst,
    write_din_addr              => write_din_addr(2**AXI_ADDRESSWIDTHLOG2B-1 downto 2),
    write_din_data              => write_din_data,
    write_din_sizelog2b         => write_din_sizelog2b,
    write_din_mask              => write_din_mask,
    write_din_valid             => write_din_valid,
    write_din_ready             => write_din_ready,
    write_dout_resp             => write_dout_resp,
    write_dout_valid            => write_dout_valid,
    write_dout_ready            => write_dout_ready,
    isActive                    => isActive,
    curAddress                  => curAddress,
    maxi_awaddr                 => maxi_awaddr,
    maxi_awlen                  => maxi_awlen,
    maxi_awsize                 => maxi_awsize,
    maxi_awcache                => maxi_awcache,
    maxi_awuser                 => maxi_awuser,
    maxi_awburst                => maxi_awburst,
    maxi_awvalid                => maxi_awvalid,
    maxi_awready                => maxi_awready,
    maxi_wdata                  => maxi_wdata,
    maxi_wstrb                  => maxi_wstrb,
    maxi_wlast                  => maxi_wlast,
    maxi_wvalid                 => maxi_wvalid,
    maxi_wready                 => maxi_wready,
    maxi_bresp                  => maxi_bresp,
    maxi_bvalid                 => maxi_bvalid,
    maxi_bready                 => maxi_bready
  );

  -- Fault detection: if the AXI4 responds with an error, do we want to do something with that information?
  faultDetector: if not SUPPRESSFAULT generate
    faultDetection: process(clk)
      variable faultSig           : boolean := false;
      variable faultAddr          : rvex_address_type := (others => '0');
      variable axiFault           : std_logic_vector(1 downto 0) := (others => '0');
    begin
      if rising_edge(clk) then
        if rst = '1' then
          faultSig := false;
        else
          if write_dout_valid = '1' and write_dout_resp /= "00" then
            faultSig := true;
            axiFault := write_dout_resp;
          elsif not slaveBusy and writeEnable = '1' then
            faultSig := false;
            faultAddr := addrIn;
          end if;
        end if;
      end if;
      faultDetected <= faultSig;
      readData <= faultAddr(faultAddr'LEFT downto 2) & axiFault;
    end process;
  end generate faultDetector;

  noFaultDetector: if SUPPRESSFAULT generate
    faultDetected <= false;
    readData <= (others => '0');
  end generate noFaultDetector;
  -- The input is stored here by the slave FSM. This machine saves the data while it is being used by the axi 4 writer
  inputStorageProcessing: process(clk)
  begin
    if rising_edge(clk) then
      if inputReadEnable then
        write_din_addr <= addrIn;
        write_din_mask <= std_logic_vector(resize(unsigned(writeMask), 2**(AXI_DATAWIDTHLOG2B - 3)));
        if integer(ceil(log2(real(rvex_data_type'LENGTH)))) < AXI_DATAWIDTHLOG2B then
          write_din_data <= (write_din_data'LEFT downto writeData'LEFT + 1 => '0') & writeData;
        else
          write_din_data <= writeData(write_din_data'RANGE);
        end if;
      end if;
    end if;

  end process;

  state_transition: process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        master_curState <= ready;
        slave_curState <= ready;
      else
        master_curState <= master_nextState;
        slave_curState <= slave_nextState;
      end if;
    end if;
  end process;

  master_stateDecider: process(slaveBusy, writeEnable, master_curState, cache_writeBusy, faultDetected, cache_hit)
  begin
    master_nextState <= master_curState;
    case master_curState is
      when readyAck|readyFault|ready =>
        master_nextState <= ready;
        if writeEnable = '1' then
          if slaveBusy then
            master_nextState <= axiBusyState_cacheHitCheck;
          else
            master_nextState <= cacheHitWaitState;
          end if;
        end if;
      when axiBusyState_cacheHitCheck =>
        master_nextState <= axiBusyState_cacheBusyCheck;
        if not slaveBusy then
          if faultDetected then
            master_nextState <= readyFault;
          elsif cache_hit = '1' or cache_writeBusy = '1' then
            master_nextState <= cacheBusyState;
          else
            master_nextState <= readyAck;
          end if;
        end if;
      when axiBusyState_cacheBusyCheck =>
        if not slaveBusy then
          if faultDetected then
            master_nextState <= readyFault;
          elsif cache_writeBusy = '1' then
            master_nextState <= cacheBusyState;
          else
            master_nextState <= readyAck;
          end if;
        end if;
      when cacheHitWaitState =>
        if cache_hit = '1' or cache_writeBusy = '1' then
          master_nextState <= cacheBusyState;
        else
          master_nextState <= readyAck;
        end if;
      when cacheBusyState =>
        if cache_writeBusy = '0' then
          if faultDetected then
            master_nextState <= readyFault;
          else
            master_nextState <= readyAck;
          end if;
        end if;
    end case;
  end process;

  master_outputDecider: process(master_curState)
  begin
    case master_curState is
      when ready =>
        ack <= '0';
        busy <= '0';
        fault <= '0';
        masterBusy <= false;
      when readyAck =>
        ack <= '1';
        busy <= '0';
        fault <= '0';
        masterBusy <= false;
      when readyFault =>
        ack <= '1';
        busy <= '0';
        fault <= '1';
        masterBusy <= false;
      when axiBusyState_cacheHitCheck|axiBusyState_cacheBusyCheck =>
        ack <= '0';
        busy <= '1';
        fault <= '0';
        masterBusy <= false;
      when cacheBusyState|cacheHitWaitState =>
        ack <= '0';
        busy <= '1';
        fault <= '0';
        masterBusy <= true;
    end case;
  end process;

  slave_stateDecider: process(slave_curState, writeEnable, write_din_ready, write_dout_valid, masterBusy)
  begin
    slave_nextState <= slave_curState;
    case slave_curState is
      when ready =>
        if writeEnable = '1' and not masterBusy then
          slave_nextState <= dinCommunication;
        end if;
      when dinCommunication =>
        if write_din_ready = '1' then
          slave_nextState <= doutCommunication;
        end if;
      when doutCommunication =>
        if write_dout_valid = '1' then
          slave_nextState <= ready;
        end if;
    end case;
  end process;

  slave_outputDecider: process(slave_curState, writeEnable, masterBusy)
  begin
    case slave_curState is
      when ready=>
        if masterBusy then
          write_din_valid <= '0';
        else
          write_din_valid <= writeEnable;
        end if;
        write_dout_ready <= '0';
        slaveBusy <= false;
        inputReadEnable <= true;
      when dinCommunication =>
        write_din_valid <= '1';
        write_dout_ready <= '0';
        slaveBusy <= true;
        inputReadEnable <= false;
      when doutCommunication =>
        write_din_valid <= '0';
        write_dout_ready <= '1';
        slaveBusy <= true;
        inputReadEnable <= true;
    end case;
  end process;
end Behavioral;



