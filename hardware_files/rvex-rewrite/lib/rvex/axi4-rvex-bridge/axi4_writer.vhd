library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--==============================
-- This entity is a thin wrapper around the actual axi4 writer,
-- intended to clear away most of the signals while preserving performance
-- On the AXI4 bus it connects to AW, W and B busses,
-- From internally it gets 2 busses, in and out.
-- The in bus contains all data, there is no address/data split like there is in axi 4
-- The out bus contains the write response.
-- There is also a signal to suspent this unit, usefull when another unit is busy on the same address.
-- There is no burst support and there is data width support for anywhere from 1 byte to DATAWIDTHLOG2B.
--
-- The sizelog2b variable is in bytes: 010 means 2**2 = 4 bytes = 32 bit
-- The address input must be aligned to 2**sizelog2b and it at least aligned to 32 bit: the 2 lsb's are assumed to be zero.
-- Data and mask need to be filled up from LSB up: if you are sending 32 bit of data, 63 downto 32 of input data is ignored.
-- Same goes for the mask.
--==============================

entity axi4_writer is
  generic (
    AXI_DATAWIDTHLOG2B      : natural range 5 to 255 := 6;
    AXI_ADDRESSWIDTHLOG2B   : natural range 5 to 255 := 5
  );
  port (
    clk                     : in std_logic;
    rst                     : in std_logic;
    -- Write din channel
    write_din_addr          : in  std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 2);
    write_din_data          : in  std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0);
    write_din_sizelog2b     : in  std_logic_vector(2 downto 0);
    write_din_mask          : in  std_logic_vector(2**(AXI_DATAWIDTHLOG2B - 3) - 1 downto 0);
    write_din_valid         : in  std_logic;
    write_din_ready         : out std_logic;
    -- write dout channel
    write_dout_resp         : out std_logic_vector(1 downto 0);
    write_dout_valid        : out std_logic;
    write_dout_ready        : in  std_logic;
    -- Interconnection/protection signals
    isActive                : out std_logic;
    curAddress              : out std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
    -- Write address channel signals
    maxi_awaddr             : out std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
    maxi_awlen              : out std_logic_vector(3 downto 0);
    maxi_awsize             : out std_logic_vector(2 downto 0);
    maxi_awcache            : out std_logic_vector(3 downto 0);
    maxi_awuser             : out std_logic_vector(4 downto 0);
    maxi_awburst            : out std_logic_vector(1 downto 0);
    maxi_awvalid            : out std_logic;
    maxi_awready            : in  std_logic;
    -- Write data channel signals
    maxi_wdata              : out std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0);
    maxi_wstrb              : out std_logic_vector(2**(AXI_DATAWIDTHLOG2B - 3) - 1 downto 0);
    maxi_wlast              : out std_logic;
    maxi_wvalid             : out std_logic;
    maxi_wready             : in  std_logic;
    --  Write response channel signals
    maxi_bresp              : in  std_logic_vector(1 downto 0);
    maxi_bvalid             : in  std_logic;
    maxi_bready             : out std_logic
  );
end axi4_writer;

ARCHITECTURE Behavioral of axi4_writer is
  type state_type is (ready, transmitting, transmitting_aw, transmitting_w, receiving);

  signal cur_state            : state_type      := ready;
  signal next_state           : state_type      := ready;
  signal storedCurAddr        : std_logic_vector(curAddress'RANGE);
  signal localMaxi_waddr      : std_logic_vector(maxi_awaddr'RANGE);
  signal updateCurAddr        : boolean         := false;

begin
  maxi_awaddr <= localMaxi_waddr;
  curAddress <= localMaxi_waddr when updateCurAddr else storedCurAddr;

  outputTranslator: process(write_din_addr, write_din_data, write_din_sizelog2b, write_din_mask, maxi_bresp)
    variable shift_modifier     : natural;
    variable prealigned_addr_in : std_logic_vector(write_din_addr'left downto 0);
  begin
    -- We require the input address to be aligned to the data write. To prevent issues, we prealign the addr_in here
    -- Since we do not know the alignment beforehand and we do not want to use the power function, we shift to the right first and then back to the left.
    -- This sets the unaligned ones to zero.
    prealigned_addr_in          := std_logic_vector(shift_left(shift_right(unsigned(write_din_addr & "00"), to_integer(unsigned(write_din_sizelog2b))), to_integer(unsigned(write_din_sizelog2b))));

    -- Because we want deterministic behaviour, we will realign the address to AXI_DATAWIDTHLOG2B.
    -- This is usually unnecessary, but the spec does not actually require the slave to realign inputs.
    localMaxi_waddr             <= prealigned_addr_in(maxi_awaddr'left downto AXI_DATAWIDTHLOG2B - 3) & (AXI_DATAWIDTHLOG2B - 4 downto 0 => '0');
    -- Burst length 1, see AXI spec p 46
    maxi_awlen                  <= (others => '0');
    -- awsize is the size of this burst (and 1 burst per transfer..)
    -- It can be no bigger then AXI_DATAWIDTHLOG2B, but can be smaller
    maxi_awsize               <= std_logic_vector(to_unsigned(AXI_DATAWIDTHLOG2B - 3, maxi_awsize'LENGTH));
    -- AWCACHE and AWUSER are specific for the slave
    -- There is a possibility to use the ZYNQ cache, this is ignored.
    maxi_awcache                <= (others => '0');
    maxi_awuser                 <= (others => '0');
    -- Does not matter, since burst size is 1, see AXI spec p 48
    -- INCR = 0x01
    maxi_awburst                <= "01";

    -- Time for the write data channel signals
    -- The shift modifier is used to move the input data to the correct output slot
    if AXI_DATAWIDTHLOG2B > 5 then
      shift_modifier            := to_integer(unsigned(prealigned_addr_in(AXI_DATAWIDTHLOG2B - 4 downto 2)))*4;
    else
      shift_modifier            := 0;
    end if;
    maxi_wdata                  <= std_logic_vector(shift_left(unsigned(write_din_data), shift_modifier*8));
    maxi_wstrb                  <= std_logic_vector(shift_left(unsigned(write_din_mask), shift_modifier));
    -- There is only one transfer, so that one is always the last
    -- See AXI4 spec p 41
    maxi_wlast                  <= '1';
    -- Pass the write response back
    write_dout_resp             <= maxi_bresp;
  end process;

  curAddressStorage: process(clk)
  begin
    if rising_edge(clk) then
      if updateCurAddr then
        storedCurAddr <= localMaxi_waddr;
      end if;
    end if;
  end process;

  state_transition : process(clk, rst)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        cur_state       <= ready;
      else
        cur_state       <= next_state;
      end if;
    end if;
  end process;

  state_decider : process(cur_state, write_din_valid, maxi_awready, maxi_wready, write_dout_ready, maxi_bvalid)
  begin
    next_state <= cur_state;
    case cur_state is
      when ready =>
        if write_din_valid = '1' then
          next_state <= transmitting;
        end if;
      when transmitting =>
        if maxi_awready = '1' and maxi_wready = '1' then
          next_state <= receiving;
        elsif maxi_awready = '1' then
          next_state <= transmitting_w;
        elsif maxi_wready = '1' then
          next_state <= transmitting_aw;
        end if;
      when transmitting_w =>
        if maxi_wready = '1' then
          next_state <= receiving;
        end if;
      when transmitting_aw =>
        if maxi_awready = '1' then
          next_state <= receiving;
        end if;
      when receiving =>
        if write_dout_ready = '1' and maxi_bvalid = '1' then
          next_state <= ready;
        end if;
    end case;
  end process;

  output_decider : process(cur_state, maxi_awready, maxi_wready, maxi_bvalid, write_dout_ready)
  begin
    case cur_state is
      when ready =>
        write_din_ready         <= '0';
        write_dout_valid        <= '0';
        isActive                <= '0';
        maxi_awvalid            <= '0';
        maxi_wvalid             <= '0';
        maxi_bready             <= '0';
        updateCurAddr           <= true;
      when transmitting =>
        write_din_ready         <= maxi_awready and maxi_wready;
        write_dout_valid        <= '0';
        isActive                <= '1';
        maxi_awvalid            <= '1';
        maxi_wvalid             <= '1';
        maxi_bready             <= '0';
        updateCurAddr           <= false;
      when transmitting_aw =>
        write_din_ready         <= maxi_awready;
        write_dout_valid        <= '0';
        isActive                <= '1';
        maxi_awvalid            <= '1';
        maxi_wvalid             <= '0';
        maxi_bready             <= '0';
        updateCurAddr           <= false;
      when transmitting_w =>
        write_din_ready         <= maxi_wready;
        write_dout_valid        <= '0';
        isActive                <= '1';
        maxi_awvalid            <= '0';
        maxi_wvalid             <= '1';
        maxi_bready             <= '0';
        updateCurAddr           <= false;
      when receiving =>
        write_din_ready         <= '0';
        write_dout_valid        <= maxi_bvalid;
        isActive                <= '1';
        maxi_awvalid            <= '0';
        maxi_wvalid             <= '0';
        maxi_bready             <= write_dout_ready;
        updateCurAddr           <= false;
    end case;
  end process;
end Behavioral;
