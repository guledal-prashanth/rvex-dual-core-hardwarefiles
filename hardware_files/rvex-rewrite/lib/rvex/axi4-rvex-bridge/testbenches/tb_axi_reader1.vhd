library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_axi_reader1 is
  generic (
    CLOCK_PERIOD                  : in time
  );
  port (
    clk                           : in  std_logic;
    test_done                     : out boolean
  );
end tb_axi_reader1;

architecture Behavioral of tb_axi_reader1 is
  constant DATAWIDTHLOG2B       : natural := 6;
  constant ADDRWIDTHLOG2B       : natural := 5;

  signal rst                    : std_logic;
  signal read_din_addr          : std_logic_vector(2**ADDRWIDTHLOG2B - 1 downto 2);
  signal read_din_valid         : std_logic;
  signal read_din_length        : std_logic_vector(3 downto 0);
  signal read_din_ready         : std_logic;
  signal read_dout_data         : std_logic_vector(2**DATAWIDTHLOG2B - 1 downto 0);
  signal read_dout_result       : std_logic_vector(1 downto 0);
  signal read_dout_valid        : std_logic;
  signal read_dout_ready        : std_logic;
  signal isBlocked              : std_logic;
  signal maxi_araddr            : std_logic_vector(2**ADDRWIDTHLOG2B - 1 downto 0);
  signal maxi_arlen             : std_logic_vector(3 downto 0);
  signal maxi_arsize            : std_logic_vector(2 downto 0);
  signal maxi_arburst           : std_logic_vector(1 downto 0);
  signal maxi_arcache           : std_logic_vector(3 downto 0);
  signal maxi_aruser            : std_logic_vector(4 downto 0);
  signal maxi_arvalid           : std_logic;
  signal maxi_arready           : std_logic;
  signal maxi_rdata             : std_logic_vector(2**DATAWIDTHLOG2B - 1 downto 0);
  signal maxi_rresp             : std_logic_vector(1 downto 0);
  signal maxi_rlast             : std_logic;
  signal maxi_rvalid            : std_logic;
  signal maxi_rready            : std_logic;

begin
  axi4_read_1: entity work.axi4_reader
  generic map (
    AXI_DATAWIDTHLOG2B          => DATAWIDTHLOG2B,
    AXI_ADDRESSwIDTHlOG2B       => ADDRWIDTHLOG2B
  )
  port map (
    clk                         =>  clk,
    rst                         =>  rst,
    read_din_addr               =>  read_din_addr,
    read_din_valid              =>  read_din_valid,
    read_din_length             =>  read_din_length,
    read_din_ready              =>  read_din_ready,
    read_dout_data              =>  read_dout_data,
    read_dout_result            =>  read_dout_result,
    read_dout_valid             =>  read_dout_valid,
    read_dout_ready             =>  read_dout_ready,
    isBlocked                   =>  isBlocked,
    maxi_araddr                 =>  maxi_araddr,
    maxi_arlen                  =>  maxi_arlen,
    maxi_arsize                 =>  maxi_arsize,
    maxi_arburst                =>  maxi_arburst,
    maxi_arcache                =>  maxi_arcache,
    maxi_aruser                 =>  maxi_aruser,
    maxi_arvalid                =>  maxi_arvalid,
    maxi_arready                =>  maxi_arready,
    maxi_rdata                  =>  maxi_rdata,
    maxi_rresp                  =>  maxi_rresp,
    maxi_rlast                  =>  maxi_rlast,
    maxi_rvalid                 =>  maxi_rvalid,
    maxi_rready                 =>  maxi_rready
  );

  test: process
  begin
    test_done <= false;
    -- Assert reset, sensible defaults
    rst <= '1';
    read_din_addr <= (others => '0');
    read_din_valid <= '0';
    read_din_length <= (others => '0');
    read_dout_ready <= '0';
    isBlocked <= '0';
    maxi_arready <= '0';
    maxi_rdata <= (others => '0');
    maxi_rresp <= (others => '0');
    maxi_rlast <= '0';
    maxi_rvalid <= '0';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert read_din_ready = '0' severity error;
    assert read_dout_data = (read_dout_data'RANGE => '0') severity error;
    assert read_dout_result = (read_dout_result'RANGE => '0') severity error;
    assert read_dout_valid = '0' severity error;
    assert maxi_araddr = (maxi_araddr'RANGE => '0') severity error;
    assert maxi_arlen = (maxi_arlen'RANGE => '0') severity error;
    assert maxi_arsize = "011" severity error;
    assert maxi_arburst = "10" severity error;
    assert maxi_arcache = (maxi_arcache'RANGE => '0') severity error;
    assert maxi_aruser = (maxi_aruser'RANGE => '0') severity error;
    assert maxi_arvalid = '0' severity error;
    assert maxi_rready = '0' severity error;
    -- Set inputs
    rst <= '0';
    read_din_addr <= (31 downto 8 => '0', 7 downto 2 => '1');
    read_din_valid <= '1';
    read_din_length <= "0101";
    read_dout_ready <= '0';
    isBlocked <= '1';
    maxi_arready <= '1';
    maxi_rdata <= (others => '0');
    maxi_rresp <= (others => '0');
    maxi_rlast <= '0';
    maxi_rvalid <= '0';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert read_din_ready = '0' severity error;
    assert read_dout_data = (read_dout_data'RANGE => '0') severity error;
    assert read_dout_result = (read_dout_result'RANGE => '0') severity error;
    assert read_dout_valid = '0' severity error;
    assert maxi_araddr(31 downto 8) = (31 downto 8 => '0') severity error;
    assert maxi_araddr(7 downto 3) = (7 downto 3 => '1') severity error;
    assert maxi_araddr(2 downto 0) = (2 downto 0 => '0') severity error;
    assert maxi_arlen = "0101" severity error;
    assert maxi_arsize = "011" severity error;
    assert maxi_arburst = "10" severity error;
    assert maxi_arcache = (maxi_arcache'RANGE => '0') severity error;
    assert maxi_aruser = (maxi_aruser'RANGE => '0') severity error;
    assert maxi_arvalid = '0' severity error;
    assert maxi_rready = '0' severity error;
    -- Set inputs
    rst <= '0';
    read_din_addr <= (31 downto 8 => '0', 7 downto 2 => '1');
    read_din_valid <= '1';
    read_din_length <= "0101";
    read_dout_ready <= '0';
    isBlocked <= '1';
    maxi_arready <= '0';
    maxi_rdata <= (others => '0');
    maxi_rresp <= (others => '0');
    maxi_rlast <= '0';
    maxi_rvalid <= '0';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert read_din_ready = '0' severity error;
    assert read_dout_data = (read_dout_data'RANGE => '0') severity error;
    assert read_dout_result = (read_dout_result'RANGE => '0') severity error;
    assert read_dout_valid = '0' severity error;
    assert maxi_araddr(31 downto 8) = (31 downto 8 => '0') severity error;
    assert maxi_araddr(7 downto 3) = (7 downto 3 => '1') severity error;
    assert maxi_araddr(2 downto 0) = (2 downto 0 => '0') severity error;
    assert maxi_arlen = "0101" severity error;
    assert maxi_arsize = "011" severity error;
    assert maxi_arburst = "10" severity error;
    assert maxi_arcache = (maxi_arcache'RANGE => '0') severity error;
    assert maxi_aruser = (maxi_aruser'RANGE => '0') severity error;
    assert maxi_arvalid = '0' severity error;
    assert maxi_rready = '0' severity error;
    -- Set inputs
    rst <= '0';
    read_din_addr <= (31 downto 8 => '0', 7 downto 2 => '1');
    read_din_valid <= '1';
    read_din_length <= "0101";
    read_dout_ready <= '0';
    isBlocked <= '0';
    maxi_arready <= '1';
    maxi_rdata <= (others => '0');
    maxi_rresp <= (others => '0');
    maxi_rlast <= '0';
    maxi_rvalid <= '0';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert read_din_ready = '1' severity error;
    assert read_dout_data = (read_dout_data'RANGE => '0') severity error;
    assert read_dout_result = (read_dout_result'RANGE => '0') severity error;
    assert read_dout_valid = '0' severity error;
    assert maxi_araddr(31 downto 8) = (31 downto 8 => '0') severity error;
    assert maxi_araddr(7 downto 3) = (7 downto 3 => '1') severity error;
    assert maxi_araddr(2 downto 0) = (2 downto 0 => '0') severity error;
    assert maxi_arlen = "0101" severity error;
    assert maxi_arsize = "011" severity error;
    assert maxi_arburst = "10" severity error;
    assert maxi_arcache = (maxi_arcache'RANGE => '0') severity error;
    assert maxi_aruser = (maxi_aruser'RANGE => '0') severity error;
    assert maxi_arvalid = '1' severity error;
    assert maxi_rready = '0' severity error;
    -- Set inputs
    rst <= '0';
    read_din_addr <= (31 downto 8 => '0', 7 downto 2 => '1');
    read_din_valid <= '1';
    read_din_length <= "0101";
    read_dout_ready <= '0';
    isBlocked <= '0';
    maxi_arready <= '1';
    maxi_rdata <= (others => '0');
    maxi_rresp <= (others => '0');
    maxi_rlast <= '0';
    maxi_rvalid <= '0';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert read_din_ready = '0' severity error;
    assert read_dout_data = (read_dout_data'RANGE => '0') severity error;
    assert read_dout_result = (read_dout_result'RANGE => '0') severity error;
    assert read_dout_valid = '0' severity error;
    assert maxi_araddr(31 downto 8) = (31 downto 8 => '0') severity error;
    assert maxi_araddr(7 downto 3) = (7 downto 3 => '1') severity error;
    assert maxi_araddr(2 downto 0) = (2 downto 0 => '0') severity error;
    assert maxi_arlen = "0101" severity error;
    assert maxi_arsize = "011" severity error;
    assert maxi_arburst = "10" severity error;
    assert maxi_arcache = (maxi_arcache'RANGE => '0') severity error;
    assert maxi_aruser = (maxi_aruser'RANGE => '0') severity error;
    assert maxi_arvalid = '0' severity error;
    assert maxi_rready = '0' severity error;
    -- Set inputs
    rst <= '0';
    read_din_addr <= (31 downto 8 => '0', 7 downto 2 => '1');
    read_din_valid <= '0';
    read_din_length <= "0101";
    read_dout_ready <= '0';
    isBlocked <= '0';
    maxi_arready <= '0';
    maxi_rdata <= (others => '0');
    maxi_rresp <= (others => '0');
    maxi_rlast <= '0';
    maxi_rvalid <= '0';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert read_din_ready = '0' severity error;
    assert read_dout_data = (read_dout_data'RANGE => '0') severity error;
    assert read_dout_result = (read_dout_result'RANGE => '0') severity error;
    assert read_dout_valid = '0' severity error;
    assert maxi_araddr(31 downto 8) = (31 downto 8 => '0') severity error;
    assert maxi_araddr(7 downto 3) = (7 downto 3 => '1') severity error;
    assert maxi_araddr(2 downto 0) = (2 downto 0 => '0') severity error;
    assert maxi_arlen = "0101" severity error;
    assert maxi_arsize = "011" severity error;
    assert maxi_arburst = "10" severity error;
    assert maxi_arcache = (maxi_arcache'RANGE => '0') severity error;
    assert maxi_aruser = (maxi_aruser'RANGE => '0') severity error;
    assert maxi_arvalid = '0' severity error;
    assert maxi_rready = '0' severity error;
    -- Set inputs
    rst <= '0';
    read_din_addr <= (31 downto 8 => '0', 7 downto 2 => '1');
    read_din_valid <= '0';
    read_din_length <= "0101";
    read_dout_ready <= '1';
    isBlocked <= '0';
    maxi_arready <= '0';
    maxi_rdata(4 downto 0) <= (others => '1');
    maxi_rresp <= "01";
    maxi_rlast <= '0';
    maxi_rvalid <= '1';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert read_din_ready = '0' severity error;
    assert read_dout_data(4 downto 0) = (4 downto 0 => '1') severity error;
    assert read_dout_result = "01" severity error;
    assert read_dout_valid = '1' severity error;
    assert maxi_araddr(31 downto 8) = (31 downto 8 => '0') severity error;
    assert maxi_araddr(7 downto 3) = (7 downto 3 => '1') severity error;
    assert maxi_araddr(2 downto 0) = (2 downto 0 => '0') severity error;
    assert maxi_arlen = "0101" severity error;
    assert maxi_arsize = "011" severity error;
    assert maxi_arburst = "10" severity error;
    assert maxi_arcache = (maxi_arcache'RANGE => '0') severity error;
    assert maxi_aruser = (maxi_aruser'RANGE => '0') severity error;
    assert maxi_arvalid = '0' severity error;
    assert maxi_rready = '1' severity error;
    -- Set inputs
    rst <= '0';
    read_din_addr <= (31 downto 8 => '0', 7 downto 2 => '1');
    read_din_valid <= '0';
    read_din_length <= "0101";
    read_dout_ready <= '0';
    isBlocked <= '0';
    maxi_arready <= '0';
    maxi_rdata(4 downto 0) <= (4 downto 0 => '1');
    maxi_rresp <= "01";
    maxi_rlast <= '0';
    maxi_rvalid <= '0';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert read_din_ready = '0' severity error;
    assert read_dout_data(4 downto 0) = (4 downto 0 => '1') severity error;
    assert read_dout_result = "01" severity error;
    assert read_dout_valid = '0' severity error;
    assert maxi_araddr(31 downto 8) = (31 downto 8 => '0') severity error;
    assert maxi_araddr(7 downto 3) = (7 downto 3 => '1') severity error;
    assert maxi_araddr(2 downto 0) = (2 downto 0 => '0') severity error;
    assert maxi_arlen = "0101" severity error;
    assert maxi_arsize = "011" severity error;
    assert maxi_arburst = "10" severity error;
    assert maxi_arcache = (maxi_arcache'RANGE => '0') severity error;
    assert maxi_aruser = (maxi_aruser'RANGE => '0') severity error;
    assert maxi_arvalid = '0' severity error;
    assert maxi_rready = '0' severity error;
    -- Set inputs
    rst <= '0';
    read_din_addr <= (31 downto 8 => '0', 7 downto 2 => '1');
    read_din_valid <= '0';
    read_din_length <= "0101";
    read_dout_ready <= '1';
    isBlocked <= '0';
    maxi_arready <= '0';
    maxi_rdata(4 downto 0) <= (4 downto 0 => '1');
    maxi_rresp <= "01";
    maxi_rlast <= '0';
    maxi_rvalid <= '0';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert read_din_ready = '0' severity error;
    assert read_dout_data(4 downto 0) = (4 downto 0 => '1') severity error;
    assert read_dout_result = "01" severity error;
    assert read_dout_valid = '0' severity error;
    assert maxi_araddr(31 downto 8) = (31 downto 8 => '0') severity error;
    assert maxi_araddr(7 downto 3) = (7 downto 3 => '1') severity error;
    assert maxi_araddr(2 downto 0) = (2 downto 0 => '0') severity error;
    assert maxi_arlen = "0101" severity error;
    assert maxi_arsize = "011" severity error;
    assert maxi_arburst = "10" severity error;
    assert maxi_arcache = (maxi_arcache'RANGE => '0') severity error;
    assert maxi_aruser = (maxi_aruser'RANGE => '0') severity error;
    assert maxi_arvalid = '0' severity error;
    assert maxi_rready = '1' severity error;
    -- Set inputs
    rst <= '0';
    read_din_addr <= (31 downto 8 => '0', 7 downto 2 => '1');
    read_din_valid <= '0';
    read_din_length <= "0101";
    read_dout_ready <= '1';
    isBlocked <= '0';
    maxi_arready <= '0';
    maxi_rdata(4 downto 0) <= (4 downto 0 => '1');
    maxi_rresp <= "01";
    maxi_rlast <= '1';
    maxi_rvalid <= '1';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert read_din_ready = '0' severity error;
    assert read_dout_data(4 downto 0) = (4 downto 0 => '1') severity error;
    assert read_dout_result = "01" severity error;
    assert read_dout_valid = '1' severity error;
    assert maxi_araddr(31 downto 8) = (31 downto 8 => '0') severity error;
    assert maxi_araddr(7 downto 3) = (7 downto 3 => '1') severity error;
    assert maxi_araddr(2 downto 0) = (2 downto 0 => '0') severity error;
    assert maxi_arlen = "0101" severity error;
    assert maxi_arsize = "011" severity error;
    assert maxi_arburst = "10" severity error;
    assert maxi_arcache = (maxi_arcache'RANGE => '0') severity error;
    assert maxi_aruser = (maxi_aruser'RANGE => '0') severity error;
    assert maxi_arvalid = '0' severity error;
    assert maxi_rready = '1' severity error;
    -- Set inputs
    rst <= '0';
    read_din_addr <= (31 downto 8 => '0', 7 downto 2 => '1');
    read_din_valid <= '0';
    read_din_length <= "0101";
    read_dout_ready <= '0';
    isBlocked <= '0';
    maxi_arready <= '0';
    maxi_rdata(4 downto 0) <= (4 downto 0 => '1');
    maxi_rresp <= "01";
    maxi_rlast <= '0';
    maxi_rvalid <= '0';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert read_din_ready = '0' severity error;
    assert read_dout_data(4 downto 0) = (4 downto 0 => '1') severity error;
    assert read_dout_result = "01" severity error;
    assert read_dout_valid = '0' severity error;
    assert maxi_araddr(31 downto 8) = (31 downto 8 => '0') severity error;
    assert maxi_araddr(7 downto 3) = (7 downto 3 => '1') severity error;
    assert maxi_araddr(2 downto 0) = (2 downto 0 => '0') severity error;
    assert maxi_arlen = "0101" severity error;
    assert maxi_arsize = "011" severity error;
    assert maxi_arburst = "10" severity error;
    assert maxi_arcache = (maxi_arcache'RANGE => '0') severity error;
    assert maxi_aruser = (maxi_aruser'RANGE => '0') severity error;
    assert maxi_arvalid = '0' severity error;
    assert maxi_rready = '0' severity error;
    test_done <= true;
    wait;
  end process;
end Behavioral;
