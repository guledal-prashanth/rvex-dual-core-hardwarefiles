library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common_pkg.all;
use work.bus_pkg.all;

entity tb_read_manager is
  generic(  CLOCK_PERIOD        : time);
  port (  clk                   : in  std_logic;
          test_done             : out boolean);
end tb_read_manager;

architecture Behavioral of tb_read_manager is
  constant AXI_ADDRESSWIDTHLOG2B: natural := 5;
  constant AXI_DATAWIDTHLOG2B   : natural := 6;
  constant CACHE_LINECOUNTLOG2  : natural := 4;
  constant CACHE_LINESIZELOG2B  : natural := 8;
  constant CACHE_BLOCKCOUNTLOG2 : natural := 2;
  constant AXIWORDSPERLINE      : natural := 2**(CACHE_LINESIZELOG2B - AXI_DATAWIDTHLOG2B);
  constant AXIWORDSPERLINE_SLV  : std_logic_vector(3 downto 0) := std_logic_vector(to_unsigned(AXIWORDSPERLINE - 1, 4));

  signal rst                    : std_logic                                                   := '0';
  signal isBlocked              : std_logic                                                   := '0';
  signal maxi_araddr            : std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
  signal maxi_arlen             : std_logic_vector(3 downto 0);
  signal maxi_arsize            : std_logic_vector(2 downto 0);
  signal maxi_arburst           : std_logic_vector(1 downto 0);
  signal maxi_arcache           : std_logic_vector(3 downto 0);
  signal maxi_aruser            : std_logic_vector(4 downto 0);
  signal maxi_arvalid           : std_logic;
  signal maxi_arready           : std_logic                                                   := '0';
  signal maxi_rdata             : std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0)        := (others => '0');
  signal maxi_rresp             : std_logic_vector(1 downto 0)                                := (others => '0');
  signal maxi_rlast             : std_logic                                                   := '0';
  signal maxi_rvalid            : std_logic                                                   := '0';
  signal maxi_rready            : std_logic;
  signal addrIn                 : rvex_address_type                                           := (others => '0');
  signal writeData              : rvex_data_type                                              := (others => '0');
  signal readData               : rvex_data_type;
  signal writeMask              : rvex_mask_type                                              := (others => '0');
  signal writeEnable            : std_logic                                                   := '0';
  signal readEnable             : std_logic                                                   := '0';
  signal fault                  : std_logic;
  signal busy                   : std_logic;
  signal cache_writeBusy        : std_logic;
  signal ack                    : std_logic;
  signal cache_hit              : std_logic;
begin
  read_manager: entity work.read_manager
  generic map(  AXI_ADDRESSWIDTHLOG2B => AXI_ADDRESSWIDTHLOG2B,
                CACHE_LINECOUNTLOG2 => CACHE_LINECOUNTLOG2,
                CACHE_LINESIZELOG2B => CACHE_LINESIZELOG2B,
                CACHE_BLOCKCOUNTLOG2 => CACHE_BLOCKCOUNTLOG2)
  port map( clk => clk,
            rst => rst,
            isBlocked => isBlocked,
            maxi_araddr => maxi_araddr,
            maxi_arlen => maxi_arlen,
            maxi_arsize => maxi_arsize,
            maxi_arburst => maxi_arburst,
            maxi_arcache => maxi_arcache,
            maxi_aruser => maxi_aruser,
            maxi_arvalid => maxi_arvalid,
            maxi_arready => maxi_arready,
            maxi_rdata => maxi_rdata,
            maxi_rresp => maxi_rresp,
            maxi_rlast => maxi_rlast,
            maxi_rvalid => maxi_rvalid,
            maxi_rready => maxi_rready,
            addrIn => addrIn,
            writeData => writeData,
            readData => readData,
            writeMask => writeMask,
            writeEnable => writeEnable,
            readEnable => readEnable,
            fault => fault,
            busy => busy,
            cache_writeBusy => cache_writeBusy,
            ack => ack,
            hit => cache_hit,
            prng_newseed => (others => '0'),
            prng_reseed => '0' );
  test: process
  begin
    -- Sensible defaults where already set
    isBlocked <= '1';
    wait for CLOCK_PERIOD;
    addrIn <= X"A0000000";
    readEnable <= '1';
    wait for CLOCK_PERIOD;
    assert busy = '1';
    wait for 5*CLOCK_PERIOD;
    isBlocked <= '0';
    assert busy = '1';
    assert maxi_arvalid = '0';
    wait until maxi_arvalid = '1';
    assert maxi_araddr = X"16800000";
    assert maxi_arlen = AXIWORDSPERLINE_SLV;
    assert maxi_arsize = "011";
    assert maxi_arburst = "10";
    maxi_arready <= '1';
    wait for CLOCK_PERIOD;
    assert maxi_rready = '0';
    maxi_arready <= '0';
    maxi_rdata <= std_logic_vector(to_unsigned(2, 32)) & std_logic_vector(to_unsigned(1, 32));
    maxi_rlast <= '0';
    maxi_rvalid <= '1';
    wait until maxi_rready = '1';
    wait for CLOCK_PERIOD;
    maxi_rvalid <= '0';
    wait until maxi_rready = '0';
    wait for CLOCK_PERIOD/2;
    assert busy = '0';
    assert ack = '1';
    assert readData = std_logic_vector(to_unsigned(1, 32));
    addrIn <= X"A0000004";
    readEnable <= '1';
    wait for CLOCK_PERIOD;
    assert busy = '1';
    for I in 0 to 1 loop
      maxi_rdata <= std_logic_vector(to_unsigned(2*I + 4, 32)) & std_logic_vector(to_unsigned(2*I + 3, 32));
      maxi_rlast <= '0';
      maxi_rvalid <= '1';
      wait until maxi_rready = '0';
      maxi_rvalid <= '0';
      wait until maxi_rready = '1';
    end loop;
    maxi_rdata <= std_logic_vector(to_unsigned(8, 32)) & std_logic_vector(to_unsigned(7, 32));
    maxi_rlast <= '1';
    maxi_rvalid <= '1';
    wait until maxi_rready = '0';
    maxi_rvalid <= '0';
    maxi_rlast <= '0';
    wait until busy = '0';
    assert ack = '1';
    assert readData = std_logic_vector(to_unsigned(2, 32));
    readEnable <= '0';
    wait for CLOCK_PERIOD + CLOCK_PERIOD/2;
    assert busy = '0';
    assert ack = '0';
    for I in 2 to 7 loop
      assert readData = std_logic_vector(to_unsigned(I, 32)) report integer'image(I*4) severity error;
      addrIn <= X"A0000000" or std_logic_vector(to_unsigned(I*4, addrIn'LENGTH));
      readEnable <= '1';
      wait for CLOCK_PERIOD;
      assert busy = '1';
      assert ack = '0';
      wait for CLOCK_PERIOD;
      assert busy = '0';
      assert ack = '1';
    end loop;
    readEnable <= '0';
    writeEnable <= '1';
    addrIn <= X"A0000000";
    writeData <= X"01010101";
    writeMask <= X"F";
    wait for CLOCK_PERIOD;
    assert cache_hit = '1';
    wait for CLOCK_PERIOD;
    assert cache_writeBusy = '0';
    wait for CLOCK_PERIOD;
    writeEnable <= '0';
    readEnable <= '1';
    wait for CLOCK_PERIOD;
    assert busy = '1';
    assert ack = '0';
    wait for CLOCK_PERIOD;
    assert readData = X"01010101";
    -- Load a new word
    addrIn <= X"A0000020";
    readEnable <= '1';
    wait for CLOCK_PERIOD;
    assert busy = '1';
    wait until maxi_arvalid = '1';
    assert maxi_araddr = X"16800020";
    assert maxi_arlen = AXIWORDSPERLINE_SLV;
    assert maxi_arsize = "011";
    assert maxi_arburst = "10";
    assert busy = '1';
    maxi_arready <= '1';
    wait until maxi_arvalid = '0';
    wait until maxi_rready = '1';
    maxi_rdata <= X"1000000210000001";
    maxi_rvalid <= '1';
    wait until maxi_rready = '0';
    maxi_rvalid <= '0';
    wait until busy = '0';
    assert ack = '1';
    assert readData = X"10000001";
    addrIn <= X"A0000024";
    readEnable <= '1';
    wait for CLOCK_PERIOD + CLOCK_PERIOD/2;
    assert busy = '1';
    assert ack = '0';
    assert maxi_rready = '1';
    maxi_rdata <= X"1000000410000003";
    maxi_rvalid <= '1';
    wait until maxi_rready = '0';
    assert busy = '1';
    assert ack = '0';
    maxi_rvalid <= '0';
    wait until maxi_rready = '1';
    assert busy = '1';
    assert ack = '0';
    maxi_rdata <= X"1000000610000005";
    maxi_rvalid <= '1';
    wait until maxi_rready = '0';
    assert busy = '1';
    assert ack = '0';
    maxi_rvalid <= '0';
    wait until maxi_rready = '1';
    assert busy = '1';
    assert ack = '0';
    maxi_rdata <= X"1000000810000007";
    maxi_rvalid <= '1';
    maxi_rlast <= '1';
    wait until maxi_rready = '0';
    assert busy = '1';
    assert ack = '0';
    maxi_rvalid <= '0';
    maxi_rlast <= '0';
    wait until busy = '0';
    assert ack = '1';
    assert readData = X"10000002";
    addrIn <= X"A0000028";
    readEnable <= '1';
    wait for CLOCK_PERIOD + CLOCK_PERIOD/2;
    assert busy = '1';
    assert ack = '0';
    wait for CLOCK_PERIOD;
    assert readData = X"10000003";
    readEnable <= '0';
    addrIn <= X"A0000024";
    writeData <= X"A0A00101";
    writeMask <= X"F";
    writeEnable <= '1';
    wait for CLOCK_PERIOD;
    assert cache_hit = '1';
    wait for CLOCK_PERIOD;
    assert cache_writeBusy = '0';
    wait for CLOCK_PERIOD;
    assert ack = '0';
    assert fault = '0';
    assert busy = '0';
    readEnable <= '1';
    writeEnable <= '0';
    wait for CLOCK_PERIOD;
    assert busy = '1';
    assert ack = '0';
    wait for CLOCK_PERIOD;
    assert ack = '1';
    assert readData = X"A0A00101";
    readEnable <= '0';
    writeEnable <= '1';
    addrIn <= X"A0000100";
    writeData <= X"01010101";
    writeMask <= X"F";
    wait for CLOCK_PERIOD;
    assert cache_hit = '0';
    wait for CLOCK_PERIOD;
    assert cache_writeBusy = '0';
    writeEnable <= '0';
    test_done <= true;
    wait;
  end process;
end Behavioral;
