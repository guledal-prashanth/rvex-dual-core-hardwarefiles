library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_cache_block is
  generic ( CLOCK_PERIOD        : time );
  port ( clk                    : in  std_logic;
         test_done              : out boolean );
end tb_cache_block;

architecture Behavioral of tb_cache_block is
  -- Machine 1 has 4 AXI words per line
  constant LINESIZEL2B_1        : natural := 8;
  constant WORDSIZEL2B_1        : natural := 5;
  constant AXISIZEL2B_1         : natural := 6;
  constant ADDRWIDTHL2_1        : natural := 5;
  constant LINECOUNTL2_1        : natural := 2;
  signal reset_1                : std_logic := '1';
  signal addr_1                 : std_logic_vector(2**ADDRWIDTHL2_1 - 1 downto 0);
  signal dataIn_1               : std_logic_vector(2**WORDSIZEL2B_1 - 1 downto 0);
  signal dataInAxi_1            : std_logic_vector(2**AXISIZEL2B_1 - 1 downto 0);
  signal maskRvex_1             : std_logic_vector(2**(WORDSIZEL2B_1 - 3) - 1 downto 0);
  signal dataOut_1              : std_logic_vector(2**WORDSIZEL2B_1 - 1 downto 0);
  signal ready_1                : std_logic;
  signal valid_1                : std_logic;
  signal read_1                 : std_logic;
  signal write_1                : std_logic;
  signal writeLine_1            : std_logic;
  signal hit_1                  : std_logic;
  signal busy_1                 : std_logic;
  signal linewriterBusy_1       : std_logic;
  signal linewriterActive_1     : std_logic;
  signal test_done_1            : boolean := false;
  -- Machine 2 has 2 AXI words per line
  constant LINESIZEL2B_2        : natural := 6;
  constant WORDSIZEL2B_2        : natural := 5;
  constant AXISIZEL2B_2         : natural := 5;
  constant ADDRWIDTHL2_2        : natural := 5;
  constant LINECOUNTL2_2        : natural := 3;
  signal reset_2                : std_logic := '1';
  signal addr_2                 : std_logic_vector(2**ADDRWIDTHL2_2 - 1 downto 0);
  signal dataIn_2               : std_logic_vector(2**WORDSIZEL2B_2 - 1 downto 0);
  signal dataInAxi_2            : std_logic_vector(2**AXISIZEL2B_2 - 1 downto 0);
  signal maskRvex_2             : std_logic_vector(2**(WORDSIZEL2B_2 - 3) - 1 downto 0);
  signal dataOut_2              : std_logic_vector(2**WORDSIZEL2B_2 - 1 downto 0);
  signal ready_2                : std_logic;
  signal valid_2                : std_logic;
  signal read_2                 : std_logic;
  signal write_2                : std_logic;
  signal writeLine_2            : std_logic;
  signal hit_2                  : std_logic;
  signal busy_2                 : std_logic;
  signal linewriterBusy_2       : std_logic;
  signal linewriterActive_2     : std_logic;
  signal test_done_2            : boolean := false;
begin
  test_done <= test_done_1 and test_done_2;
  cache_block_1: entity work.cache_block
  generic map ( LINESIZELOG2B => LINESIZEL2B_1,
                RVEXWORDSIZELOG2B => WORDSIZEL2B_1,
                AXIWORDSIZELOG2B => AXISIZEL2B_1,
                ADDRWIDTHLOG2 => ADDRWIDTHL2_1,
                LINECOUNTLOG2 => LINECOUNTL2_1 )
  port map ( clk => clk,
             reset => reset_1,
             address => addr_1,
             data_in_rvex => dataIn_1,
             data_in_axi => dataInAxi_1,
             mask_rvex => maskRvex_1,
             data_out => dataOut_1,
             ready => ready_1,
             valid => valid_1,
             read => read_1,
             write => write_1,
             write_line => writeLine_1,
             hit => hit_1,
             busy => busy_1,
             linewriter_busy => linewriterBusy_1,
             linewriter_active => linewriterActive_1 );

  cache_block_2: entity work.cache_block
  generic map ( LINESIZELOG2B => LINESIZEL2B_2,
                RVEXWORDSIZELOG2B => WORDSIZEL2B_2,
                AXIWORDSIZELOG2B => AXISIZEL2B_2,
                ADDRWIDTHLOG2 => ADDRWIDTHL2_2,
                LINECOUNTLOG2 => LINECOUNTL2_2 )
  port map ( clk => clk,
             reset => reset_2,
             address => addr_2,
             data_in_rvex => dataIn_2,
             data_in_axi => dataInAxi_2,
             mask_rvex => maskRvex_2,
             data_out => dataOut_2,
             ready => ready_2,
             valid => valid_2,
             read => read_2,
             write => write_2,
             write_line => writeLine_2,
             hit => hit_2,
             busy => busy_2,
             linewriter_busy => linewriterBusy_2,
             linewriter_active => linewriterActive_2 );

  linewriterActive_1 <= linewriterBusy_1;
  linewriterActive_2 <= linewriterBusy_2;

  test_1: process
  begin
    -- Sensible defaults
    reset_1 <= '1';
    addr_1 <= (others => '0');
    dataIn_1 <= (others => '0');
    dataInAxi_1 <= (others => '0');
    valid_1 <= '0';
    write_1 <= '0';
    writeLine_1 <= '0';
    read_1 <= '0';
    maskRvex_1 <= X"F";
    -- Wait for 1 cycle
    wait for CLOCK_PERIOD;
    reset_1 <= '0';
    read_1 <= '1';
    addr_1 <= X"A0000000";
    wait for CLOCK_PERIOD;
    assert hit_1 = '0';
    -- Miss detected, set up for line refresh
    read_1 <= '0';
    addr_1 <= X"A0000000";
    dataInAxi_1 <= X"0000000200000001";
    valid_1 <= '0';
    writeLine_1 <= '1';
    wait for CLOCK_PERIOD;
    -- Do the line refresh
    assert linewriterBusy_1 = '0';
    assert busy_1= '1';
    writeLine_1 <= '0';
    wait for CLOCK_PERIOD;
    assert ready_1 = '1';
    assert busy_1 = '0';
    assert linewriterBusy_1 = '1';
    valid_1 <= '1';
    wait for CLOCK_PERIOD;
    valid_1 <= '0';
    dataInAxi_1 <= X"0000000400000003";
    wait for CLOCK_PERIOD;
    assert ready_1 = '1';
    assert busy_1 = '0';
    valid_1 <= '1';
    wait for CLOCK_PERIOD;
    valid_1 <= '0';
    dataInAxi_1 <= X"0000000600000005";
    wait for CLOCK_PERIOD;
    assert ready_1 = '1';
    assert busy_1 = '0';
    valid_1 <= '1';
    wait for CLOCK_PERIOD;
    valid_1 <= '0';
    dataInAxi_1 <= X"0000000800000007";
    wait for CLOCK_PERIOD;
    assert ready_1 = '1';
    assert busy_1 = '0';
    valid_1 <= '1';
    wait for CLOCK_PERIOD;
    -- Data transaction complete!
    -- Do some checks
    valid_1 <= '0';
    addr_1 <= X"A0000004";
    read_1 <= '1';
    wait for CLOCK_PERIOD;
    assert hit_1 = '1';
    assert dataOut_1 = X"00000002";
    addr_1 <= X"A000000C";
    wait for CLOCK_PERIOD;
    assert hit_1 = '1';
    assert dataOut_1 = X"00000004";
    addr_1 <= X"A0000000";
    wait for CLOCK_PERIOD;
    assert hit_1 = '1';
    assert dataOut_1 = X"00000001";
    -- Do a write
    addr_1 <= X"A0000000";
    write_1 <= '1';
    dataIn_1 <= X"0000000E";
    read_1 <= '0';
    wait for CLOCK_PERIOD;
    assert busy_1 = '1';
    addr_1 <= X"A0000000";
    write_1 <= '1';
    dataIn_1 <= X"0000000E";
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    write_1 <= '0';
    dataIn_1 <= (others => '0');
    valid_1 <= '0';
    addr_1 <= X"A0000000";
    read_1 <= '1';
    wait for CLOCK_PERIOD;
    assert hit_1 = '1';
    assert busy_1 = '1';
    wait for CLOCK_PERIOD;
    assert hit_1 = '1';
    assert busy_1 = '0';
    assert dataOut_1 = X"0000000E";
    -- 2 writes immidiaty after each other
    read_1 <= '0';
    addr_1 <= X"A0000004";
    write_1 <= '1';
    dataIn_1 <= X"0000000A";
    wait for CLOCK_PERIOD;
    assert busy_1 = '1';
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    addr_1 <= X"A0000008";
    write_1 <= '1';
    dataIn_1 <= X"0000000B";
    wait for CLOCK_PERIOD;
    assert busy_1 = '1';
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    write_1 <= '0';
    read_1 <= '1';
    addr_1 <= X"A0000004";
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    assert hit_1 = '1';
    assert dataOut_1 = X"0000000A";
    read_1 <= '1';
    addr_1 <= X"A0000008";
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    assert hit_1 = '1';
    assert dataOut_1 = X"0000000B";
    read_1 <= '0';
    wait for CLOCK_PERIOD;
    -- Redo the block write, but now for the second block
    -- Do reads and writes simultaniously
    assert busy_1 = '0';
    read_1 <= '0';
    addr_1 <= X"A0000020";
    dataInAxi_1 <= X"0000000200000001";
    valid_1 <= '0';
    writeLine_1 <= '1';
    wait for CLOCK_PERIOD;
    -- End of preparation phase
    assert busy_1 = '1';
    wait for CLOCK_PERIOD;
    -- The block should be ready to receive the data
    -- Also try to get a hit from a different line
    assert ready_1 = '1';
    assert busy_1 = '0';
    writeLine_1 <= '0';
    valid_1 <= '1';
    addr_1 <= X"A0000004";
    read_1 <= '1';
    wait for CLOCK_PERIOD;
    -- Now attempt a write in a different line
    -- It should be delayed until the write is done
    assert hit_1 = '1';
    assert busy_1 <= '0';
    assert dataOut_1 = X"0000000A";
    valid_1 <= '0';
    dataInAxi_1 <= X"0000000400000003";
    addr_1 <= X"A0000000";
    write_1 <= '1';
    read_1 <= '0';
    dataIn_1 <= X"00000009";
    wait for CLOCK_PERIOD;
    assert ready_1 = '1';
    assert busy_1 = '1';
    valid_1 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_1 = '1';
    write_1 <= '1';
    valid_1 <= '0';
    dataInAxi_1 <= X"0000000600000005";
    wait for CLOCK_PERIOD;
    assert ready_1 = '1';
    assert busy_1 = '1';
    assert hit_1 = '1';
    valid_1 <= '1';
    wait for CLOCK_PERIOD;
    dataIn_1 <= X"0000000C";
    valid_1 <= '0';
    dataInAxi_1 <= X"0000000800000007";
    wait for CLOCK_PERIOD;
    assert ready_1 = '1';
    assert busy_1 = '1';
    write_1 <= '0';
    wait for CLOCK_PERIOD;
    assert ready_1 = '1';
    assert busy_1 = '1';
    valid_1 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_1 <= '1';
    assert ready_1 = '0';
    valid_1 <= '0';
    wait for CLOCK_PERIOD;
    assert busy_1 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    assert dataOut_1 <= X"0000009";
    -- Now for some tests with 2 line writes at the same time
    wait for CLOCK_PERIOD;
    addr_1 <= X"A0000040";
    wait for CLOCK_PERIOD;
    assert hit_1 = '0';
    assert busy_1 = '0';
    assert linewriterBusy_1 = '0';
    writeLine_1 <= '1';
    dataInAxi_1 <= X"0000001500000014";
    wait for CLOCK_PERIOD;
    assert hit_1 = '0';
    assert busy_1 = '1';
    assert linewriterBusy_1 = '0';
    wait for CLOCK_PERIOD;
    assert hit_1 = '0';
    assert busy_1 = '0';
    assert linewriterBusy_1 = '1';
    assert ready_1 = '1';
    addr_1 <= X"A0000000";
    writeLine_1 <= '0';
    wait for CLOCK_PERIOD;
    assert hit_1 = '1';
    assert busy_1 = '0';
    assert linewriterBusy_1 = '1';
    assert ready_1 = '1';
    valid_1 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    assert linewriterBusy_1 = '1';
    assert ready_1 = '0';
    valid_1 <= '0';
    addr_1 <= X"A0000170";
    writeLine_1 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_1 = '1';
    assert linewriterBusy_1 = '1';
    assert ready_1 = '1';
    dataInAxi_1 <= X"0000001700000016";
    valid_1 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_1 = '1';
    assert linewriterBusy_1 = '1';
    assert ready_1 = '0';
    valid_1 <= '0';
    wait for CLOCK_PERIOD;
    assert busy_1 = '1';
    assert linewriterBusy_1 = '1';
    assert ready_1 = '1';
    dataInAxi_1 <= X"0000001900000018";
    valid_1 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_1 = '1';
    assert linewriterBusy_1 = '1';
    assert ready_1 = '0';
    valid_1 <= '0';
    wait for CLOCK_PERIOD;
    assert busy_1 = '1';
    assert linewriterBusy_1 = '1';
    assert ready_1 = '1';
    dataInAxi_1 <= X"0000001B0000001A";
    valid_1 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_1 = '1';
    assert linewriterBusy_1 = '0';
    assert ready_1 = '0';
    valid_1 <= '0';
    wait for CLOCK_PERIOD;
    assert busy_1 = '1';
    assert linewriterBusy_1 = '0';
    assert ready_1 = '0';
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    assert linewriterBusy_1 = '1';
    assert ready_1 = '1';
    writeLine_1 <= '0';
    -- Finish this transaction
    dataInAxi_1 <= X"0000001D0000001C";
    valid_1 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    assert linewriterBusy_1 = '1';
    assert ready_1 = '0';
    valid_1 <= '0';
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    assert linewriterBusy_1 = '1';
    assert ready_1 = '1';
    dataInAxi_1 <= X"0000001F0000001E";
    valid_1 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    assert linewriterBusy_1 = '1';
    assert ready_1 = '0';
    valid_1 <= '0';
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    assert linewriterBusy_1 = '1';
    assert ready_1 = '1';
    dataInAxi_1 <= X"000000200000001F";
    valid_1 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    assert linewriterBusy_1 = '1';
    assert ready_1 = '0';
    valid_1 <= '0';
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    assert linewriterBusy_1 = '1';
    assert ready_1 = '1';
    dataInAxi_1 <= X"0000002200000021";
    valid_1 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    assert linewriterBusy_1 = '0';
    assert ready_1 = '0';
    valid_1 <= '0';
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    assert linewriterBusy_1 = '0';
    assert ready_1 = '0';
    -- Test a partial write
    addr_1 <= X"A0000168";
    read_1 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    assert hit_1 = '1';
    assert dataOut_1 = X"00000021";
    read_1 <= '0';
    dataIn_1 <= X"12000000";
    write_1 <= '1';
    maskRvex_1 <= X"8";
    wait for CLOCK_PERIOD;
    assert busy_1 = '1';
    wait for CLOCK_PERIOD;
    assert busy_1 = '0';
    read_1 <= '1';
    wait for CLOCK_PERIOD;
    assert hit_1 = '1';
    assert busy_1 = '1';
    wait for CLOCK_PERIOD;
    assert hit_1 = '1';
    assert busy_1 = '0';
    assert dataOut_1 = X"12000021";
    write_1 <= '0';
    wait for 20*CLOCK_PERIOD;
    -- Data transaction complete!
    test_done_1 <= true;
    wait;
  end process;

  test_2: process
  begin
  -- Sensible defaults
  -- Test cache block 2 has 8 lines
  reset_2 <= '1';
  addr_2 <= (others => '0');
  dataIn_2 <= (others => '0');
  dataInAxi_2 <= (others => '0');
  valid_2 <= '0';
  write_2 <= '0';
  read_2 <= '0';
  writeLine_2 <= '0';
  maskRvex_2 <= X"F";
  -- Wait for 1 cycle
  wait for CLOCK_PERIOD;
  -- Miss
  reset_2 <= '0';
  addr_2 <= X"A0000000";
  wait for CLOCK_PERIOD;
  -- Prepare a write to line 0
  assert hit_2 = '0';
  writeLine_2 <= '1';
  wait for CLOCK_PERIOD;
  -- Respond to the new eviction line request
  assert ready_2 = '0';
  writeLine_2 <= '0';
  wait for CLOCK_PERIOD;
  -- Prepare the first word for the write
  assert ready_2 = '1';
  dataInAxi_2 <= X"00000001";
  valid_2 <= '1';
  wait for CLOCK_PERIOD;
  assert ready_2 = '0';
  valid_2 <= '0';
  addr_2 <= X"A0000008";
  wait for CLOCK_PERIOD;
  -- Do a second write line and go into waiting mode
  assert ready_2 = '1';
  assert hit_1 = '0';
  writeLine_2 <= '1';
  wait for CLOCK_PERIOD;
  -- Write the second word to the first line
  assert busy_2 = '1';
  dataInAxi_2 <= X"00000002";
  valid_2 <= '1';
  writeLine_2 <= '0';
  wait for CLOCK_PERIOD;
  assert ready_2 = '0';
  assert busy_2 = '1';
  valid_2 <= '0';
  wait for CLOCK_PERIOD;
  assert ready_2 = '0';
  wait for CLOCK_PERIOD;
  assert ready_2 = '1';
  dataInAxi_2 <= X"00000003";
  valid_2 <= '1';
  addr_2 <= X"A0000000";
  wait for CLOCK_PERIOD;
  assert hit_2 = '1';
  assert dataOut_2 = X"00000001";
  assert ready_2 = '0';
  valid_2 <= '0';
  write_2 <= '1';
  dataIn_2 <= X"0000000A";
  wait for CLOCK_PERIOD;
  assert busy_2 = '1';
  assert ready_2 = '1';
  valid_2 <= '1';
  dataInAxi_2 <= X"00000004";
  write_2 <= '0';
  wait for CLOCK_PERIOD;
  assert busy_2 = '1';
  assert ready_2 = '0';
  valid_2 <= '0';
    test_done_2 <= true;
    wait;
  end process;
end Behavioral;
