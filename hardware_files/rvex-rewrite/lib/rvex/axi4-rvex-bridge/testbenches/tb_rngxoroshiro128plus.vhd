library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_rngxoroshiro128plus is
  generic (
    CLOCK_PERIOD                : time
  );
  port (
    clk                         : in  std_logic;
    test_done                   : out boolean
  );
end tb_rngxoroshiro128plus;

architecture Behavioral of tb_rngxoroshiro128plus is
  constant seed                 : natural := 14;

  signal rst                    : std_logic                         := '0';
  signal reseed                 : std_logic                         := '0';
  signal newseed                : std_logic_vector(127 downto 0)    := (others => '0');
  signal out_ready              : std_logic                         := '0';
  signal out_valid              : std_logic;
  signal out_data               : std_logic_vector(63 downto 0);

begin
  prng_1: entity work.rng_xoroshiro128plus
  generic map (
    init_seed => std_logic_vector(to_unsigned(seed, newseed'LENGTH))
  )
  port map (
    clk => clk,
    rst => rst,
    reseed => reseed,
    newseed => newseed,
    out_ready => out_ready,
    out_valid => out_valid,
    out_data => out_data
  );

  test: process
    variable output : std_logic_vector(63 downto 0);
  begin
    rst <= '1';
    wait for 2*CLOCK_PERIOD;
    rst <= '0';
    wait for CLOCK_PERIOD;
    assert out_valid = '1';
    wait for CLOCK_PERIOD;
    assert out_valid = '1';
    out_ready <= '1';
    output := out_data;
    wait for CLOCK_PERIOD;
    assert out_valid = '1';
    assert output /= out_data;
    out_ready <= '0';
    test_done <= true;
    wait;
  end process;

end Behavioral;
