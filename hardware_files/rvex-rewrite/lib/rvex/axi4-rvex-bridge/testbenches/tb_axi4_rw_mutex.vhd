library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_axi4_rw_mutex is
  generic ( CLOCK_PERIOD        : time );
  port ( clk                    : in  std_logic;
         test_done              : out boolean );
end tb_axi4_rw_mutex;

architecture Behavioral of tb_axi4_rw_mutex is
  constant ADDRESSWIDTHL2       : natural := 5;
  constant LINESIZEL2           : natural := 8;
  signal read_inputAddr         : std_logic_vector(2**ADDRESSWIDTHL2 - 1 downto 0);
  signal write_inputAddr        : std_logic_vector(2**ADDRESSWIDTHL2 - 1 downto 0);
  signal read_blocked           : std_logic;
  signal write_isActive         : std_logic;
begin
  axi4_rw_mutex: entity work.axi4_rw_mutex
  generic map(  AXI_ADDRESSWIDTHLOG2B => ADDRESSWIDTHL2,
                LINESIZELOG2B => LINESIZEL2)
  port map( read_inputAddr => read_inputAddr,
            write_inputAddr => write_inputAddr,
            write_isActive => write_isActive,
            read_blocked => read_blocked
          );
  test: process
  begin
    -- Sensible defaults
    read_inputAddr <= (others => '0');
    write_inputAddr <= (others => '0');
    write_isActive <= '0';
    wait for CLOCK_PERIOD;
    -- Activate write, see if read gets blocked
    write_isActive <= '1';
    wait for CLOCK_PERIOD;
    assert read_blocked = '1';
    -- Change the write address so that it is different but should still block
    write_inputAddr <= X"A0000001";
    write_isActive <= '0';
    wait for CLOCK_PERIOD;
    assert read_blocked = '0';
    -- Switch active signals, see if block signals follow
    write_isActive <= '1';
    wait for CLOCK_PERIOD;
    assert read_blocked = '1';
    test_done <= true;
    wait;
  end process;
end Behavioral;
