library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common_pkg.all;
use work.bus_pkg.all;

entity tb_toplevel is
  generic ( CLOCK_PERIOD        : time );
  port ( clk                    : in  std_logic;
         test_done              : out boolean );
end tb_toplevel;

architecture Behavioral of tb_toplevel is
  subtype addressable_byte is std_logic_vector(7 downto 0);
  type mem_array is array (natural range <>) of addressable_byte;

  function recalculateAddress( input, rvexBase, axiBase: std_logic_vector)
  return std_logic_vector is
  variable retval : std_logic_vector(rvex_address_type'RANGE) := (others => '0');
  begin
    retval := std_logic_vector(unsigned(input) - unsigned(rvexBase) + unsigned(axiBase));
    return retval;
  end function;

  function readCompare(memAddr: natural; compareData: std_logic_vector; memory: mem_array)
    return boolean is
  begin
    return compareData  = memory(memAddr + 3) & memory(memAddr + 2) & memory(memAddr + 1) & memory(memAddr + 0);
  end function;

  constant AXI_ADDRESSWIDTHLOG2B    : natural := 5;
  constant AXI_DATAWIDTHLOG2B       : natural := 6;
  constant CACHE_LINECOUNTLOG2      : natural := 2;
  constant CACHE_LINESIZELOG2B      : natural := 8;
  constant CACHE_BLOCKCOUNTLOG2     : natural := 2;
  constant AXIWRITDELAYCYCLES       : natural := 2;
  constant AXIREADDELAYCYCLES       : natural := 8;
  constant ADDRESSCOUNT             : natural := 20;
  constant RVEXBASEADDRESS          : std_logic_vector(rvex_address_type'RANGE) := (others => '0');
  constant AXIBASEADDRESS           : std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0) := X"16900000";
  signal reset                      : std_logic;
  signal updateAddrBase             : std_logic;
  signal baseAddrIn                 : std_logic_vector(rvex_address_type'RANGE);
  signal rvex_bus_in                : bus_mst2slv_type;
  signal rvex_bus_out               : bus_slv2mst_type;
  signal maxi_aclk                  : std_logic;
  signal maxi_awid                  : std_logic_vector(2 DOWNTO 0);
  signal maxi_awaddr                : std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
  signal maxi_awlen                 : std_logic_vector(3 downto 0);
  signal maxi_awsize                : std_logic_vector(2 downto 0);
  signal maxi_awburst               : std_logic_vector(1 downto 0);
  signal maxi_awlock                : std_logic_vector(1 downto 0);
  signal maxi_awcache               : std_logic_vector(3 downto 0);
  signal maxi_awprot                : std_logic_vector(2 downto 0);
  signal maxi_awqos                 : std_logic_vector(3 downto 0);
  signal maxi_awuser                : std_logic_vector(4 downto 0);
  signal maxi_awvalid               : std_logic;
  signal maxi_awready               : std_logic;
  signal maxi_wid                   : std_logic_vector(2 downto 0);
  signal maxi_wdata                 : std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0);
  signal maxi_wstrb                 : std_logic_vector(2**(AXI_DATAWIDTHLOG2B - 3) - 1 downto 0);
  signal maxi_wlast                 : std_logic;
  signal maxi_wvalid                : std_logic;
  signal maxi_wready                : std_logic;
  signal maxi_bid                   : std_logic_vector(2 downto 0);
  signal maxi_bresp                 : std_logic_vector(1 downto 0);
  signal maxi_bvalid                : std_logic;
  signal maxi_bready                : std_logic;
  signal maxi_arid                  : std_logic_vector(2 downto 0);
  signal maxi_araddr                : std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
  signal maxi_arlen                 : std_logic_vector(3 downto 0);
  signal maxi_arsize                : std_logic_vector(2 downto 0);
  signal maxi_arburst               : std_logic_vector(1 downto 0);
  signal maxi_arlock                : std_logic_vector(1 downto 0);
  signal maxi_arcache               : std_logic_vector(3 downto 0);
  signal maxi_arprot                : std_logic_vector(2 downto 0);
  signal maxi_arqos                 : std_logic_vector(3 downto 0);
  signal maxi_aruser                : std_logic_vector(4 downto 0);
  signal maxi_arvalid               : std_logic;
  signal maxi_arready               : std_logic;
  signal maxi_rid                   : std_logic_vector(2 downto 0);
  signal maxi_rdata                 : std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0);
  signal maxi_rresp                 : std_logic_vector(1 downto 0);
  signal maxi_rlast                 : std_logic;
  signal maxi_rvalid                : std_logic;
  signal maxi_rready                : std_logic;

  signal rvex_sim_done              : boolean := false;
  signal read_rvexAddr              : std_logic_vector(rvex_bus_in.address'RANGE);
  signal write_rvexAddr             : std_logic_vector(rvex_bus_in.address'RANGE);

  signal memory                     : mem_array(2**10 - 1 downto 0) := (others => (others => '0'));

  subtype rvexAddress is std_logic_vector(rvex_bus_in.address'RANGE);
  type addr_array is array (natural range <>) of rvexAddress;
  signal write_addressList          : addr_array(ADDRESSCOUNT - 1 downto 0):= (others => (others => '0'));
  signal read_addressList           : addr_array(ADDRESSCOUNT - 1 downto 0):= (others => (others => '0'));
begin
  test_done <= rvex_sim_done;
  toplevel: entity work.pvexAxiSlaveToplevel
  generic map ( AXI_ADDRESSWIDTHLOG2B => AXI_ADDRESSWIDTHLOG2B,
                AXI_DATAWIDTHLOG2B  => AXI_DATAWIDTHLOG2B,
                CACHE_LINECOUNTLOG2 => CACHE_LINECOUNTLOG2,
                CACHE_LINESIZELOG2B => CACHE_LINESIZELOG2B,
                CACHE_BLOCKCOUNTLOG2 => CACHE_BLOCKCOUNTLOG2 )
  port map( clk            => clk,
            rst            => reset,
            updateAddrBase => updateAddrBase,
            baseAddrIn     => baseAddrIn,
            rvex_bus_in    => rvex_bus_in,
            rvex_bus_out   => rvex_bus_out,
            maxi_aclk      => maxi_aclk,
            maxi_awid      => maxi_awid,
            maxi_awaddr    => maxi_awaddr,
            maxi_awlen     => maxi_awlen,
            maxi_awsize    => maxi_awsize,
            maxi_awburst   => maxi_awburst,
            maxi_awlock    => maxi_awlock,
            maxi_awcache   => maxi_awcache,
            maxi_awprot    => maxi_awprot,
            maxi_awqos     => maxi_awqos,
            maxi_awuser    => maxi_awuser,
            maxi_awvalid   => maxi_awvalid,
            maxi_awready   => maxi_awready,
            maxi_wid       => maxi_wid,
            maxi_wdata     => maxi_wdata,
            maxi_wstrb     => maxi_wstrb,
            maxi_wlast     => maxi_wlast,
            maxi_wvalid    => maxi_wvalid,
            maxi_wready    => maxi_wready,
            maxi_bid       => maxi_bid,
            maxi_bresp     => maxi_bresp,
            maxi_bvalid    => maxi_bvalid,
            maxi_bready    => maxi_bready,
            maxi_arid      => maxi_arid,
            maxi_araddr    => maxi_araddr,
            maxi_arlen     => maxi_arlen,
            maxi_arsize    => maxi_arsize,
            maxi_arburst   => maxi_arburst,
            maxi_arlock    => maxi_arlock,
            maxi_arcache   => maxi_arcache,
            maxi_arprot    => maxi_arprot,
            maxi_arqos     => maxi_arqos,
            maxi_aruser    => maxi_aruser,
            maxi_arvalid   => maxi_arvalid,
            maxi_arready   => maxi_arready,
            maxi_rid       => maxi_rid,
            maxi_rdata     => maxi_rdata,
            maxi_rresp     => maxi_rresp,
            maxi_rlast     => maxi_rlast,
            maxi_rvalid    => maxi_rvalid,
            maxi_rready    => maxi_rready,
            prng_reseed    => '0',
            prng_newseed   => (others => '0') );

  assert not (rvex_bus_out.busy = '1' and rvex_bus_out.ack = '1');
  assert not (rvex_bus_in.writeEnable = '1' and rvex_bus_in.readEnable = '1');

  -- This is an AXI read slave. So it is actually a writer
  -- Giechelt.
  axi_read_sim: process
    variable readIndex          : natural := 0;
    variable pointer            : std_logic_vector(1 downto 0);
    variable internalAddr       : std_logic_vector(maxi_araddr'RANGE);
    variable address_nat        : natural := 0;
  begin
    if not rvex_sim_done then
      -- Default values
      maxi_rdata <= (others => '0');
      maxi_rresp <= (others => '0');
      maxi_rlast <= '0';
      maxi_rvalid <= '0';
      maxi_arready <= '1';
      wait until maxi_arvalid = '1';
      assert maxi_arid = (maxi_arid'RANGE => '0');
      assert maxi_araddr(maxi_araddr'left downto AXI_ADDRESSWIDTHLOG2B - 2)
        = recalculateAddress(read_addressList(readIndex), RVEXBASEADDRESS, AXIBASEADDRESS)
          (maxi_araddr'left downto AXI_ADDRESSWIDTHLOG2B - 2);
      assert maxi_araddr(AXI_ADDRESSWIDTHLOG2B - 4 downto 0) = (AXI_ADDRESSWIDTHLOG2B - 4 downto 0 => '0');
      readIndex := readIndex + 1;
      assert maxi_arlen = "0011";
      assert maxi_arsize = "011";
      assert maxi_arburst = "10";
      assert maxi_arlock = (maxi_arlock'RANGE => '0');
      assert maxi_arcache = (maxi_arcache'RANGE => '0');
      assert maxi_arprot = (maxi_arprot'RANGE => '0');
      assert maxi_arqos = (maxi_arqos'RANGE => '0');
      assert maxi_aruser = (maxi_aruser'RANGE => '0');
      pointer := maxi_araddr(4 downto 3);
      internalAddr := std_logic_vector(unsigned(maxi_araddr) - unsigned(AXIBASEADDRESS));
      wait for CLOCK_PERIOD + CLOCK_PERIOD/2;
      maxi_arready <= '0';
      wait for AXIREADDELAYCYCLES*CLOCK_PERIOD;
      for I in 0 to 3 loop
        assert maxi_rready = '1';
        address_nat := to_integer(unsigned(internalAddr));
        maxi_rid <= (others => '0');
        maxi_rdata <= memory(address_nat + 7) & memory(address_nat + 6) & memory(address_nat + 5) & memory(address_nat + 4) &
                      memory(address_nat + 3) & memory(address_nat + 2) & memory(address_nat + 1) & memory(address_nat + 0);
        maxi_rresp <= (others => '0');
        if I = 3 then
          maxi_rlast <= '1';
        else
          maxi_rlast <= '0';
        end if;
      wait for CLOCK_PERIOD;
        maxi_rvalid <= '1';
        wait for CLOCK_PERIOD;
        assert maxi_rready = '0';
        maxi_rvalid <= '0';
        pointer := std_logic_vector(unsigned(pointer) + 1);
        internalAddr := internalAddr(internalAddr'LEFT downto 5) & pointer & internalAddr(2 downto 0);
        wait for CLOCK_PERIOD;
      end loop;
    else
      wait;
    end if;
  end process;

  -- This is an AXI write slave, so it reads
  axi_write_sim: process
    variable writeIndex         : natural := 0;
    variable address_nat        : natural := 0;
  begin
    if not (rvex_sim_done) then
      -- Default values
      maxi_awready <= '0';
      maxi_wready <= '0';
      maxi_bresp <= (others => '0');
      maxi_bvalid <= '0';
      wait for CLOCK_PERIOD;
      maxi_awready <= '1';
      wait until maxi_awvalid = '1';
      assert maxi_awid = (maxi_awid'RANGE => '0');
      assert maxi_awaddr(maxi_awaddr'left downto AXI_ADDRESSWIDTHLOG2B - 2)
        = recalculateAddress(write_addressList(writeIndex), RVEXBASEADDRESS, AXIBASEADDRESS)(maxi_awaddr'left downto AXI_ADDRESSWIDTHLOG2B - 2);
      assert maxi_awaddr(AXI_ADDRESSWIDTHLOG2B - 4 downto 0) = (AXI_ADDRESSWIDTHLOG2B - 4 downto 0 => '0');
      address_nat := to_integer(unsigned(maxi_awaddr) - unsigned(AXIBASEADDRESS));
      writeIndex := writeIndex + 1;
      assert maxi_awlen = (maxi_awlen'RANGE => '0');
      assert maxi_awsize = "010";
      assert maxi_awburst = "01";
      assert maxi_awlock = (maxi_awlock'RANGE => '0');
      assert maxi_awcache = (maxi_awcache'RANGE => '0');
      assert maxi_awprot = (maxi_awprot'RANGE => '0');
      assert maxi_awqos = (maxi_awqos'RANGE => '0');
      assert maxi_awuser = (maxi_awuser'RANGE => '0');
      wait for CLOCK_PERIOD;
      maxi_awready <= '0';
      maxi_wready <= '1';
      assert maxi_wvalid = '1';
      assert maxi_wid = (maxi_wid'RANGE => '0');
      assert maxi_wlast = '1';
      assert maxi_wstrb = X"0F" or maxi_wstrb = X"F0";
      if maxi_wstrb = X"0F" then
        memory(address_nat) <= maxi_wdata(7 downto 0);
        memory(address_nat + 1) <= maxi_wdata(15 downto 8);
        memory(address_nat + 2) <= maxi_wdata(23 downto 16);
        memory(address_nat + 3) <= maxi_wdata(31 downto 24);
      else
        memory(address_nat + 4) <= maxi_wdata(39 downto 32);
        memory(address_nat + 5) <= maxi_wdata(47 downto 40);
        memory(address_nat + 6) <= maxi_wdata(55 downto 48);
        memory(address_nat + 7) <= maxi_wdata(63 downto 56);
      end if;
      wait for CLOCK_PERIOD;
      maxi_wready <= '0';
      wait for AXIWRITDELAYCYCLES*CLOCK_PERIOD;
      assert maxi_bready = '1';
      maxi_bid <= (others => '0');
      maxi_bresp <= (others => '0');
      maxi_bvalid <= '1';
      wait for CLOCK_PERIOD;
      maxi_bvalid <= '0';
    else
      wait;
    end if;
  end process;

  -- This process handles the rvex bus
  rvex_sim: process
    variable writeIndex         : natural := 0;
    variable readIndex          : natural := 0;
    variable address_nat           : natural := 0;
  begin
    -- Default values
    reset <= '0';
    updateAddrBase <= '0';
    baseAddrIn <= (others => '0');
    rvex_bus_in.address <= (others => '0');
    rvex_bus_in.readEnable <= '0';
    rvex_bus_in.writeEnable <= '0';
    rvex_bus_in.writeMask <= (others => '0');
    rvex_bus_in.writeData <= (others => '0');
    wait for CLOCK_PERIOD;
    -- Start with setting the translator
    baseAddrIn <= RVEXBASEADDRESS;
    updateAddrBase <= '1';
    wait until rising_edge(clk);
    wait for CLOCK_PERIOD/2;
    updateAddrBase <= '0';
    wait for CLOCK_PERIOD;
    -- Set up the first request
    address_nat := 0;
    read_addressList(readIndex) <=
        std_logic_vector(to_unsigned(address_nat, read_addressList(readIndex)'LENGTH) + unsigned(RVEXBASEADDRESS));
    readIndex := readIndex + 1;
    rvex_bus_in.address <=
        std_logic_vector(to_unsigned(address_nat, read_addressList(readIndex)'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.readEnable <= '1';
    wait until rising_edge(clk);
    wait for CLOCK_PERIOD;
    assert rvex_bus_out.busy = '1';
    assert rvex_bus_out.fault = '0';
    assert rvex_bus_out.ack = '0';
    wait until rvex_bus_out.busy = '0';
    assert rvex_bus_out.ack = '1';
    assert rvex_bus_out.fault = '0';
    assert readCompare(address_nat, rvex_bus_out.readData, memory);
    rvex_bus_in.readEnable <= '0';
    wait for 4*CLOCK_PERIOD;
    assert rvex_bus_out.busy = '0';
    assert rvex_bus_out.fault = '0';
    assert rvex_bus_out.ack = '0';
    address_nat := 4;
    rvex_bus_in.address <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.readEnable <= '1';
    wait for CLOCK_PERIOD + CLOCK_PERIOD/2;
    assert rvex_bus_out.busy = '1';
    assert rvex_bus_out.fault = '0';
    assert rvex_bus_out.ack = '0';
    wait until rvex_bus_out.busy = '0';
    assert rvex_bus_out.busy = '0';
    assert rvex_bus_out.fault = '0';
    assert rvex_bus_out.ack = '1';
    assert readCompare(address_nat, rvex_bus_out.readData, memory);
    address_nat := 8;
    rvex_bus_in.address <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    wait for CLOCK_PERIOD;
    assert rvex_bus_out.busy = '0';
    assert rvex_bus_out.fault = '0';
    assert rvex_bus_out.ack = '1';
    assert readCompare(address_nat, rvex_bus_out.readData, memory);
    address_nat := 12;
    rvex_bus_in.address <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    wait for CLOCK_PERIOD;
    assert rvex_bus_out.busy = '1';
    assert rvex_bus_out.fault = '0';
    assert rvex_bus_out.ack = '0';
    wait for CLOCK_PERIOD/2;
    assert rvex_bus_out.busy = '0';
    assert rvex_bus_out.fault = '0';
    assert rvex_bus_out.ack = '1';
    assert readCompare(address_nat, rvex_bus_out.readData, memory);
    rvex_bus_in.readEnable <= '0';
    address_nat := 4;
    write_addressList(writeIndex) <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    writeIndex := writeIndex + 1;
    rvex_bus_in.address <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.writeData <= X"000b00b5";
    rvex_bus_in.writeMask <= (others => '1');
    rvex_bus_in.writeEnable <= '1';
    wait for CLOCK_PERIOD + CLOCK_PERIOD/2;
    assert rvex_bus_out.busy = '1';
    assert rvex_bus_out.fault = '0';
    assert rvex_bus_out.ack = '0';
    wait until rvex_bus_out.busy = '0';
    assert rvex_bus_out.fault = '0';
    assert rvex_bus_out.ack = '1';
    address_nat := 12;
    write_addressList(writeIndex) <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    writeIndex := writeIndex + 1;
    rvex_bus_in.address <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.writeData <= X"000b00b4";
    rvex_bus_in.writeMask <= (others => '1');
    rvex_bus_in.writeEnable <= '1';
    wait for CLOCK_PERIOD + CLOCK_PERIOD/2;
    assert rvex_bus_out.busy = '1';
    assert rvex_bus_out.fault = '0';
    assert rvex_bus_out.ack = '0';
    wait until rvex_bus_out.busy = '0';
    wait for CLOCK_PERIOD/2;
    assert rvex_bus_out.fault = '0';
    assert rvex_bus_out.ack = '1';
    rvex_bus_in.writeEnable <= '0';
    address_nat := 4;
    rvex_bus_in.address <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.readEnable <= '1';
    wait for CLOCK_PERIOD;
    assert rvex_bus_out.busy = '1';
    assert rvex_bus_out.fault = '0';
    assert rvex_bus_out.ack = '0';
    wait for CLOCK_PERIOD;
    assert rvex_bus_out.busy = '0';
    assert rvex_bus_out.fault = '0';
    assert rvex_bus_out.ack = '1';
    assert rvex_bus_out.readData <= X"000B00B5";
    rvex_bus_in.readEnable <= '0';
    rvex_bus_in.writeEnable <= '1';
    address_nat := 32;
    write_addressList(writeIndex) <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.address <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.writeData <= X"87654321";
    writeIndex := writeIndex + 1;
    wait for CLOCK_PERIOD;
    assert rvex_bus_out.busy = '1';
    assert rvex_bus_out.fault = '0';
    assert rvex_bus_out.ack = '0';
    wait until rvex_bus_out.busy = '0';
    assert rvex_bus_out.ack = '1';
    assert rvex_bus_out.fault = '0';
    rvex_bus_in.writeEnable <= '0';
    rvex_bus_in.writeData <= (others => '0');
    read_addressList(readIndex) <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    readIndex := readIndex + 1;
    rvex_bus_in.readEnable <= '1';
    wait for CLOCK_PERIOD + CLOCK_PERIOD/2;
    assert rvex_bus_out.busy = '1';
    assert rvex_bus_out.fault = '0';
    assert rvex_bus_out.ack = '0';
    wait until rvex_bus_out.busy = '0';
    assert rvex_bus_out.ack = '1';
    assert rvex_bus_out.fault = '0';
    assert readCompare(address_nat, rvex_bus_out.readData, memory);
    rvex_bus_in.readEnable <= '0';
    address_nat := 36;
    write_addressList(writeIndex) <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.address <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.writeData <= X"0FEDCBA9";
    writeIndex := writeIndex + 1;
    rvex_bus_in.writeEnable <= '1';
    wait for CLOCK_PERIOD + CLOCK_PERIOD/2;
    assert rvex_bus_out.busy = '1';
    assert rvex_bus_out.ack = '0';
    assert rvex_bus_out.fault = '0';
    wait for CLOCK_PERIOD;
    assert rvex_bus_out.busy = '0';
    assert rvex_bus_out.ack = '1';
    assert rvex_bus_out.fault = '0';
    rvex_bus_in.writeEnable <= '0';
    address_nat := 64;
    rvex_bus_in.writeData <= (others => '0');
    read_addressList(readIndex) <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.address <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    readIndex := readIndex + 1;
    rvex_bus_in.readEnable <= '1';
    wait for CLOCK_PERIOD;
    assert rvex_bus_out.busy = '1';
    assert rvex_bus_out.ack = '0';
    assert rvex_bus_out.fault = '0';
    wait until rvex_bus_out.busy = '0';
    wait for CLOCK_PERIOD/2;
    assert rvex_bus_out.ack = '1';
    assert rvex_bus_out.fault = '0';
    assert readCompare(address_nat, rvex_bus_out.readData, memory);
    rvex_bus_in.readEnable <= '0';
    address_nat := 40;
    write_addressList(writeIndex) <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.address <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.writeData <= X"76543210";
    writeIndex := writeIndex + 1;
    rvex_bus_in.writeEnable <= '1';
    wait for CLOCK_PERIOD;
    assert rvex_bus_out.busy = '1';
    assert rvex_bus_out.ack = '0';
    assert rvex_bus_out.fault = '0';
    wait until rvex_bus_out.busy = '0';
    assert rvex_bus_out.ack = '1';
    assert rvex_bus_out.fault = '0';
    address_nat := 44;
    write_addressList(writeIndex) <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.address <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.writeData <= X"FEDCBA98";
    writeIndex := writeIndex + 1;
    wait for CLOCK_PERIOD + CLOCK_PERIOD/2;
    assert rvex_bus_out.busy = '1';
    assert rvex_bus_out.ack = '0';
    assert rvex_bus_out.fault = '0';
    wait until rvex_bus_out.busy = '0';
    assert rvex_bus_out.ack = '1';
    assert rvex_bus_out.fault = '0';
    address_nat := 68;
    rvex_bus_in.writeData <= (others => '0');
    read_addressList(readIndex) <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.address <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    readIndex := readIndex + 1;
    rvex_bus_in.readEnable <= '1';
    rvex_bus_in.writeEnable <= '0';
    wait for CLOCK_PERIOD + CLOCK_PERIOD/2;
    assert rvex_bus_out.busy = '1';
    assert rvex_bus_out.ack = '0';
    assert rvex_bus_out.fault = '0';
    wait for CLOCK_PERIOD;
    assert rvex_bus_out.busy = '0';
    assert rvex_bus_out.ack = '1';
    assert rvex_bus_out.fault = '0';
    assert readCompare(address_nat, rvex_bus_out.readData, memory);
    address_nat := 40;
    rvex_bus_in.writeData <= (others => '0');
    read_addressList(readIndex) <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    rvex_bus_in.address <= std_logic_vector(to_unsigned(address_nat, rvex_address_type'LENGTH) + unsigned(RVEXBASEADDRESS));
    readIndex := readIndex + 1;
    rvex_bus_in.readEnable <= '1';
    wait for CLOCK_PERIOD;
    assert rvex_bus_out.busy = '1';
    assert rvex_bus_out.ack = '0';
    assert rvex_bus_out.fault = '0';
    wait for CLOCK_PERIOD;
    assert rvex_bus_out.busy = '0';
    assert rvex_bus_out.ack = '1';
    assert rvex_bus_out.fault = '0';
    assert readCompare(address_nat, rvex_bus_out.readData, memory);
    rvex_bus_in.readEnable <= '0';



    wait for 100*CLOCK_PERIOD;
    rvex_sim_done <= true;
    wait;
  end process;
end Behavioral;
