library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.common_pkg.all;
use work.bus_pkg.all;

entity tb_write_manager is
  generic (
    CLOCK_PERIOD                : time
  );
  port (
    clk                         : in  std_logic;
    test_done                   : out boolean
  );
end tb_write_manager;

architecture Behavioral of tb_write_manager is
  constant AXI_ADDRESSWIDTHLOG2B: natural := 5;
  constant AXI_DATAWIDTHLOG2B   : natural := 6;

  signal rst                    : std_logic                                                   := '0';
  signal isActive               : std_logic;
  signal maxi_awaddr            : std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
  signal maxi_awlen             : std_logic_vector(3 downto 0);
  signal maxi_awsize            : std_logic_vector(2 downto 0);
  signal maxi_awcache           : std_logic_vector(3 downto 0);
  signal maxi_awuser            : std_logic_vector(4 downto 0);
  signal maxi_awburst           : std_logic_vector(1 downto 0);
  signal maxi_awvalid           : std_logic;
  signal maxi_awready           : std_logic                                                   := '0';
  signal maxi_wdata             : std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0);
  signal maxi_wstrb             : std_logic_vector(2**(AXI_DATAWIDTHLOG2B - 3) - 1 downto 0);
  signal maxi_wlast             : std_logic;
  signal maxi_wvalid            : std_logic;
  signal maxi_wready            : std_logic                                                   := '0';
  signal maxi_bresp             : std_logic_vector(1 downto 0)                                := "00";
  signal maxi_bvalid            : std_logic                                                   := '0';
  signal maxi_bready            : std_logic;
  signal addrIn                 : rvex_address_type                                           := (others => '0');
  signal writeData              : rvex_data_type                                              := (others => '0');
  signal readData               : rvex_data_type;
  signal writeMask              : rvex_mask_type                                              := (others => '0');
  signal writeEnable            : std_logic                                                   := '0';
  signal fault                  : std_logic;
  signal busy                   : std_logic;
  signal cache_writeBusy        : std_logic                                                   := '0';
  signal ack                    : std_logic;
  signal cache_hit              : std_logic                                                   := '0';
begin
  write_manager: entity work.write_manager
  generic map (
    AXI_ADDRESSWIDTHLOG2B => AXI_ADDRESSWIDTHLOG2B,
    AXI_DATAWIDTHLOG2B => AXI_DATAWIDTHLOG2B,
    SUPPRESSFAULT => false
  )
  port map (
    clk => clk,
    rst => rst,
    isActive => isActive,
    maxi_awaddr => maxi_awaddr,
    maxi_awlen => maxi_awlen,
    maxi_awsize => maxi_awsize,
    maxi_awuser => maxi_awuser,
    maxi_awcache => maxi_awcache,
    maxi_awburst => maxi_awburst,
    maxi_awvalid => maxi_awvalid,
    maxi_awready => maxi_awready,
    maxi_wdata => maxi_wdata,
    maxi_wstrb => maxi_wstrb,
    maxi_wlast => maxi_wlast,
    maxi_wvalid => maxi_wvalid,
    maxi_wready => maxi_wready,
    maxi_bresp => maxi_bresp,
    maxi_bvalid => maxi_bvalid,
    maxi_bready => maxi_bready,
    addrIn => addrIn,
    writeData => writeData,
    readData => readData,
    writeMask => writeMask,
    writeEnable => writeEnable,
    fault => fault,
    busy => busy,
    cache_writeBusy => cache_writeBusy,
    ack => ack,
    cache_hit => cache_hit
  );

  test: process
    constant expectedMaxiAwsize : std_logic_vector(maxi_awsize'RANGE) := std_logic_vector(
                                            to_unsigned(integer(ceil(log2(real(rvex_data_type'LENGTH / 8)))), maxi_awsize'LENGTH));
    constant pvexOffset         : unsigned(31 downto 0) := x"F0000000";
    constant axiOffset          : unsigned(31 downto 0) := x"16900000";
    constant writeData1         : std_logic_vector(writeData'RANGE) := x"00ABCDEF";
    constant writeData2         : std_logic_vector(writeData'RANGE) := x"00500005";
  begin
    -- Set default values
    rst <= '1';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= "00";
    maxi_bvalid <= '0';
    addrIn <= (others => '0');
    writeData <= (others => '0');
    writeMask <= (others => '0');
    writeEnable <= '0';
    cache_writeBusy <= '0';
    cache_hit <= '0';
    wait for CLOCK_PERIOD;
    -- Check if the default outputs are what we expect
    -- Most of the maxi outputs are not checked, since this is not the axi_writer testbench
    assert isActive = '0';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '0';
    assert maxi_wvalid = '0';
    assert maxi_bready = '0';
    assert readData = (readData'RANGE => '0');
    assert fault = '0';
    assert busy = '0';
    assert ack = '0';
    -- Deassert the reset
    rst <= '0';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= "00";
    maxi_bvalid <= '0';
    addrIn <= (others => '0');
    writeData <= (others => '0');
    writeMask <= (others => '0');
    writeEnable <= '0';
    wait for CLOCK_PERIOD;
    assert isActive = '0';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '0';
    assert maxi_wvalid = '0';
    assert maxi_bready = '0';
    assert readData = (readData'RANGE => '0');
    assert fault = '0';
    assert busy = '0';
    assert ack = '0';
    -- ####
    -- First part: set in a write, we need it to return within one cycle
    -- setup the input data and address translation
    -- ####
    rst <= '0';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= "00";
    maxi_bvalid <= '0';
    addrIn <= std_logic_vector(resize(pvexOffset + x"FCFC", addrIn'length));
    writeData <= writeData1;
    writeMask <= (others => '1');
    writeEnable <= '0';
    wait for CLOCK_PERIOD;
    assert isActive = '0';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '0';
    assert maxi_wvalid = '0';
    assert maxi_bready = '0';
    assert fault = '0';
    assert busy = '0';
    assert ack = '0';
    -- Stop updating the addr base so that the propagated address becomes correct
    rst <= '0';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= "00";
    maxi_bvalid <= '0';
    addrIn <= std_logic_vector(resize(pvexOffset + x"FCFC", addrIn'length));
    writeData <= writeData1;
    writeMask <= (others => '1');
    writeEnable <= '0';
    wait for CLOCK_PERIOD;
    assert isActive = '0';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '0';
    assert maxi_wvalid = '0';
    assert maxi_bready = '0';
    -- Do check the maxi outputs here
    assert fault = '0';
    assert busy = '0';
    assert ack = '0';
    assert maxi_awaddr = std_logic_vector(axiOffset + x"FCF8");
    assert maxi_wdata = writeData1 & (writeData'Range => '0');
    assert maxi_wstrb = "11110000";
    assert readData = (readData'RANGE => '0');
    -- Enable writeEnable to commit the transfer
    rst <= '0';
    maxi_awready <= '1';
    maxi_wready <= '1';
    maxi_bresp <= "00";
    maxi_bvalid <= '0';
    addrIn <= std_logic_vector(resize(pvexOffset + x"FCFC", addrIn'length));
    writeData <= writeData1;
    writeMask <= (others => '1');
    writeEnable <= '1';
    wait for CLOCK_PERIOD;
    -- The writer did nothing and a writeEnable happened.
    -- There is no hit, so on the next rising edge, an ack should occur
    -- Push in the second write
    assert isActive = '1';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '1';
    assert maxi_wvalid = '1';
    assert maxi_bready = '0';
    assert fault = '0';
    assert busy = '1';
    assert ack = '0';
    maxi_awready <= '0';
    maxi_wready <= '0';
    wait for CLOCK_PERIOD;
    assert fault = '0';
    assert busy = '0';
    assert ack = '1';
    assert maxi_awaddr = std_logic_vector(axiOffset + x"FCF8");
    assert maxi_wdata = writeData1 & (writeData'Range => '0');
    assert maxi_wstrb = "11110000";
    assert readData = std_logic_vector(resize(pvexOffset + x"FCFC", addrIn'length));
    rst <= '0';
    maxi_awready <= '1';
    maxi_wready <= '0';
    maxi_bresp <= "00";
    maxi_bvalid <= '0';
    addrIn <= std_logic_vector(resize(pvexOffset + x"FC00", addrIn'length));
    writeData <= writeData2;
    writeMask <= (others => '1');
    writeEnable <= '1';
    wait for CLOCK_PERIOD;
    assert busy = '1';
    assert ack = '0';
    rst <= '0';
    maxi_awready <= '1';
    maxi_wready <= '0';
    maxi_bresp <= "00";
    maxi_bvalid <= '0';
    addrIn <= std_logic_vector(resize(pvexOffset + x"FC00", addrIn'length));
    writeData <= writeData2;
    writeMask <= (others => '1');
    writeEnable <= '1';
    wait for CLOCK_PERIOD;
    assert isActive = '1';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '0';
    assert maxi_wvalid = '1';
    assert maxi_bready = '0';
    assert fault = '0';
    assert busy = '1';
    assert ack = '0';
    assert maxi_awaddr = std_logic_vector(axiOffset + x"FCF8");
    assert maxi_wdata = writeData1 & (writeData'Range => '0');
    assert maxi_wstrb = "11110000";
    assert readData = std_logic_vector(resize(pvexOffset + x"FCFC", addrIn'length));
    rst <= '0';
    maxi_awready <= '0';
    maxi_wready <= '1';
    maxi_bresp <= "00";
    maxi_bvalid <= '0';
    addrIn <= std_logic_vector(resize(pvexOffset + x"FC00", addrIn'length));
    writeData <= writeData2;
    writeMask <= (others => '1');
    writeEnable <= '1';
    wait for CLOCK_PERIOD;
    assert isActive = '1';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '0';
    assert maxi_wvalid = '0';
    assert maxi_bready = '1';
    assert fault = '0';
    assert busy = '1';
    assert ack = '0';
    assert readData = std_logic_vector(resize(pvexOffset + x"FCFC", addrIn'length));
    rst <= '0';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= "00";
    maxi_bvalid <= '0';
    addrIn <= std_logic_vector(resize(pvexOffset + x"FC00", addrIn'length));
    writeData <= writeData2;
    writeMask <= (others => '1');
    writeEnable <= '1';
    wait for CLOCK_PERIOD;
    assert isActive = '1';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '0';
    assert maxi_wvalid = '0';
    assert maxi_bready = '1';
    assert fault = '0';
    assert busy = '1';
    assert ack = '0';
    assert readData = std_logic_vector(resize(pvexOffset + x"FCFC", addrIn'length));
    assert maxi_awaddr = std_logic_vector(axiOffset + x"FC00");
    assert maxi_wdata = (writeData'Range => '0') & writeData2;
    assert maxi_wstrb = "00001111";
    rst <= '0';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= "00";
    maxi_bvalid <= '1';
    addrIn <= std_logic_vector(resize(pvexOffset + x"FC00", addrIn'length));
    writeData <= writeData2;
    writeMask <= (others => '1');
    writeEnable <= '1';
    wait for CLOCK_PERIOD;
    assert isActive = '0';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '0';
    assert maxi_wvalid = '0';
    assert maxi_bready = '0';
    assert fault = '0';
    assert busy = '1';
    assert ack = '0';
    assert readData = std_logic_vector(resize(pvexOffset + x"FCFC", addrIn'length));
    assert maxi_awaddr = std_logic_vector(axiOffset + x"FC00");
    assert maxi_wdata = (writeData'Range => '0') & writeData2;
    assert maxi_wstrb = "00001111";
    rst <= '0';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= "00";
    maxi_bvalid <= '0';
    addrIn <= std_logic_vector(resize(pvexOffset + x"FC00", addrIn'length));
    writeData <= writeData2;
    writeMask <= (others => '1');
    writeEnable <= '1';
    wait for CLOCK_PERIOD;
    assert isActive = '0';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '0';
    assert maxi_wvalid = '0';
    assert maxi_bready = '0';
    assert fault = '0';
    assert busy = '0';
    assert ack = '1';
    assert readData = std_logic_vector(resize(pvexOffset + x"FC00", addrIn'length));
    assert maxi_awaddr = std_logic_vector(axiOffset + x"FC00");
    assert maxi_wdata = (writeData'Range => '0') & writeData2;
    assert maxi_wstrb = "00001111";
    rst <= '0';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= "00";
    maxi_bvalid <= '0';
    addrIn <= (others => '0');
    writeData <= (others => '0');
    writeMask <= (others => '0');
    writeEnable <= '0';
    wait for CLOCK_PERIOD;
    assert isActive = '0';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '0';
    assert maxi_wvalid = '0';
    assert maxi_bready = '0';
    assert fault = '0';
    assert busy = '0';
    assert ack = '0';
    assert readData = std_logic_vector(resize(pvexOffset + x"FC00", addrIn'length));
    assert maxi_awaddr = std_logic_vector(axiOffset + x"FC00");
    assert maxi_wdata = (writeData'Range => '0') & writeData2;
    assert maxi_wstrb = "00001111";
    rst <= '0';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= "00";
    maxi_bvalid <= '0';
    addrIn <= (others => '0');
    writeData <= (others => '0');
    writeMask <= (others => '0');
    writeEnable <= '0';
    wait for CLOCK_PERIOD;
    assert isActive = '1';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '1';
    assert maxi_wvalid = '1';
    assert maxi_bready = '0';
    assert fault = '0';
    assert busy = '0';
    assert ack = '0';
    assert readData = std_logic_vector(resize(pvexOffset + x"FC00", addrIn'length));
    assert maxi_awaddr = std_logic_vector(axiOffset + x"FC00");
    assert maxi_wdata = (writeData'Range => '0') & writeData2;
    assert maxi_wstrb = "00001111";
    rst <= '0';
    maxi_awready <= '1';
    maxi_wready <= '1';
    maxi_bresp <= "00";
    maxi_bvalid <= '0';
    addrIn <= (others => '0');
    writeData <= (others => '0');
    writeMask <= (others => '0');
    writeEnable <= '0';
    wait for CLOCK_PERIOD;
    assert isActive = '1';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '0';
    assert maxi_wvalid = '0';
    assert maxi_bready = '1';
    assert fault = '0';
    assert busy = '0';
    assert ack = '0';
    assert readData = std_logic_vector(resize(pvexOffset + x"FC00", addrIn'length));
    assert maxi_wdata = (writeData'Range => '0') & writeData2;
    assert maxi_wstrb = "00001111";
    rst <= '0';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= "10";
    maxi_bvalid <= '1';
    addrIn <= (others => '0');
    writeData <= (others => '0');
    writeMask <= (others => '0');
    writeEnable <= '1';
    wait for CLOCK_PERIOD;
    wait for CLOCK_PERIOD;
    assert isActive = '1';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '1';
    assert maxi_wvalid = '1';
    assert maxi_bready = '0';
    assert fault = '1';
    assert busy = '0';
    assert ack = '1';
    rst <= '1';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= "00";
    maxi_bvalid <= '0';
    addrIn <= (others => '0');
    writeData <= (others => '0');
    writeMask <= (others => '0');
    writeEnable <= '0';
    wait for CLOCK_PERIOD;
    assert isActive = '0';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '0';
    assert maxi_wvalid = '0';
    assert maxi_bready = '0';
    assert fault = '0';
    assert busy = '0';
    assert ack = '0';
    rst <= '0';
    addrIn <= X"F0000000";
    writeData <= X"12341234";
    writeMask <= (others => '1');
    cache_hit <= '1';
    writeEnable <= '1';
    wait for CLOCK_PERIOD;
    assert isActive = '1';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '1';
    assert maxi_wvalid = '1';
    assert maxi_bready = '0';
    assert fault = '0';
    assert busy = '1';
    assert ack = '0';
    assert maxi_awaddr = X"16900000";
    assert maxi_wdata = (writeData'Range => '0') & X"12341234";
    assert maxi_wstrb = "00001111";
    cache_writeBusy <= '1';
    wait for CLOCK_PERIOD;
    assert isActive = '1';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '1';
    assert maxi_wvalid = '1';
    assert maxi_bready = '0';
    assert fault = '0';
    assert busy = '1';
    assert ack = '0';
    assert maxi_awaddr = X"16900000";
    assert maxi_wdata = (writeData'Range => '0') & X"12341234";
    assert maxi_wstrb = "00001111";
    cache_writeBusy <= '0';
    wait for CLOCK_PERIOD;
    assert isActive = '1';
    assert maxi_awsize = expectedMaxiAwsize;
    assert maxi_awvalid = '1';
    assert maxi_wvalid = '1';
    assert maxi_bready = '0';
    assert fault = '0';
    assert busy = '0';
    assert ack = '1';
    assert maxi_awaddr = X"16900000";
    assert maxi_wdata = (writeData'Range => '0') & X"12341234";
    assert maxi_wstrb = "00001111";
    writeEnable <= '0';
    rst <= '1';
    
    test_done <= true;
    wait;
  end process;

end Behavioral;
