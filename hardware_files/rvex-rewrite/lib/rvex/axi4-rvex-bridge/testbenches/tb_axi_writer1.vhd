library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_axi_writer1 is
  generic (
    CLOCK_PERIOD                  : in time
  );
  port (
    clk                           : in  std_logic;
    test_done                     : out boolean
  );
end tb_axi_writer1;

architecture Behavioral of tb_axi_writer1 is
  constant DATAWIDTHLOG2B       : natural := 6;
  constant ADDRWIDTHLOG2B       : natural := 5;

  signal rst                    : std_logic                                                 := '1';
  signal write_din_addr         : std_logic_vector(2**ADDRWIDTHLOG2B - 1 downto 2)          := (others => '0');
  signal write_din_data         : std_logic_vector(2**DATAWIDTHLOG2B - 1 downto 0)          := (others => '0');
  signal write_din_sizelog2b    : std_logic_vector(2 downto 0)                              := (others => '0');
  signal write_din_mask         : std_logic_vector(2**(DATAWIDTHLOG2B - 3) - 1 downto 0)    := (others => '0');
  signal write_din_valid        : std_logic                                                 := '0';
  signal write_din_ready        : std_logic;
  signal write_dout_resp        : std_logic_vector(1 downto 0);
  signal write_dout_valid       : std_logic;
  signal write_dout_ready       : std_logic                                                 := '0';
  signal isActive               : std_logic;
  signal maxi_awaddr            : std_logic_vector(2**ADDRWIDTHLOG2B - 1 downto 0);
  signal maxi_awlen             : std_logic_vector(3 downto 0);
  signal maxi_awsize            : std_logic_vector(2 downto 0);
  signal maxi_awcache           : std_logic_vector(3 downto 0);
  signal maxi_awuser            : std_logic_vector(4 downto 0);
  signal maxi_awburst           : std_logic_vector(1 downto 0);
  signal maxi_awvalid           : std_logic;
  signal maxi_awready           : std_logic                                                 := '0';
  signal maxi_wdata             : std_logic_vector(2**DATAWIDTHLOG2B - 1 downto 0);
  signal maxi_wstrb             : std_logic_vector(2**(DATAWIDTHLOG2B - 3) - 1 downto 0);
  signal maxi_wlast             : std_logic;
  signal maxi_wvalid            : std_logic;
  signal maxi_wready            : std_logic                                                 := '0';
  signal maxi_bresp             : std_logic_vector(1 downto 0)                              := (others => '0');
  signal maxi_bvalid            : std_logic                                                 := '0';
  signal maxi_bready            : std_logic;
begin
  axi4_write_1 : entity work.axi4_writer
  generic map (
    AXI_DATAWIDTHLOG2B          => DATAWIDTHLOG2B,
    AXI_ADDRESSWIDTHLOG2B       => ADDRWIDTHLOG2B
  )
  port map (
    clk                         => clk,
    rst                         => rst,
    write_din_addr              => write_din_addr,
    write_din_data              => write_din_data,
    write_din_sizelog2b         => write_din_sizelog2b,
    write_din_mask              => write_din_mask,
    write_din_valid             => write_din_valid,
    write_din_ready             => write_din_ready,
    write_dout_resp             => write_dout_resp,
    write_dout_valid            => write_dout_valid,
    write_dout_ready            => write_dout_ready,
    isActive                    => isActive,
    maxi_awaddr                 => maxi_awaddr,
    maxi_awlen                  => maxi_awlen,
    maxi_awsize                 => maxi_awsize,
    maxi_awcache                => maxi_awcache,
    maxi_awuser                 => maxi_awuser,
    maxi_awburst                => maxi_awburst,
    maxi_awvalid                => maxi_awvalid,
    maxi_awready                => maxi_awready,
    maxi_wdata                  => maxi_wdata,
    maxi_wstrb                  => maxi_wstrb,
    maxi_wlast                  => maxi_wlast,
    maxi_wvalid                 => maxi_wvalid,
    maxi_wready                 => maxi_wready,
    maxi_bresp                  => maxi_bresp,
    maxi_bvalid                 => maxi_bvalid,
    maxi_bready                 => maxi_bready
  );

  test: process
    constant firstTransferData  : std_logic_vector(63 downto 0) := (15 downto 0 => '0') & (15 downto 0 => '1') & x"ACE02468";
    constant firstAddress       : std_logic_vector(31 downto 0) := x"000000F4";
    constant firstMask          : std_logic_vector(7 downto 0)  := x"0F";
    constant firstSizeLog2b     : std_logic_vector(2 downto 0)  := "010";

    constant secondTransferData : std_logic_vector(63 downto 0) := x"13579BDFECA86420";
    constant secondAddress      : std_logic_vector(31 downto 0) := x"000000F0";
    constant secondMask         : std_logic_vector(7 downto 0)  := x"A5";
    constant secondSizeLog2b    : std_logic_vector(2 downto 0)  := "011";
  begin
    -- Pass sensible defaults
    rst <= '1';
    write_din_addr <= (others => '0');
    write_din_data <= (others => '0');
    write_din_sizelog2b <= (others => '0');
    write_din_mask <= (others => '0');
    write_din_valid <= '0';
    write_dout_ready <= '0';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= (others => '0');
    maxi_bvalid <= '0';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert write_din_ready = '0';
    assert write_dout_resp = (write_dout_resp'RANGE => '0');
    assert write_dout_valid = '0';
    assert isActive = '0';
    assert maxi_awaddr = (maxi_awaddr'RANGE => '0');
    assert maxi_awlen = (maxi_awlen'RANGE => '0');
    assert maxi_awsize = (maxi_awsize'RANGE => '0');
    assert maxi_awcache = (maxi_awcache'RANGE => '0');
    assert maxi_awuser = (maxi_awuser'RANGE => '0');
    assert maxi_awburst = "01";
    assert maxi_awvalid = '0';
    assert maxi_wdata = (maxi_wdata'RANGE => '0');
    assert maxi_wstrb = (maxi_wstrb'RANGE => '0');
    assert maxi_wlast = '1';
    assert maxi_wvalid = '0';
    assert maxi_bready = '0';
    -- Drop the reset
    rst <= '0';
    write_din_addr <= (others => '0');
    write_din_data <= (others => '0');
    write_din_sizelog2b <= (others => '0');
    write_din_mask <= (others => '0');
    write_din_valid <= '0';
    write_dout_ready <= '0';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= (others => '0');
    maxi_bvalid <= '0';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert write_din_ready = '0';
    assert write_dout_resp = (write_dout_resp'RANGE => '0');
    assert write_dout_valid = '0';
    assert isActive = '0';
    assert maxi_awaddr = (maxi_awaddr'RANGE => '0');
    assert maxi_awlen = (maxi_awlen'RANGE => '0');
    assert maxi_awsize = (maxi_awsize'RANGE => '0');
    assert maxi_awcache = (maxi_awcache'RANGE => '0');
    assert maxi_awuser = (maxi_awuser'RANGE => '0');
    assert maxi_awburst = "01";
    assert maxi_awvalid = '0';
    assert maxi_wdata = (maxi_wdata'RANGE => '0');
    assert maxi_wstrb = (maxi_wstrb'RANGE => '0');
    assert maxi_wlast = '1';
    assert maxi_wvalid = '0';
    assert maxi_bready = '0';

    -- Set the new inputs: deassert the reset and set the inputs
    -- ==================================================
    -- First transfer: 32 bit of data, not 64 bit aligned
    -- We expect the writer to put the lower 32 bit of the input data on the upper 32 bit of the output data.
    -- We expect the address to become realigned: the lower 3 bits become 0
    -- We expect the lower half of the masked to become all zeros and the upper half all 1.
    -- ==================================================
    rst <= '0';
    write_din_addr <= firstAddress(31 downto 2);
    write_din_data <= firstTransferData;
    write_din_sizelog2b <= firstSizeLog2b;
    write_din_mask <= firstMask;
    write_din_valid <= '1';
    write_dout_ready <= '0';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= (others => '0');
    maxi_bvalid <= '0';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert write_din_ready = '0';
    assert write_dout_resp = (write_dout_resp'RANGE => '0');
    assert write_dout_valid = '0';
    assert isActive = '1';
    assert maxi_awaddr = firstAddress(31 downto 3) & "000";
    assert maxi_awlen = (maxi_awlen'RANGE => '0');
    assert maxi_awsize = firstSizeLog2b;
    assert maxi_awcache = (maxi_awcache'RANGE => '0');
    assert maxi_awuser = (maxi_awuser'RANGE => '0');
    assert maxi_awburst = "01";
    assert maxi_awvalid = '1';
    assert maxi_wdata = firstTransferData(31 downto 0) & (31 downto 0 => '0');
    assert maxi_wstrb = firstMask(3 downto 0) & (3 downto 0 => '0');
    assert maxi_wlast = '1';
    assert maxi_wvalid = '1';
    assert maxi_bready = '0';
    -- ####
    -- The axi4 slave aw part is now ready
    -- ####
    rst <= '0';
    write_din_addr <= firstAddress(31 downto 2);
    write_din_data <= firstTransferData;
    write_din_sizelog2b <= firstSizeLog2b;
    write_din_mask <= firstMask;
    write_din_valid <= '1';
    write_dout_ready <= '0';
    maxi_awready <= '1';
    maxi_wready <= '0';
    maxi_bresp <= (others => '0');
    maxi_bvalid <= '0';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert write_din_ready = '0';
    assert write_dout_resp = (write_dout_resp'RANGE => '0');
    assert write_dout_valid = '0';
    assert isActive = '1';
    assert maxi_awaddr = firstAddress(31 downto 3) & "000";
    assert maxi_awlen = (maxi_awlen'RANGE => '0');
    assert maxi_awsize = firstSizeLog2b;
    assert maxi_awcache = (maxi_awcache'RANGE => '0');
    assert maxi_awuser = (maxi_awuser'RANGE => '0');
    assert maxi_awburst = "01";
    assert maxi_awvalid = '0';
    assert maxi_wdata = firstTransferData(31 downto 0) & (31 downto 0 => '0');
    assert maxi_wstrb = firstMask(3 downto 0) & (3 downto 0 => '0');
    assert maxi_wlast = '1';
    assert maxi_wvalid = '1';
    assert maxi_bready = '0';
    -- ###
    -- The AXI4 slave w part now becomes ready and so does the write response
    -- ###
    rst <= '0';
    write_din_addr <= firstAddress(31 downto 2);
    write_din_data <= firstTransferData;
    write_din_sizelog2b <= firstSizeLog2b;
    write_din_mask <= firstMask;
    write_din_valid <= '1';
    write_dout_ready <= '0';
    maxi_awready <= '0';
    maxi_wready <= '1';
    maxi_bresp <= (others => '0');
    maxi_bvalid <= '1';
    wait for CLOCK_PERIOD / 2;
    assert write_din_ready = '1';
    write_din_valid <= '0';
    wait for CLOCK_PERIOD/2;
    -- Check outputs
    assert write_din_ready = '0';
    assert write_dout_resp = (write_dout_resp'RANGE => '0');
    assert write_dout_valid = '1';
    assert isActive = '1';
    assert maxi_awaddr = firstAddress(31 downto 3) & "000";
    assert maxi_awlen = (maxi_awlen'RANGE => '0');
    assert maxi_awsize = firstSizeLog2b;
    assert maxi_awcache = (maxi_awcache'RANGE => '0');
    assert maxi_awuser = (maxi_awuser'RANGE => '0');
    assert maxi_awburst = "01";
    assert maxi_awvalid = '0';
    assert maxi_wdata = firstTransferData(31 downto 0) & (31 downto 0 => '0');
    assert maxi_wstrb = firstMask(3 downto 0) & (3 downto 0 => '0');
    assert maxi_wlast = '1';
    assert maxi_wvalid = '0';
    assert maxi_bready = '0';
    --###
    -- The din bus can now be fully deasserted, which is the only thing that changes in this cycle
    --##
    rst <= '0';
    write_din_addr <= (others => '0');
    write_din_data <= (others => '0');
    write_din_sizelog2b <= (others => '0');
    write_din_mask <= (others => '0');
    write_din_valid <= '0';
    write_dout_ready <= '0';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= (others => '0');
    maxi_bvalid <= '1';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert write_din_ready = '0';
    assert write_dout_resp = (write_dout_resp'RANGE => '0');
    assert write_dout_valid = '1';
    assert isActive = '1';
    assert maxi_awaddr = (maxi_awaddr'RANGE => '0');
    assert maxi_awlen = (maxi_awlen'RANGE => '0');
    assert maxi_awsize = (maxi_awsize'RANGE => '0');
    assert maxi_awcache = (maxi_awcache'RANGE => '0');
    assert maxi_awuser = (maxi_awuser'RANGE => '0');
    assert maxi_awburst = "01";
    assert maxi_awvalid = '0';
    assert maxi_wdata = (maxi_wdata'RANGE => '0');
    assert maxi_wstrb = (maxi_wstrb'RANGE => '0');
    assert maxi_wlast = '1';
    assert maxi_wvalid = '0';
    assert maxi_bready = '0';
    --###
    -- The internal side is now ready to receive the response
    --##
    rst <= '0';
    write_din_addr <= (others => '0');
    write_din_data <= (others => '0');
    write_din_sizelog2b <= (others => '0');
    write_din_mask <= (others => '0');
    write_din_valid <= '0';
    write_dout_ready <= '1';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= (others => '0');
    maxi_bvalid <= '1';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert write_din_ready = '0';
    assert write_dout_resp = (write_dout_resp'RANGE => '0');
    assert write_dout_valid = '0';
    assert isActive = '0';
    assert maxi_awaddr = (maxi_awaddr'RANGE => '0');
    assert maxi_awlen = (maxi_awlen'RANGE => '0');
    assert maxi_awsize = (maxi_awsize'RANGE => '0');
    assert maxi_awcache = (maxi_awcache'RANGE => '0');
    assert maxi_awuser = (maxi_awuser'RANGE => '0');
    assert maxi_awburst = "01";
    assert maxi_awvalid = '0';
    assert maxi_wdata = (maxi_wdata'RANGE => '0');
    assert maxi_wstrb = (maxi_wstrb'RANGE => '0');
    assert maxi_wlast = '1';
    assert maxi_wvalid = '0';
    assert maxi_bready = '0';
    -- ===============================================
    -- The internal side immidiately continues with transmission 2
    -- This is a 64 bit transfer with a special mask.
    -- This transfer is blocked for 2 cycles.
    -- ===============================================
    rst <= '0';
    write_din_addr <= secondAddress(31 downto 2);
    write_din_data <= secondTransferData;
    write_din_sizelog2b <= secondSizeLog2b;
    write_din_mask <= secondMask;
    write_din_valid <= '1';
    write_dout_ready <= '0';
    maxi_awready <= '0';
    maxi_wready <= '0';
    maxi_bresp <= (others => '0');
    maxi_bvalid <= '0';
    wait for CLOCK_PERIOD;
    -- Check outputs
    assert write_din_ready = '0';
    assert write_dout_resp = (write_dout_resp'RANGE => '0');
    assert write_dout_valid = '0';
    assert isActive = '0';
    assert maxi_awaddr = firstAddress(31 downto 3) & "000";
    assert maxi_awlen = (maxi_awlen'RANGE => '0');
    assert maxi_awsize = secondSizeLog2b;
    assert maxi_awcache = (maxi_awcache'RANGE => '0');
    assert maxi_awuser = (maxi_awuser'RANGE => '0');
    assert maxi_awburst = "01";
    assert maxi_awvalid = '0';
    assert maxi_wdata = secondTransferData;
    assert maxi_wstrb = secondMask;
    assert maxi_wlast = '1';
    assert maxi_wvalid = '0';
    assert maxi_bready = '0';



    test_done <= true;
    wait;
  end process;

end architecture;
