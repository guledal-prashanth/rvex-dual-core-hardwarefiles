library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_cache is
  generic ( CLOCK_PERIOD        : time );
  port ( clk                    : in  std_logic;
         test_done              : out boolean );
end tb_cache;

architecture Behavioral of tb_cache is
  -- Machine 1 has 1 cache block with one line
  constant LINESIZEL2B_1        : natural := 6;
  constant WORDSIZEL2B_1        : natural := 5;
  constant AXISIZEL2B_1         : natural := 5;
  constant ADDRWIDTHL2_1        : natural := 6;
  constant LINECOUNTL2_1        : natural := 0;
  constant BLOCKCOUNTL2_1       : natural := 0;
  signal reset_1                : std_logic := '0';
  signal addr_1                 : std_logic_vector(2**ADDRWIDTHL2_1 - 1 downto 0);
  signal dataIn_1               : std_logic_vector(2**WORDSIZEL2B_1 - 1 downto 0);
  signal dataInAxi_1            : std_logic_vector(2**AXISIZEL2B_1 - 1 downto 0);
  signal maskRvex_1             : std_logic_vector(2**(WORDSIZEL2B_1 - 3) - 1 downto 0);
  signal dataOut_1              : std_logic_vector(2**WORDSIZEL2B_1 - 1 downto 0);
  signal ready_1                : std_logic;
  signal valid_1                : std_logic;
  signal read_1                 : std_logic;
  signal write_1                : std_logic;
  signal writeLine_1            : std_logic;
  signal hit_1                  : std_logic;
  signal busy_1                 : std_logic;
  signal getNextLine_1          : std_logic;
  signal curEvictLine_1         : std_logic_vector(LINECOUNTL2_1 - 1 downto 0);
  signal test_done_1            : boolean := false;

  -- Second cache address layout: 32 bit address
  -- Bits 0-1: unused
  -- Bits 1 - 4: rvex block tag
  -- Bits 2 - 4: axi block tag
  -- Bits 5 - 6: Block selector
  -- bits 7 - 31: search tag
  constant LINESIZEL2B_2        : natural := 8;
  constant WORDSIZEL2B_2        : natural := 5;
  constant AXISIZEL2B_2         : natural := 6;
  constant ADDRWIDTHL2_2        : natural := 5;
  constant LINECOUNTL2_2        : natural := 4;
  constant BLOCKCOUNTL2_2       : natural := 2;
  signal reset_2                : std_logic := '0';
  signal addr_2                 : std_logic_vector(2**ADDRWIDTHL2_2 - 1 downto 0);
  signal dataIn_2               : std_logic_vector(2**WORDSIZEL2B_2 - 1 downto 0);
  signal dataInAxi_2            : std_logic_vector(2**AXISIZEL2B_2 - 1 downto 0);
  signal maskRvex_2             : std_logic_vector(2**(WORDSIZEL2B_2 - 3) - 1 downto 0);
  signal dataOut_2              : std_logic_vector(2**WORDSIZEL2B_2 - 1 downto 0);
  signal ready_2                : std_logic;
  signal valid_2                : std_logic;
  signal read_2                 : std_logic;
  signal write_2                : std_logic;
  signal writeLine_2            : std_logic;
  signal hit_2                  : std_logic;
  signal busy_2                 : std_logic;
  signal getNextLine_2          : std_logic;
  signal curEvictLine_2         : std_logic_vector(LINECOUNTL2_2 - 1 downto 0);
  signal test_done_2            : boolean := false;
begin
  test_done <= test_done_1 and test_done_2;

  cache_1: entity work.axi_4_cache
  generic map ( LINESIZELOG2B => LINESIZEL2B_1,
                RVEXWORDSIZELOG2B => WORDSIZEL2B_1,
                AXIWORDSIZELOG2B => AXISIZEL2B_1,
                ADDRWIDTHLOG2 => ADDRWIDTHL2_1,
                LINECOUNTLOG2 => LINECOUNTL2_1,
                BLOCKCOUNTLOG2 => BLOCKCOUNTL2_1)
  port map ( clk => clk,
             reset => reset_1,
             address => addr_1,
             data_in_rvex => dataIn_1,
             data_in_axi => dataInAxi_1,
             mask_rvex => maskRvex_1,
             data_out => dataOut_1,
             ready => ready_1,
             valid => valid_1,
             read => read_1,
             write => write_1,
             write_line => writeLine_1,
             hit => hit_1,
             busy => busy_1 );

  cache_2: entity work.axi_4_cache
  generic map ( LINESIZELOG2B => LINESIZEL2B_2,
                RVEXWORDSIZELOG2B => WORDSIZEL2B_2,
                AXIWORDSIZELOG2B => AXISIZEL2B_2,
                ADDRWIDTHLOG2 => ADDRWIDTHL2_2,
                LINECOUNTLOG2 => LINECOUNTL2_2,
                BLOCKCOUNTLOG2 => BLOCKCOUNTL2_2)
  port map ( clk => clk,
             reset => reset_2,
             address => addr_2,
             data_in_rvex => dataIn_2,
             data_in_axi => dataInAxi_2,
             mask_rvex => maskRvex_2,
             data_out => dataOut_2,
             ready => ready_2,
             valid => valid_2,
             read => read_2,
             write => write_2,
             write_line => writeLine_2,
             hit => hit_2,
             busy => busy_2 );

  test_1: process
  begin
    -- Sensible defaults
    reset_1 <= '1';
    addr_1 <= (others => '0');
    dataIn_1 <= (others => '0');
    dataInAxi_1 <= (others => '0');
    valid_1 <= '0';
    read_1 <= '0';
    write_1 <= '0';
    writeLine_1 <= '0';
    maskRvex_1 <= X"F";
    test_done_1 <= true;
    wait;
  end process;

  test_2: process
  begin
    -- Sensible defaults
    reset_2 <= '1';
    addr_2 <= (others => '0');
    dataIn_2 <= (others => '0');
    dataInAxi_2 <= (others => '0');
    valid_2 <= '0';
    read_2 <= '0';
    write_2 <= '0';
    writeLine_2 <= '0';
    maskRvex_2 <= X"F";
    wait for CLOCK_PERIOD;
    reset_2 <= '0';
    addr_2 <= X"AB000000";
    read_2 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_2 = '0';
    assert hit_2 = '0';
    read_2 <= '0';
    addr_2 <= X"AB000000";
    writeLine_2 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_2 = '1';
    assert ready_2 = '0';
    wait for CLOCK_PERIOD;
    assert busy_2 = '0';
    assert ready_2 = '1';
    dataInAxi_2 <= X"0202020201010101";
    writeLine_2 <= '0';
    valid_2 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_2 = '0';
    assert ready_2 = '0';
    valid_2 <= '0';
    wait for CLOCK_PERIOD;
    assert ready_2 <= '1';
    dataInAxi_2 <= X"0404040403030303";
    valid_2 <= '1';
    addr_2 <= X"AB000020";
    writeLine_2 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_2 = '1';
    assert ready_2 = '0';
    valid_2 <= '0';
    wait for CLOCK_PERIOD;
    assert busy_2 = '1';
    assert ready_2 = '1';
    dataInAxi_2 <= X"0606060605050505";
    valid_2 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_2 = '1';
    assert ready_2 = '0';
    valid_2 <= '0';
    wait for CLOCK_PERIOD;
    assert busy_2 = '1';
    assert ready_2 = '1';
    dataInAxi_2 <= X"0808080807070707";
    valid_2 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_2 = '1';
    assert ready_2 = '0';
    valid_2 <= '0';
    wait for CLOCK_PERIOD;
    assert busy_2 = '1';
    wait for CLOCK_PERIOD;
    assert busy_2 = '0';
    assert ready_2 = '1';
    writeLine_2 <= '0';
    addr_2 <= X"AB000000";
    read_2 <= '1';
    valid_2 <= '1';
    dataInAxi_2 <= X"0A0A0A0A09090909";
    wait for CLOCK_PERIOD;
    assert busy_2 = '0';
    assert ready_2 = '0';
    assert hit_2 = '1';
    assert dataOut_2 = X"01010101";
    addr_2 <= X"AB000003";
    read_2 <= '1';
    valid_2 <= '0';
    wait for CLOCK_PERIOD;
    assert busy_2 = '0';
    assert ready_2 = '1';
    assert hit_2 = '1';
    assert dataOut_2 = X"01010101";
    valid_2 <= '1';
    dataInAxi_2 <= X"0C0C0C0C0B0B0B0B";
    addr_2 <= X"AB000004";
    read_2 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_2 = '0';
    assert ready_2 = '0';
    assert hit_2 = '1';
    assert dataOut_2 = X"02020202";
    valid_2 <= '0';
    addr_2 <= X"AB00000C";
    read_2 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_2 = '0';
    assert ready_2 = '1';
    assert hit_2 = '1';
    assert dataOut_2 = X"04040404";
    valid_2 <= '1';
    dataInAxi_2 <= X"0E0E0E0E0D0D0D0D";
    read_2 <= '0';
    addr_2 <= X"AB000020";
    read_2 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_2 = '1';
    assert ready_2 = '0';
    valid_2 <= '0';
    wait for CLOCK_PERIOD;
    assert busy_2 = '1';
    assert ready_2 = '1';
    valid_2 <= '1';
    dataInAxi_2 <= X"101010100F0F0F0F";
    wait for CLOCK_PERIOD;
    assert busy_2 = '1';
    assert ready_2 = '0';
    valid_2 <= '0';
    wait for CLOCK_PERIOD;
    assert busy_2 = '0';
    assert hit_2 = '1';
    assert dataOut_2 = X"09090909";
    addr_2 <= X"AB000024";
    dataIn_2 <= X"12121212";
    read_2 <= '0';
    write_2 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_2 = '1';
    wait for CLOCK_PERIOD;
    assert busy_2 = '0';
    write_2 <= '0';
    read_2 <= '1';
    wait for CLOCK_PERIOD;
    assert busy_2 = '1';
    wait for CLOCK_PERIOD;
    assert busy_2 = '0';
    assert dataOut_2 = X"12121212";
    read_2 <= '0';
    test_done_2 <= true;
    wait;
  end process;
end Behavioral;
