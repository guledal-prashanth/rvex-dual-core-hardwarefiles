library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_main is
  end tb_main;


architecture Behavioral of tb_main is
  constant CLOCK_PERIOD             : time := 20 ns;

  signal clk                        : std_logic                     := '1';
  signal testDone                   : boolean;

  signal readerTestDone             : boolean                       := false;
  constant READERTESTENABLED        : boolean                       := true;

  signal addressTransTestDone       : boolean                       := false;
  constant ADDRESSTRANSENABLED      : boolean                       := true;

  signal writer1TestDone            : boolean                       := false;
  constant WRITER1TESTENABLED       : boolean                       := true;

  signal writeManagerTestDone       : boolean                       := false;
  constant WRITEMANAGERTESTENABLED  : boolean                       := true;

  signal xoroshiro128TestDone       : boolean                       := false;
  constant XOROSHIRO128TESTENABLED  : boolean                       := true;

  signal cacheBlockTestDone         : boolean                       := false;
  constant CACHEBLOCKTESTENABLED    : boolean                       := true;

  signal cacheTestDone              : boolean                       := false;
  constant CACHETESTENABLED         : boolean                       := true;

  signal readManagerTestDone        : boolean                       := false;
  constant READMANAGERTESTENABLED   : boolean                       := true;

  signal axi4RWMutexTestDone        : boolean                       := false;
  constant AXI4RWMUTEXTESTENABLED   : boolean                       := true;

  signal toplevelTestDone           : boolean                       := false;
  constant TOPLEVELTESTENABLED      : boolean                       := true;
Begin

  testDone <= (readerTestDone or not READERTESTENABLED) and
               (addressTransTestDone or not ADDRESSTRANSENABLED) and
               (writer1TestDone or not WRITER1TESTENABLED) and
               (writeManagerTestDone or not WRITEMANAGERTESTENABLED) and
               (xoroshiro128TestDone or not XOROSHIRO128TESTENABLED) and
               (cacheBlockTestDone or not CACHEBLOCKTESTENABLED) and
               (cacheTestDone or not CACHETESTENABLED) and
               (readManagerTestDone or not READMANAGERTESTENABLED) and
               (axi4RWMutexTestDone or not AXI4RWMUTEXTESTENABLED) and
               (toplevelTestDone or not TOPLEVELTESTENABLED);
  -- Clk generator, simply switch flanks every half period
  clock_gen : process
  begin
    if not (testDone) then
      -- 1/2 duty cycle
      clk <= not clk;
      wait for CLOCK_PERIOD/2;
    else
      wait;
    end if;
  end process;

  reader_1: if READERTESTENABLED generate
    reader_1: entity work.tb_axi_reader1
    generic map ( CLOCK_PERIOD => CLOCK_PERIOD )
    port map (  clk => clk,
                test_done => readerTestDone );
  end generate reader_1;

  writer_1: if WRITER1TESTENABLED generate
    writer_1: entity work.tb_axi_writer1
    generic map ( CLOCK_PERIOD => CLOCK_PERIOD )
    port map (  clk => clk,
                test_done => writer1TestDone );
  end generate writer_1;

  addrTrans: if ADDRESSTRANSENABLED generate
    addrTrans: entity work.tb_address_translator
    generic map ( CLOCK_PERIOD => CLOCK_PERIOD )
    port map (  clk => clk,
                test_done => addressTransTestDone );
  end generate addrTrans;

  writeManager: if WRITEMANAGERTESTENABLED generate
    writeManager: entity work.tb_write_manager
    generic map ( CLOCK_PERIOD => CLOCK_PERIOD )
    port map (  clk => clk,
                test_done => writeManagerTestDone );
  end generate writeManager;

  xoroshiro128 : if XOROSHIRO128TESTENABLED generate
    xoroshiro128: entity work.tb_rngxoroshiro128plus
    generic map ( CLOCK_PERIOD => CLOCK_PERIOD )
    port map (  clk => clk,
                test_done => xoroshiro128TestDone );
  end generate xoroshiro128;

  cacheBlock: if CACHEBLOCKTESTENABLED generate
    cacheBlock: entity work.tb_cache_block
    generic map ( CLOCK_PERIOD => CLOCK_PERIOD )
    port map (  clk => clk,
                test_done => cacheBlockTestDone );
  end generate cacheBlock;

  cache: if CACHETESTENABLED generate
    cache: entity work.tb_cache
    generic map ( CLOCK_PERIOD => CLOCK_PERIOD )
    port map (  clk => clk,
                test_done => cacheTestDone );
  end generate cache;

  read_manager: if READMANAGERTESTENABLED generate
    read_manager: entity work.tb_read_manager
    generic map(  CLOCK_PERIOD => CLOCK_PERIOD)
    port map( clk => clk,
              test_done => readManagerTestDone);
  end generate read_manager;

  axi4rwMutex: if AXI4RWMUTEXTESTENABLED generate
    read_manager: entity work.tb_axi4_rw_mutex
    generic map(  CLOCK_PERIOD => CLOCK_PERIOD)
    port map( clk => clk,
              test_done => axi4RWMutexTestDone);
  end generate axi4rwMutex;

  toplevel: if TOPLEVELTESTENABLED generate
    toplevel: entity work.tb_toplevel
    generic map(  CLOCK_PERIOD => CLOCK_PERIOD )
    port map( clk => clk,
              test_done => toplevelTestDone);
  end generate toplevel;
end Behavioral;
