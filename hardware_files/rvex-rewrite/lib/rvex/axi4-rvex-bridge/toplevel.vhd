library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common_pkg.all;
use work.bus_pkg.all;

entity pvexAxiSlaveToplevel is
  generic (
    ADDRESSALIGNMENTMASK        : natural range 0 to 128 := 13;
    AXI_ADDRESSWIDTHLOG2B       : natural range 5 to 7 := 5;
    AXI_DATAWIDTHLOG2B          : natural range 5 to 255 := 6;
    CACHE_LINECOUNTLOG2         : natural range 0 to 64 := 4;
    CACHE_LINESIZELOG2B         : natural range 6 to 255 := 8;
    CACHE_BLOCKCOUNTLOG2        : natural range 0 to 255 := 2
  );
  port (
    clk                         : in  std_logic;
    rst                         : in  std_logic;
    -- Address translation inout
    updateAddrBase              : in  std_logic;
    baseAddrIn                  : in  std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
    -- The rvex inouts
    rvex_bus_in                 : in  bus_mst2slv_type;
    rvex_bus_out                : out bus_slv2mst_type;
    -- The AXI4 bus interaction
    -- Global Signals
    maxi_aclk                   : out std_logic;
    -- No reset
    -- Write address channel signals
    maxi_awid                   : out std_logic_vector(2 DOWNTO 0);
    maxi_awaddr                 : out std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
    maxi_awlen                  : out std_logic_vector(3 downto 0);
    maxi_awsize                 : out std_logic_vector(2 downto 0);
    maxi_awburst                : out std_logic_vector(1 downto 0);
    maxi_awlock                 : out std_logic_vector(1 downto 0);
    maxi_awcache                : out std_logic_vector(3 downto 0);
    maxi_awprot                 : out std_logic_vector(2 downto 0);
    maxi_awqos                  : out std_logic_vector(3 downto 0);
    maxi_awuser                 : out std_logic_vector(4 downto 0);
    maxi_awvalid                : out std_logic;
    maxi_awready                : in  std_logic;
    -- Write data channel signals
    maxi_wid                    : out std_logic_vector(2 downto 0);
    maxi_wdata                  : out std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0);
    maxi_wstrb                  : out std_logic_vector(2**(AXI_DATAWIDTHLOG2B - 3) - 1 downto 0);
    maxi_wlast                  : out std_logic;
    maxi_wvalid                 : out std_logic;
    maxi_wready                 : in  std_logic;
    --  Write response channel signals
    maxi_bid                    : in  std_logic_vector(2 downto 0);
    maxi_bresp                  : in  std_logic_vector(1 downto 0);
    maxi_bvalid                 : in  std_logic;
    maxi_bready                 : out std_logic;
    --  Read address channel signals
    maxi_arid                   : out std_logic_vector(2 downto 0);
    maxi_araddr                 : out std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
    maxi_arlen                  : out std_logic_vector(3 downto 0);
    maxi_arsize                 : out std_logic_vector(2 downto 0);
    maxi_arburst                : out std_logic_vector(1 downto 0);
    maxi_arlock                 : out std_logic_vector(1 downto 0);
    maxi_arcache                : out std_logic_vector(3 downto 0);
    maxi_arprot                 : out std_logic_vector(2 downto 0);
    maxi_arqos                  : out std_logic_vector(3 downto 0);
    maxi_aruser                 : out std_logic_vector(4 downto 0);
    maxi_arvalid                : out std_logic;
    maxi_arready                : in  std_logic;
    -- Read data channel signals
    maxi_rid                    : in  std_logic_vector(2 downto 0);
    maxi_rdata                  : in  std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0);
    maxi_rresp                  : in  std_logic_vector(1 downto 0);
    maxi_rlast                  : in  std_logic;
    maxi_rvalid                 : in  std_logic;
    maxi_rready                 : out std_logic;
    -- Pseudo-random number generator
    prng_newseed                : in  std_logic_vector(127 downto 0);
    prng_reseed                 : in  std_logic
  );
end pvexAxiSlaveToplevel;

architecture Behavioral of pvexAxiSlaveToplevel is
  signal writer_isActive               : std_logic;
  signal reader_isActive               : std_logic;
  signal writer_isBlocked              : std_logic;
  signal reader_isBlocked              : std_logic;
  signal writer_curAddress             : std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
  signal reader_curAddress             : std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
  signal cache_writeBusy               : std_logic;
  signal cache_hit                     : std_logic;

  signal writer_readData               : rvex_data_type;
  signal writer_fault                  : std_logic;
  signal writer_busy                   : std_logic;
  signal writer_ack                    : std_logic;

  signal reader_readData               : rvex_data_type;
  signal reader_fault                  : std_logic;
  signal reader_busy                   : std_logic;
  signal reader_ack                    : std_logic;
  signal reader_writeEnable            : std_logic;

  signal rvex_data_endianSwapped       : rvex_data_type;
  signal rvex_mask_endianSwapped       : rvex_mask_type;
  signal translatorInput               : std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
  signal translatedAddress             : std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);

  signal delay2system                  : bus_mst2slv_type;
  signal system2delay                  : bus_slv2mst_type;
begin
  -- Drive the driverless
  maxi_awid <= (others => '0');
  maxi_awlock <= (others => '0');
  maxi_awprot <= (others => '0');
  maxi_awqos <= (others => '0');
  maxi_wid <= (others => '0');
  maxi_arid <= (others => '0');
  maxi_arlock <= (others => '0');
  maxi_arprot <= (others => '0');
  maxi_arqos <= (others => '0');
  -- Readers' readenable needs to be suspended while the writer is working on it
  reader_writeEnable <= delay2system.writeEnable when writer_busy = '0' else '0';
  translatorInput <= baseAddrIn when updateAddrBase = '1' else std_logic_vector(resize(unsigned(delay2system.address), 2**AXI_ADDRESSWIDTHLOG2B));

  -- The halfDelay unit
  delay: entity work.bus_halfStage
  port map  ( clk => clk,
              clkEn => '1',
              reset => rst,
              mst2stage => rvex_bus_in,
              stage2mst => rvex_bus_out,
              stage2slv => delay2system,
              slv2stage => system2delay );

  -- The translator
  addrTrans: entity work.address_translator
  generic map (ADDRESSWIDTHL2B => AXI_ADDRESSWIDTHLOG2B )
  port map    ( clk => clk,
                set_address => updateAddrBase,
                addr_in => translatorInput,
                addr_out => translatedAddress );

  -- Instantiate a write manager
  writeManager: entity work.write_manager
  generic map(
    AXI_ADDRESSWIDTHLOG2B => AXI_ADDRESSWIDTHLOG2B,
    AXI_DATAWIDTHLOG2B => AXI_DATAWIDTHLOG2B,
    SUPPRESSFAULT => true
  )
  port map(
    clk => clk,
    rst => rst,
    isActive => writer_isActive,
    curAddress => writer_curAddress,
    maxi_awaddr => maxi_awaddr,
    maxi_awlen => maxi_awlen,
    maxi_awsize => maxi_awsize,
    maxi_awcache => maxi_awcache,
    maxi_awuser => maxi_awuser,
    maxi_awburst => maxi_awburst,
    maxi_awvalid => maxi_awvalid,
    maxi_awready => maxi_awready,
    maxi_wdata => maxi_wdata,
    maxi_wstrb => maxi_wstrb,
    maxi_wlast => maxi_wlast,
    maxi_wvalid => maxi_wvalid,
    maxi_wready => maxi_wready,
    maxi_bresp => maxi_bresp,
    maxi_bvalid => maxi_bvalid,
    maxi_bready => maxi_bready,
    addrIn => translatedAddress,
    writeData => rvex_data_endianSwapped,
    readData => writer_readData,
    writeMask => rvex_mask_endianSwapped,
    writeEnable => delay2system.writeEnable,
    fault => writer_fault,
    busy => writer_busy,
    cache_writeBusy => cache_writeBusy,
    ack => writer_ack,
    cache_hit => cache_hit
  );
  -- Instantiate a read manager
  readManager: entity work.read_manager
  generic map(  AXI_ADDRESSWIDTHLOG2B => AXI_ADDRESSWIDTHLOG2B,
                AXI_DATAWIDTHLOG2B => AXI_DATAWIDTHLOG2B,
                CACHE_LINECOUNTLOG2 => CACHE_LINECOUNTLOG2,
                CACHE_LINESIZELOG2B => CACHE_LINESIZELOG2B,
                CACHE_BLOCKCOUNTLOG2 => CACHE_BLOCKCOUNTLOG2 )
  port map( clk => clk,
            rst => rst,
            isBlocked => reader_isBlocked,
            maxi_araddr => maxi_araddr,
            maxi_arlen => maxi_arlen,
            maxi_arsize => maxi_arsize,
            maxi_arburst => maxi_arburst,
            maxi_arcache => maxi_arcache,
            maxi_aruser => maxi_aruser,
            maxi_arvalid => maxi_arvalid,
            maxi_arready => maxi_arready,
            maxi_rdata => maxi_rdata,
            maxi_rresp => maxi_rresp,
            maxi_rlast => maxi_rlast,
            maxi_rvalid => maxi_rvalid,
            maxi_rready => maxi_rready,
            addrIn => translatedAddress,
            writeData => rvex_data_endianSwapped,
            readData => reader_readData,
            writeMask => rvex_mask_endianSwapped,
            writeEnable => reader_writeEnable,
            readEnable => delay2system.readEnable,
            cache_writeBusy => cache_writeBusy,
            fault => reader_fault,
            busy => reader_busy,
            ack => reader_ack,
            hit => cache_hit,
            prng_newseed => prng_newseed,
            prng_reseed => prng_reseed
          );

  -- The r/w mutex
  axi4_rw_mutex: entity work.axi4_rw_mutex
  generic map(  AXI_ADDRESSWIDTHLOG2B => AXI_ADDRESSWIDTHLOG2B,
                LINESIZELOG2B => CACHE_LINESIZELOG2B)
  port map(  write_inputAddr => writer_curAddress,
            read_inputAddr => translatedAddress,
            write_isActive => writer_isActive,
            read_blocked => reader_isBlocked );
  -- Endian swap input data
  rvex_data_endianSwapped <= delay2system.writeData(7 downto 0) & delay2system.writeData(15 downto 8) & delay2system.writeData(23 downto 16) & delay2system.writeData(31 downto 24);
  rvex_mask_endianSwapped <= (  0 => delay2system.writeMask(3),
                                1 => delay2system.writeMask(2),
                                2 => delay2system.writeMask(1),
                                3 => delay2system.writeMask(0) );
  -- Endian swap output data
  system2delay.readData <= reader_readData(7 downto 0) &
                                 reader_readData(15 downto 8) &
                                 reader_readData(23 downto 16) &
                                 reader_readData(31 downto 24);

  -- Connect the output signals
  maxi_aclk <= clk;
  system2delay.fault <= '0';
  system2delay.busy <= writer_busy or reader_busy;
  system2delay.ack <= writer_ack or reader_ack;

  assert(writer_busy /= '1' or reader_busy /= '1');
  assert(writer_busy /= '1' or writer_ack /= '1');
  assert(writer_busy /= '1' or reader_ack /= '1');
  assert(writer_ack /= '1' or reader_busy /= '1');
  assert(writer_ack /= '1' or reader_ack /= '1');
  assert(reader_busy /= '1' or reader_ack /= '1');


end Behavioral;
