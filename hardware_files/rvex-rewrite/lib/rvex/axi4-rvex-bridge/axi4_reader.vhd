library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- =================================================
-- The AXI4 reader. The sizes of the lines can be specified using generics.
-- To use it: set all inputs and then set read_valid.
-- You have to keep all inputs stable for as long as read_ready is low
-- You can read the output in the first cycle where read_ready is high.
-- Read ready will stay high until read_valid is de-asserted.

-- When transacting multiple words, ready will rise when the first word is ready,
-- then, when valid is deasserted

-- read_length is the amount of reads that is done. 1 is always added to the input
-- ================================================

entity axi4_reader is
  generic (
    AXI_DATAWIDTHLOG2B          : natural range 5 to 7 := 5;
    AXI_ADDRESSWIDTHLOG2B       : natural range 5 to 7 := 6
  );
  port (
    clk                         : in  std_logic;
    rst                         : in  std_logic;
    -- Read din channel: from extern to intern
    read_din_addr               : in  std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 2);
    read_din_valid              : in  std_logic;
    read_din_length             : in  std_logic_vector(3 downto 0);
    read_din_ready              : out std_logic;
    -- Read dout channel: from intern to extern
    read_dout_data              : out std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0);
    read_dout_result            : out std_logic_vector(1 downto 0);
    read_dout_valid             : out std_logic;
    read_dout_ready             : in  std_logic;
    -- interconn: interconnect between read and write on AXI
    isBlocked                   : in  std_logic;
    curAddress                  : out std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
    -- Read address channel signals
    maxi_araddr                 : out std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
    maxi_arlen                  : out std_logic_vector(3 downto 0);
    maxi_arsize                 : out std_logic_vector(2 downto 0);
    maxi_arburst                : out std_logic_vector(1 downto 0);
    maxi_arcache                : out std_logic_vector(3 downto 0);
    maxi_aruser                 : out std_logic_vector(4 downto 0);
    maxi_arvalid                : out std_logic;
    maxi_arready                : in  std_logic;
    -- Read data channel signals
    maxi_rdata                  : in  std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0);
    maxi_rresp                  : in  std_logic_vector(1 downto 0);
    maxi_rlast                  : in  std_logic;
    maxi_rvalid                 : in  std_logic;
    maxi_rready                 : out std_logic
  );
end axi4_reader;

architecture behavioral of axi4_reader is
  type state_type is (din_ready, address_transmission, data_transmission, completing);
  signal cur_state            : state_type := din_ready;
  signal next_state           : state_type := din_ready;
  signal updateCurAddress     : boolean := false;
  signal curAddressMem        : std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);

begin
  -- See tech ref page 103. ARCACHE and AWCACHE control wether or not the processor cache is involved in this transaction
  -- For now, they are set to 0, no cache involvement.
  maxi_arcache <= (others => '0');
  maxi_aruser <= (others => '0');
  -- Always wrapping, so lock it down
  maxi_arburst <= "10";
  -- Always request maximum size blocks
  maxi_arsize  <= std_logic_vector(to_unsigned(AXI_DATAWIDTHLOG2B - 3, maxi_arsize'length));
  -- The output address is aligned on the size of the databus: 64 bit if the databus is 64 bits wide, etc.
  maxi_araddr                 <= read_din_addr(read_din_addr'left downto AXI_DATAWIDTHLOG2B - 3) &
                                 (AXI_DATAWIDTHLOG2B - 4 downto 0 => '0');
  read_dout_data              <= maxi_rdata;
  read_dout_result            <= maxi_rresp;
  maxi_arlen                  <= read_din_length;
  curAddress <= read_din_addr & "00" when updateCurAddress else curAddressMem;

  -- Updates the cur address output variable
  coherency_proc: process(clk)
  begin
    if rising_edge(clk) then
      if updateCurAddress then
        curAddressMem <= read_din_addr & "00";
      end if;
    end if;
  end process;


  -- Handles the cur_state variable
  sync_proc : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        cur_state <= din_ready;
      else
        cur_state <= next_state;
      end if;
    end if;
  end process;

  -- handles the next_state variable
  state_decider : process(cur_state, maxi_arready, maxi_rlast, maxi_rvalid, read_din_valid, read_dout_ready, isBlocked)
  begin
    next_state <= cur_state;
    case cur_state is
      when din_ready =>
        if isBlocked = '0' and read_din_valid = '1' then
          next_state <= address_transmission;
        end if;
      when address_transmission =>
        if maxi_arready = '1' then
          if read_dout_ready = '1' and maxi_rvalid = '1' and maxi_rlast = '1' then
            next_state <= completing;
          else
            next_state <= data_transmission;
          end if;
        end if;
      when data_transmission =>
        if read_dout_ready = '1' and maxi_rvalid = '1' and maxi_rlast = '1' then
          next_state <= completing;
        end if;
      when completing =>
        if read_dout_ready = '0' and maxi_rvalid = '0' then
          next_state <= din_ready;
        end if;
    end case;
  end process;

  -- The state decides the output
  output_decider: process(cur_state, read_din_valid, read_dout_ready, maxi_arready, maxi_rvalid)
  begin
    case cur_state is
      when din_ready=>
        maxi_arvalid            <= '0';
        maxi_rready             <= '0';
        read_din_ready          <= '0';
        read_dout_valid         <= '0';
        updateCurAddress        <= true;
      when address_transmission =>
        maxi_arvalid            <= read_din_valid;
        maxi_rready             <= '0';
        read_din_ready          <= maxi_arready;
        read_dout_valid         <= '0';
        updateCurAddress        <= false;
      when data_transmission    =>
        maxi_arvalid            <= '0';
        maxi_rready             <= read_dout_ready;
        read_din_ready          <= '0';
        read_dout_valid         <= maxi_rvalid;
        updateCurAddress        <= false;
      when completing =>
        maxi_arvalid            <= '0';
        maxi_rready             <= read_dout_ready;
        read_din_ready          <= '0';
        read_dout_valid         <= maxi_rvalid;
        updateCurAddress        <= true;
    end case;
  end process;
end Behavioral;

