library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- This is a cache block, it is a set of lines which are addressed.
-- When a new address enters, the tag of the corresponding line is checked. If they are equal, we have a hit.

-- Lets talk about addresses. Assume:
-- *Address is 32 bit (byte addressed)
-- *Line size is 256 bit = 2^8 (32 byte)
-- *rVex block size is 32 bit = 2^5 (4 byte)
-- *AXI block size is 64 bit = 2^6  (8 byte)
-- *There are 4 cache lines, 2^2
-- Address layout
-- | Tag |  line index | line-internal subaddress |
-- To determine the line we ignore the first 7 bits (LSB): 2 of those select the block and thus are already known and 5 are irrelevant because the lines are aligned to 32 byte
-- The next 2 bits determine which line we are looking at.
-- To determine the AXI block we need 2 relevant bits from the first 5, the 2 MSB's
-- Similarly, the rVex block needs 3 relevant bits, the 3 MSB's of the first 5 bit
-- Our requirement now is that the line size is bigger then the word size of both the rVex and the AXI.

-- A Line consists of a multiple of words. A word is requested from the rvex.
-- An AXI word is supposed to be a multiple of a rVex word.

entity cache_block is
  generic ( LINESIZELOG2B               : natural range 6 to 255;
            RVEXWORDSIZELOG2B           : natural range 5 to 255;
            AXIWORDSIZELOG2B            : natural range 5 to 255;
            ADDRWIDTHLOG2               : natural range 5 to 255;
            LINECOUNTLOG2               : natural range 0 to 64 );
  port (  clk                         : in  std_logic;
          reset                       : in  std_logic;
          -- The address from the rVexs' perspective
          address                     : in  std_logic_vector(2**ADDRWIDTHLOG2 - 1 downto 0);
          -- Two data ins: we want to be able to use the simultaniously:
          -- Write a line whilst updating a word from another line
          data_in_axi                 : in  std_logic_vector(2**AXIWORDSIZELOG2B - 1 downto 0);
          data_in_rvex                : in  std_logic_vector(2**RVEXWORDSIZELOG2B - 1 downto 0);
          -- Since the rVex protocol also supports a mask
          mask_rvex                   : in  std_logic_vector(2**(RVEXWORDSIZELOG2B - 3) - 1 downto 0);
          -- Only rVex sized words ever come out here
          data_out                    : out std_logic_vector(2**RVEXWORDSIZELOG2B - 1 downto 0);
          -- Ready/valid pair used in AXI4 communication
          ready                       : out std_logic;
          valid                       : in  std_logic;
          -- Signals single rVex sized word read
          read                        : in  std_logic;
          -- Signals single rVex sized word write
          write                       : in  std_logic;
          -- Signals that an entire line is going to be written with axi4 sized words
          -- This signal is supposed to be asserted directly after an attempt to read:
          -- Address should still be stable!
          write_line                  : in  std_logic;
          -- Hit!
          hit                         : out std_logic;
          -- If busy='1' this block is not ready, else it is ready.
          -- A cache block will store the address if required, so there is no need to keep address stable during busy = '1'
          busy                        : out std_logic;
          -- Signals that the currently selected line is diry and thus preferable to "evict"
          curLineDirty                : out std_logic;
          -- A special signal to signal that a block is writing a line and thus needs exclusive access to the ready/valid signal pair
          -- This also allows the cache to prevent to blocks from writing at the same time.
          linewriter_busy             : out std_logic;
          linewriter_active           : in  std_logic );
end cache_block;

architecture Behavioral of cache_block is
  constant LINECOUNT            : natural := 2**LINECOUNTLOG2;
  constant RVEXWORDSPERLINE     : natural := 2**(LINESIZELOG2B - RVEXWORDSIZELOG2B);
  constant AXIWORDSPERLINE      : natural := 2**(LINESIZELOG2B - AXIWORDSIZELOG2B);
  constant RVEXWORDCOUNTL2      : natural := LINECOUNTLOG2 + LINESIZELOG2B - RVEXWORDSIZELOG2B;
  constant AXIWORDCOUNTL2       : natural := LINECOUNTLOG2 + LINESIZELOG2B - AXIWORDSIZELOG2B;
  constant RVEXWORDSPERAXIWORD  : natural := 2**(AXIWORDSIZELOG2B - RVEXWORDSIZELOG2B);
  constant SEARCHTAGADDRBITS    : natural := 2**ADDRWIDTHLOG2 - LINESIZELOG2B +3 - LINECOUNTLOG2;
  constant WORDSIZE             : natural := 2**RVEXWORDSIZELOG2B;
  constant BYTESPERRVEXWORD     : natural := 2**(RVEXWORDSIZELOG2B - 3);
  constant BYTESPERAXIWORD      : natural := 2**(AXIWORDSIZELOG2B - 3);
  -- Every block has a FSM
  type state_type is (ready_state, ready_writeWord, write_word_checkHit, write_word_lineWriter_wait, write_line_start, write_line_wait, read_wait, read_delayCycle);
  subtype line_data_type is std_logic_vector(2**AXIWORDSIZELOG2B - 1 downto 0);
  subtype tag_data_type is std_logic_vector(SEARCHTAGADDRBITS - 1 downto 0);
  -- An array of data lines
  type line_data_array is array (natural range <>) of line_data_type;
  type tag_data_array is array (natural range <>) of tag_data_type;
  subtype cache_line_dataset is line_data_array(0 to 2**AXIWORDCOUNTL2 - 1);
  subtype tag_dataset is tag_data_array(0 to LINECOUNT - 1);

  signal curState               : state_type := ready_state;
  signal nextState              : state_type := ready_state;
  -- The part of the address that represents this unique block, the tag
  signal searchTag              : std_logic_vector(SEARCHTAGADDRBITS - 1 downto 0);
  -- When a whole line is written, it starts by reading the requested block from the memory to make sure the rVex waits as short as possible.
  -- That is why this variable is required
  signal lineIndexAxiPart       : std_logic_vector(LINESIZELOG2B - AXIWORDSIZELOG2B - 1 downto 0);
  signal axiBlockIndex          : std_logic_vector(LINESIZELOG2B + LINECOUNTLOG2 - 4 downto AXIWORDSIZELOG2B - 3);
  -- We do not request a whole line, we request a part of it. Which part is determined by this part of the address
  signal rvexSubIndex           : std_logic_vector(AXIWORDSIZELOG2B - 4 downto RVEXWORDSIZELOG2B - 3);
  -- The index of the line where we expect the hit to happen
  signal lineIndex              : std_logic_vector(LINECOUNTLOG2 - 1 downto 0);
  -- Line writer interaction signals
  signal lineWriter_start       : std_logic;
  signal lineWriter_done        : std_logic;
  signal lineWriter_writeHit    : std_logic;
  signal lineWriter_curTag      : std_logic_vector(searchTag'RANGE);
  signal lineWriter_curLine     : std_logic_vector(lineIndex'RANGE);
  signal lineWriter_curAddr     : natural range cache_line_dataset'RANGE;
  signal lineWriter_writeEn     : std_logic_vector(BYTESPERAXIWORD - 1 downto 0);
  signal lineWriter_writing     : std_logic;
  signal lineWriter_curData     : line_data_type;
  -- Word writer control signal
  signal wordWriter_start       : std_logic;
  -- Replica of the hit signal, since that one is required on multiple places
  signal internal_hit           : std_logic;
  -- BRAM signals: cache_data is a BRAM
  signal cache_data_writeAddress: natural range cache_line_dataset'RANGE;
  signal cache_data_readAddress : natural range cache_line_dataset'RANGE;
  signal cache_data_writeEnable : std_logic_vector(BYTESPERAXIWORD - 1 downto 0);
  signal cache_data_dataIn      : line_data_type;
  signal cache_data_dataOut     : line_data_type;
  signal cache_data             : cache_line_dataset := (others => (others => 'X'));

  signal rvexWordWrite_writeEnable  : std_logic_vector(cache_data_writeEnable'RANGE);
  signal rvexWordWrite_writeData    : std_logic_vector(cache_data_dataIn'RANGE);
  signal rvexWordWrite_writeAddress : natural range cache_line_dataset'RANGE;
  signal rvexWordWrite_writeHit     : std_logic;
  signal startVector                : std_logic_vector(1 downto 0);

  signal tag_data_writeAddress      : natural range tag_dataset'RANGE;
  signal tag_data_readAddress       : natural range tag_dataset'RANGE;
  signal tag_data_writeEnable       : std_logic;
  signal tag_data_dataIn            : tag_data_type;
  signal tag_data_dataOut           : tag_data_type;
  signal tag_data                   : tag_dataset := (others => (others => 'X'));

  signal dirtyLine                  : std_logic_vector(LINECOUNT - 1 downto 0) := (others => '1');
  signal dirty                      : std_logic;

  -- Declare XST RAM extraction hints.
  attribute ram_extract       : string;
  attribute ram_style         : string;

  -- Hints for XST to implement the data memory and tag memory in block RAMs.
  attribute ram_extract of cache_data : signal is "yes";
  attribute ram_style   of cache_data : signal is "block";
  attribute ram_extract of tag_data   : signal is "yes";
  attribute ram_style   of tag_data   : signal is "block";


begin

  -- Some asserts to prevent issues
  assert LINECOUNTLOG2 + SEARCHTAGADDRBITS + LINESIZELOG2B - 3 = 2**ADDRWIDTHLOG2;
  assert LINESIZELOG2B > AXIWORDSIZELOG2B;
  assert LINESIZELOG2B > RVEXWORDSIZELOG2B;
  assert AXIWORDSIZELOG2B >= RVEXWORDSIZELOG2B;
  assert not (lineWriter_start = '1' and wordWriter_start = '1');
  -- Signals only depending on input signals
  lineIndex <= address(LINESIZELOG2B - 3 + LINECOUNTLOG2 - 1 downto LINESIZELOG2B - 3);
  searchTag <= address(address'LEFT downto address'LEFT - SEARCHTAGADDRBITS + 1);
  lineIndexAxiPart <= address(LINESIZELOG2B - 4 downto AXIWORDSIZELOG2B - 3);
  axiBlockIndex <= address(LINESIZELOG2B + LINECOUNTLOG2 - 4 downto AXIWORDSIZELOG2B - 3);
  rvexSubIndex <= address(AXIWORDSIZELOG2B - 4 downto RVEXWORDSIZELOG2B - 3);
  -- Signals depending at least partially on internal signals
  hit <= internal_hit;
  linewriter_busy <= not lineWriter_done;
  startVector <= lineWriter_writing & wordWriter_start;
  lineWriter_writeHit <= '1' when lineWriter_curTag = searchTag and lineIndex = lineWriter_curLine and lineWriter_done = '0' else '0';
  cache_data_writeAddress <= lineWriter_curAddr when startVector = "10" else
                             rvexWordWrite_writeAddress when startVector = "01" else
                             0;
  cache_data_readAddress <= to_integer(unsigned(axiBlockIndex));
  cache_data_writeEnable <= lineWriter_writeEn when startVector = "10" else
                            rvexWordWrite_writeEnable when startVector = "01" else
                            (others => '0');
  cache_data_dataIn <= lineWriter_curData when startVector = "10" else
                       rvexWordWrite_writeData when startVector = "01" else
                       (others => '0');
  data_out <= cache_data_dataOut((to_integer(unsigned(rvexSubIndex)) + 1) * 2**RVEXWORDSIZELOG2B - 1 downto to_integer(unsigned(rvexSubIndex))* 2**RVEXWORDSIZELOG2B);
  rvexWordWrite_writeHit <= '1' when wordWriter_start = '1' and rvexWordWrite_writeAddress = cache_data_readAddress else '0';
  tag_data_readAddress <= to_integer(unsigned(lineIndex));
  curLineDirty <= dirty;
  internal_hit <= '1' when tag_data_dataOut = searchTag and dirty = '0' else '0';


  cache_data_writeHandler: process(clk)
  begin
    if rising_edge(clk) then
      for I in 0 to BYTESPERAXIWORD - 1 loop
        if cache_data_writeEnable(I) = '1' then
          cache_data(cache_data_writeAddress)(8*I + 7 downto 8*I) <= cache_data_dataIn(8*I + 7 downto 8*I);
        end if;
      end loop;
      cache_data_dataOut <= cache_data(cache_data_readAddress);
    end if;
  end process;

  tag_data_handler: process(clk)
  begin
    if rising_edge(clk) then
      if tag_data_writeEnable = '1' then
        tag_data(tag_data_writeAddress) <= tag_data_dataIn;
      end if;
      tag_data_dataOut <= tag_data(tag_data_readAddress);
    end if;
  end process;

  dirtyHandler: process(clk)
  begin
    if rising_edge(clk) then
      dirty <= dirtyLine(tag_data_readAddress);
    end if;
  end process;

  writeWordRegisters: process(clk)
  begin
    if rising_edge(clk) then
      rvexWordWrite_writeEnable <= std_logic_vector(shift_left(resize(unsigned(mask_rvex), rvexWordWrite_writeEnable'LENGTH), to_integer(unsigned(rvexSubIndex))*mask_rvex'LENGTH));
      rvexWordWrite_writeData <= std_logic_vector(shift_left(resize(unsigned(data_in_rvex), rvexWordWrite_writeData'LENGTH), to_integer(unsigned(rvexSubIndex))*data_in_rvex'LENGTH));
      rvexWordWrite_writeAddress <= to_integer(unsigned(axiBlockIndex));
    end if;
  end process;

  -- Used to write to the cache
  -- wordWriter_start = '1'<->busy = '1'
  -- wordWriter_start = '1' means that it will write witin the next cycle.
  -- So the address lines etectera should be stable before that
  cache_handler: process(clk, reset)
    variable counter            : unsigned(LINESIZELOG2B - AXIWORDSIZELOG2B - 1 downto 0) := (others => '0');
    variable pointer            : unsigned(LINESIZELOG2B - AXIWORDSIZELOG2B - 1 downto 0) := (others => '0');
    variable final_cycle        : boolean := false;
    variable isActive           : boolean := false;
    variable workingLine        : std_logic_vector(lineIndex'RANGE) := (others => '0');
    variable vReady             : std_logic := '0';
  begin
    if rising_edge(clk) then
      if reset = '1' then
        counter := (others => '0');
        isActive := false;
        final_cycle := false;
        vReady := '0';
        lineWriter_writing <= '0';
        dirtyLine <= (others => '1');
        lineWriter_curAddr <= 0;
        lineWriter_writeEn <= (others => '0');
      else

        if final_cycle then
          isActive := false;
          dirtyLine(to_integer(unsigned(workingLine))) <= '0';
          final_cycle := false;
          tag_data_writeEnable <= '0';
        end if;

        if not isActive then
          lineWriter_writing <= '0';
          lineWriter_curAddr <= 0;
          if lineWriter_start = '1' then
            isActive := true;
            pointer := unsigned(lineIndexAxiPart);
            counter := (others => '0');
            dirtyLine(to_integer(unsigned(workingLine))) <= '1';
            lineWriter_curTag <= searchTag;
            tag_data_dataIn <= searchTag;
            tag_data_writeAddress <= to_integer(unsigned(workingLine));
          else
            workingLine := lineIndex;
            lineWriter_writeEn <= (others => '0');
          end if;
        end if;

        if isActive then
          lineWriter_writing <= '1';
          if vReady = '0' then
            vReady := '1';
            lineWriter_writeEn <= (others => '0');
          elsif valid = '1' then
            lineWriter_curAddr <= to_integer(unsigned(workingLine))*AXIWORDSPERLINE + to_integer(pointer);
            lineWriter_writeEn <= (others => '1');
            lineWriter_curData <= data_in_axi;
            pointer := pointer + 1;
            counter := counter + 1;
            vReady := '0';
          -- If counter = 0, then we have come full circle.
            if counter = 0 then
              final_cycle := true;
              tag_data_writeEnable <= '1';
            end if;
          end if;
        end if;

      end if;   -- reset
    end if;     -- rising_edge
    lineWriter_curLine <= workingLine;
    ready <= vReady;
    if isActive then
      lineWriter_done <= '0';
    else
      lineWriter_done <= '1';
    end if;
  end process;

  -- The FSM, to handle all other signals
  state_transition_determination: process(clk, reset)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        curState <= ready_state;
      else
        curState <= nextState;
      end if;
    end if;
  end process;

  state_decider: process(curState, write, write_line, internal_hit, lineWriter_done, lineWriter_writeHit, rvexWordWrite_writeHit, linewriter_active, read)
  begin
    nextState <= curState;
    case curState is
      when ready_state|ready_writeWord =>
        nextState <= ready_state;
        -- Read situation.
        if read = '1'  then
          if lineWriter_writeHit = '1' then
            nextState <= read_wait;
          elsif rvexWordWrite_writeHit = '1' then
            nextState <= read_delayCycle;
          end if;
        -- Write situation
        elsif write = '1' then
          if lineWriter_writeHit = '1' then
            nextState <= write_word_lineWriter_wait;
          else
            nextState <= write_word_checkHit;
          end if;
        -- Linewrite situation
        elsif write_line = '1' then
          if linewriter_active = '1' then
            nextState <= write_line_wait;
          else
            nextState <= write_line_start;
          end if;
        end if;
      when write_word_checkHit =>
        if internal_hit = '1' then
          if lineWriter_done = '0' then
            nextState <= write_word_lineWriter_wait;
          else
            nextState <= ready_writeWord;
          end if;
        else
          nextState <= ready_state;
        end if;
      when read_wait =>
        if lineWriter_done = '1' then
          nextState <= ready_state;
        end if;
      when write_word_lineWriter_wait =>
        if lineWriter_done = '1' then
          nextState <= ready_writeWord;
        end if;
      when write_line_start =>
        nextState <= ready_state;
      when write_line_wait =>
        if lineWriter_done = '1' and linewriter_active = '0' then
          nextState <= write_line_start;
        end if;
      when read_delayCycle =>
        nextState <= ready_state;
    end case;
  end process;

  output_decider: process(curState)
  begin
    case curState is
      when ready_state|write_word_checkHit =>
        busy <= '0';
        lineWriter_start <= '0';
        wordWriter_start <= '0';
      when ready_writeWord =>
        busy <= '0';
        lineWriter_start <= '0';
        wordWriter_start <= '1';
      when write_word_lineWriter_wait|read_wait|write_line_wait|read_delayCycle =>
        busy <= '1';
        lineWriter_start <= '0';
        wordWriter_start <= '0';
      when write_line_start =>
        busy <= '1';
        lineWriter_start <= '1';
        wordWriter_start <= '0';
    end case;
  end process;
end Behavioral;
