library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity axi4_rw_mutex is
  generic( AXI_ADDRESSWIDTHLOG2B    : natural range 5 to 7 := 5;
           LINESIZELOG2B            : natural range 5 to 255 := 8);
  port( read_inputAddr              : in  std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
        write_inputAddr             : in  std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
        write_isActive              : in  std_logic;
        read_blocked                : out std_logic );
end axi4_rw_mutex;

architecture Behavioral of axi4_rw_mutex is
begin
  equalReg: process(read_inputAddr, write_isActive, write_inputAddr)
  begin
    if read_inputAddr(2**AXI_ADDRESSWIDTHLOG2B - 1 downto LINESIZELOG2B - 2 ) = write_inputAddr(2**AXI_ADDRESSWIDTHLOG2B - 1 downto LINESIZELOG2B - 2) then
      read_blocked <= write_isActive;
    else
      read_blocked <= '0';
    end if;
  end process;

end Behavioral;
