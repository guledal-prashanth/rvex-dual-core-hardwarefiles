library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.common_pkg.all;
use work.core_pkg.all;
use work.cache_pkg.all;
use work.rvsys_cachedDualCore_pkg.all;
use work.bus_pkg.all;
use work.bus_addrConv_pkg.all;

entity rvnew_toplevel is
  generic (
    -- Related to rvex2axi4 bridge
    -- LSB part of memory that is ignored on translation
    ADDRESSALIGNMENTMASK        : natural range 0 to 128 := 13;

    -- AXI layout: address width and data width
    AXI_ADDRESSWIDTHLOG2B       : natural range 5 to 7 := 5;
    AXI_DATAWIDTHLOG2B          : natural range 5 to 255 := 6;

    -- Cache size setup
    L2_CACHE_LINECOUNTLOG2      : natural range 0 to 64 := 4;
    L2_CACHE_LINESIZELOG2B      : natural range 6 to 255 := 8;
    L2_CACHE_BLOCKCOUNTLOG2     : natural range 0 to 255 := 2;

    L1_ICACHE_LINECOUNT         : natural range 4 to 20 := 6;
    L1_DCACHE_LINECOUNT         : natural range 4 to 20 := 6
  );
  port (

    -- Clock and reset from PS and block diagram.
    clk                         : in  std_logic;
    reset                       : in  std_logic;

    -- The AXI4 bus interaction, this is the backside of rvex2axi4
    -- Global Signals
    maxi_aclk                   :   out std_logic;

    -- Write address channel signals
    maxi_awid                   :   out std_logic_vector(2 DOWNTO 0);
    maxi_awaddr                 :   out std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
    maxi_awlen                  :   out std_logic_vector(3 downto 0);
    maxi_awsize                 :   out std_logic_vector(2 downto 0);
    maxi_awburst                :   out std_logic_vector(1 downto 0);
    maxi_awlock                 :   out std_logic_vector(1 downto 0);
    maxi_awcache                :   out std_logic_vector(3 downto 0);
    maxi_awprot                 :   out std_logic_vector(2 downto 0);
    maxi_awqos                  :   out std_logic_vector(3 downto 0);
    maxi_awuser                 :   out std_logic_vector(4 downto 0);
    maxi_awvalid                :   out std_logic;
    maxi_awready                :   in  std_logic;

    -- Write data channel signals
    maxi_wid                    :   out std_logic_vector(2 downto 0);
    maxi_wdata                  :   out std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0);
    maxi_wstrb                  :   out std_logic_vector(2**(AXI_DATAWIDTHLOG2B - 3) - 1 downto 0);
    maxi_wlast                  :   out std_logic;
    maxi_wvalid                 :   out std_logic;
    maxi_wready                 :   in  std_logic;

    --  Write response channel signals
    maxi_bid                    :   in  std_logic_vector(2 downto 0);
    maxi_bresp                  :   in  std_logic_vector(1 downto 0);
    maxi_bvalid                 :   in  std_logic;
    maxi_bready                 :   out std_logic;

    --  Read address channel signals
    maxi_arid                   :   out std_logic_vector(2 downto 0);
    maxi_araddr                 :   out std_logic_vector(2**AXI_ADDRESSWIDTHLOG2B - 1 downto 0);
    maxi_arlen                  :   out std_logic_vector(3 downto 0);
    maxi_arsize                 :   out std_logic_vector(2 downto 0);
    maxi_arburst                :   out std_logic_vector(1 downto 0);
    maxi_arlock                 :   out std_logic_vector(1 downto 0);
    maxi_arcache                :   out std_logic_vector(3 downto 0);
    maxi_arprot                 :   out std_logic_vector(2 downto 0);
    maxi_arqos                  :   out std_logic_vector(3 downto 0);
    maxi_aruser                 :   out std_logic_vector(4 downto 0);
    maxi_arvalid                :   out std_logic;
    maxi_arready                :   in  std_logic;

    -- Read data channel signals
    maxi_rid                    :   in  std_logic_vector(2 downto 0);
    maxi_rdata                  :   in  std_logic_vector(2**AXI_DATAWIDTHLOG2B - 1 downto 0);
    maxi_rresp                  :   in  std_logic_vector(1 downto 0);
    maxi_rlast                  :   in  std_logic;
    maxi_rvalid                 :   in  std_logic;
    maxi_rready                 :   out std_logic;

    -- Data input using block RAM interface.
    arm_en                      :   in  std_logic;
    arm_addr                    :   in  std_logic_vector(11 downto 0);
    arm_rdata                   :   out std_logic_vector(31 downto 0);
    arm_wdata                   :   in  std_logic_vector(31 downto 0);
    arm_wen                     :   in  std_logic_vector(3 downto 0);

    -- Codec control serial port.
    uart_rx                     :   in  std_logic;
    uart_tx                     :   out std_logic
  );
end rvnew_toplevel;

architecture Behavioral of rvnew_toplevel is

  constant CCFG               : cache_generic_config_type := (
    instrCacheLinesLog2         => L1_ICACHE_LINECOUNT,
    dataCacheLinesLog2          => L1_DCACHE_LINECOUNT
  );

  -- rVex configuration.
  function rvex_cfg_fn return rvex_cdc_generic_config_type is
      variable cfg : rvex_cdc_generic_config_type;
  begin
      cfg                                     := RVEX_CDC_DEFAULT_CONFIG;
      cfg.core_0.numLanesLog2                 := 2;
      cfg.core_0.numLaneGroupsLog2            := 1;
      cfg.core_0.numContextsLog2              := 1;
      cfg.core_0.traceEnable                  := false;
      cfg.core_1.numLanesLog2                 := 2;
      cfg.core_1.numLaneGroupsLog2            := 1;
      cfg.core_1.numContextsLog2              := 1;
      cfg.core_1.traceEnable                  := false;
      cfg.cache_config_0                      := CCFG;
      cfg.cache_config_1                      := CCFG;
      cfg.cache_bypassRange                   := addrRange(match => "1-------------------------------");
      cfg.memDepthLog2B                       := 27;
      cfg.debugBusMap_mem                     := addrRangeAndMap(match => "001-----------------------------");
      cfg.debugBusMap_rvex0                   := addrRangeAndMap(match => "0111----------------------------");
      cfg.debugBusMap_trace0                  := addrRangeAndMap(match => "0110----------------------------");
      cfg.debugBusMap_rvex1                   := addrRangeAndMap(match => "0101----------------------------");
      cfg.debugBusMap_trace1                  := addrRangeAndMap(match => "0100----------------------------");
      return cfg;
  end rvex_cfg_fn;

  -- bAddr is the base address family
  -- This needs to be set up to translate the rVex addresses.
  -- bAddrIn is always zero, so it is ignored here.
  -- bAddrOut needs to be set by the ARM, and thus there is a register
  signal arm_bAddrOut         : std_logic_vector(31 downto 0);

  -- The new seed for the PRNG of the cache
  signal prng_newseed         : std_logic_vector(127 downto 0);

  -- The three flush signals. L1 cache, L2 cache and synchronization unit
  -- An L1 flush is both icache and dcache
  signal arm_cntrl_L1Flush    : std_logic;
  signal arm_cntrl_L2Flush    : std_logic;
  signal arm_cntrl_syncFlush  : std_logic;

  -- Signals that the bAddrOut is valid and should be forwarded to the bridge
  signal arm_cntrl_setAddr    : std_logic;

  -- Signals a reseed
  signal arm_cntrl_reseed     : std_logic;

  -- rvex/axi4 interaction signals
  -- Bus masters: the debug and the rVex.
  -- Debug also comes from UART, making this slightly confusing
  signal rvsa2bus             : bus_mst2slv_type;
  signal bus2rvsa             : bus_slv2mst_type;
  signal debug2bus            : bus_mst2slv_type;
  signal bus2debug            : bus_slv2mst_type;

  -- The slaves: rvex2axi4 bridge, rVex debug port, UART and IRQ unit
  signal bus2axi4             : bus_mst2slv_type;
  signal axi42bus             : bus_slv2mst_type;
  signal debug2rvsa           : bus_mst2slv_type;
  signal rvsa2debug           : bus_slv2mst_type;
  signal bus2uart             : bus_mst2slv_type;
  signal uart2bus             : bus_slv2mst_type;
  signal bus2irq              : bus_mst2slv_type;
  signal irq2bus              : bus_slv2mst_type;

  -- The interconnects: to split and merge debug and rvsa busses
  signal arb2demux            : bus_mst2slv_type;
  signal demux2arb            : bus_slv2mst_type;
  signal debug2demux          : bus_mst2slv_type;
  signal demux2debug          : bus_slv2mst_type;


  -- Interrupt related signals
  signal rctrl2rvsa_irq           : std_logic_vector(2**rvex_cfg_fn.core_0.numLaneGroupsLog2 + 2**rvex_cfg_fn.core_1.numLaneGroupsLog2 - 1 downto 0);
  signal rvsa2rctrl_irqAck        : std_logic_vector(2**rvex_cfg_fn.core_0.numLaneGroupsLog2 + 2**rvex_cfg_fn.core_1.numLaneGroupsLog2 - 1 downto 0);
  signal rctrl2rvsa_irqID         : rvex_address_array(2**rvex_cfg_fn.core_0.numLaneGroupsLog2 + 2**rvex_cfg_fn.core_1.numLaneGroupsLog2 - 1 downto 0);
  signal rctrl2rvsa_run           : std_logic_vector(2**rvex_cfg_fn.core_0.numLaneGroupsLog2 + 2**rvex_cfg_fn.core_1.numLaneGroupsLog2 - 1 downto 0);
  signal rvsa2rctrl_idle          : std_logic_vector(2**rvex_cfg_fn.core_0.numLaneGroupsLog2 + 2**rvex_cfg_fn.core_1.numLaneGroupsLog2 - 1 downto 0);
  signal rvsa2rctrl_break         : std_logic_vector(2**rvex_cfg_fn.core_0.numLaneGroupsLog2 + 2**rvex_cfg_fn.core_1.numLaneGroupsLog2 - 1 downto 0);
  signal rvsa2rctrl_traceStall    : std_logic;
  signal rctrl2rvsa_reset         : std_logic_vector(2**rvex_cfg_fn.core_0.numLaneGroupsLog2 + 2**rvex_cfg_fn.core_1.numLaneGroupsLog2 - 1 downto 0);
  signal rctrl2rvsa_resetVect     : rvex_address_array(2**rvex_cfg_fn.core_0.numLaneGroupsLog2 + 2**rvex_cfg_fn.core_1.numLaneGroupsLog2 - 1 downto 0);
  signal rvsa2rctrl_done          : std_logic_vector(2**rvex_cfg_fn.core_0.numLaneGroupsLog2 + 2**rvex_cfg_fn.core_1.numLaneGroupsLog2 - 1 downto 0);

  signal uart_irq                 : std_logic;

  -- Reset for axi4 is seperate, since that includes the flush
  signal axi_reset                : std_logic;

  -- Sync unit flush signals
  signal syncUnitFlush            : std_logic_vector(2**rvex_cfg_fn.core_0.numLaneGroupsLog2 + 2**rvex_cfg_fn.core_1.numLaneGroupsLog2 - 1 downto 0);
begin

  axi_reset <= reset or arm_cntrl_L2Flush;
  -- Sync unit signals: if the irq unit resets the core or generates an interrupt, flush the line belonging to that context.
  -- If the ARM requests a flush, flush all
  genSyncFlush: for I in 0 to 2**rvex_cfg_fn.core_0.numLaneGroupsLog2 + 2**rvex_cfg_fn.core_1.numLaneGroupsLog2 - 1 generate
    syncUnitFlush(I) <= rctrl2rvsa_irq(I) or rctrl2rvsa_reset(I) or arm_cntrl_syncFlush;
  end generate genSyncFlush;

  -- The arm_* signals are the way for the ARM to control the rVex.
  -- Connect them to the input bus
  -- Memory map:
  -- 0x0 - 0x3  : control. Write to lowest four bytes handles cntrl_* signals. All other bytes are ignored. Reading yields all zero.
  -- 0x4 - 0x7  : arm_bAddrOut r/w
  -- 0x8 - 0xB  : First 32 bit of PRNG seed
  -- 0xC - 0xF  : Second 32 bit of PRNG seed
  -- 0x10 - 0x13: Third 32 bit of PRNG seed
  -- 0x14 - 0x17: Fourth 32 bit of PRNG seed
  -- 0x14 = 00010100
  arm_interaction: process(clk)
    variable uAddr              : natural range 0 to 5;
  begin
    if rising_edge(clk) then
      arm_cntrl_L1Flush <= '0';
      arm_cntrl_L2Flush <= '0';
      arm_cntrl_syncFlush <= '0';
      arm_cntrl_setAddr <= '0';
      arm_cntrl_reseed <= '0';
      arm_rdata <= (others => '0');
      if reset /= '1' and arm_en = '1' then
        uAddr := to_integer(unsigned(arm_addr(4 downto 2)));
        case uAddr is
          -- Control register
          when 0 =>
            -- Reads are ignored
            if arm_wen(0) = '1' then
              arm_cntrl_L1Flush <= arm_wdata(0);
              arm_cntrl_L2Flush <= arm_wdata(1);
              arm_cntrl_syncFlush <= arm_wdata(2);
              arm_cntrl_setAddr <= arm_wdata(3);
            end if;
            if arm_wen(1) = '1' then
              arm_cntrl_reseed <= arm_wdata(4);
            end if;
          -- Readdressing register
          when 1 =>
            arm_rdata <= arm_bAddrOut;
            for b in 0 to 3 loop
              if arm_wen(b) = '1' then
                arm_bAddrOut(b*8+7 downto b*8) <= arm_wdata(b*8+7 downto b*8);
              end if;
            end loop;
          -- all parts of the 128 bit seed
          when 2 =>
            arm_rdata <= prng_newseed(127 downto 96);
            for b in 0 to 3 loop
              if arm_wen(b) = '1' then
                prng_newseed(b*8+7+96 downto b*8+96) <= arm_wdata(b*8+7 downto b*8);
              end if;
            end loop;
          when 3 =>
            arm_rdata <= prng_newseed(95 downto 64);
            for b in 0 to 3 loop
              if arm_wen(b) = '1' then
                prng_newseed(b*8+7+64 downto b*8+64) <= arm_wdata(b*8+7 downto b*8);
              end if;
            end loop;
          when 4 =>
            arm_rdata <= prng_newseed(63 downto 32);
            for b in 0 to 3 loop
              if arm_wen(b) = '1' then
                prng_newseed(b*8+7+32 downto b*8+32) <= arm_wdata(b*8+7 downto b*8);
              end if;
            end loop;
          when 5 =>
            arm_rdata <= prng_newseed(31 downto 0);
            for b in 0 to 3 loop
              if arm_wen(b) = '1' then
                prng_newseed(b*8+7 downto b*8) <= arm_wdata(b*8+7 downto b*8);
              end if;
            end loop;
        end case;
      end if;
    end if;
  end process;

  rvex_inst : entity work.rvsys_cachedDualCore
  generic map ( CFG => rvex_cfg_fn )
  port map (
    reset => reset,
    clk => clk,
    clkEn => '1',
    flushCache => arm_cntrl_L1Flush,
    flushSyncUnit => syncUnitFlush,
    rvsa2bus => rvsa2bus,
    bus2rvsa => bus2rvsa,
    debug2rvsa => debug2rvsa,
    rvsa2debug => rvsa2debug,
    rvsa2mem => bus2axi4,
    mem2rvsa => axi42bus,
    rctrl2rvsa_irq => rctrl2rvsa_irq,
    rvsa2rctrl_irqAck => rvsa2rctrl_irqAck,
    rctrl2rvsa_irqID => rctrl2rvsa_irqID,
    rctrl2rvsa_run => rctrl2rvsa_run,
    rvsa2rctrl_idle => rvsa2rctrl_idle,
    rvsa2rctrl_break => rvsa2rctrl_break,
    rvsa2rctrl_traceStall => rvsa2rctrl_traceStall,
    rctrl2rvsa_reset => rctrl2rvsa_reset,
    rctrl2rvsa_resetVect => rctrl2rvsa_resetVect,
    rvsa2rctrl_done => rvsa2rctrl_done
  );

  irq : entity work.periph_irq
  generic map(  BASE_ADDRESS => X"E0000000",
                NUM_CONTEXTS => 2**rvex_cfg_fn.core_0.numLaneGroupsLog2 + 2**rvex_cfg_fn.core_1.numLaneGroupsLog2,
                NUM_IRQ => 2 )
  port map (
    clk => clk,
    reset => reset,
    clkEn => '1',
    irq2rv_irq => rctrl2rvsa_irq,
    irq2rv_irqID => rctrl2rvsa_irqID,
    rv2irq_irqAck => rvsa2rctrl_irqAck,
    irq2rv_run => rctrl2rvsa_run,
    rv2irq_idle => rvsa2rctrl_idle,
    rv2irq_break => rvsa2rctrl_break,
    rv2irq_traceStall => rvsa2rctrl_traceStall,
    irq2rv_reset => rctrl2rvsa_reset,
    irq2rv_resetVect => rctrl2rvsa_resetVect,
    rv2irq_done => rvsa2rctrl_done,
    periph2irq(1) => '0',
    periph2irq(2) => uart_irq,
    bus2irq => bus2irq,
    irq2bus => irq2bus
  );

  UART : entity work.periph_uart
  generic map(
    F_CLK => 50000000.0
  )
  port map (
    reset => reset,
    clk => clk,
    clkEn => '1',
    rx => uart_rx,
    tx => uart_tx,
    bus2uart => bus2uart,
    uart2bus => uart2bus,
    irq => uart_irq,
    uart2dbg_bus => debug2demux,
    dbg2uart_bus => demux2debug
  );

  -- The debug master needs to be able to access the entire bus
  -- When connected directly to the rVex it is unable to, so it is split
  dbg_bus_demux_inst : entity work.bus_demux
  generic map (
      ADDRESS_MAP => (
        1 => addrRangeAndMap(
          -- 0x00000000 - 0x7FFFFFFF: rvsa debug
          low =>  X"00000000",
          high => X"7FFFFFFF"),
        2 => addrRangeAndMap(
          -- 0x80000000 - 0xFFFFFFFF: Rest of the bus
          low =>  X"80000000",
          high => X"FFFFFFFF")
      )
  )
  port map (
      reset => reset,
      clk => clk,
      clkEn => '1',
      mst2demux => debug2demux,
      demux2mst => demux2debug,
      demux2slv(1) => debug2rvsa,
      demux2slv(2) => debug2bus,
      slv2demux(1) => rvsa2debug,
      slv2demux(2) => bus2debug
  );

  -- This unit merges the debug2bus and rvsa2bus busses.
  data_bus_arbiter: entity work.bus_arbiter
  generic map (
    NUM_MASTERS             => 2
  )
  port map (
    reset                   => reset,
    clk                     => clk,
    clkEn                   => '1',
    mst2arb(1)              => debug2bus,
    mst2arb(0)              => rvsa2bus,
    arb2mst(1)              => bus2debug,
    arb2mst(0)              => bus2rvsa,
    arb2slv                 => arb2demux,
    slv2arb                 => demux2arb
  );

  bus_demux_inst : entity work.bus_demux
  generic map (
    ADDRESS_MAP => (
      1 => addrRangeAndMap(
        -- 0xD0000000 - 0xDFFFFFFF: UART unit
        low =>  X"D0000000",
        high => X"DFFFFFFF"),
      2 => addrRangeAndMap(
        -- 0xE0000000 - 0xEFFFFFFF: IRQ unit
        low =>  X"E0000000",
        high => X"EFFFFFFF")
     )
  )
  port map (
    reset => reset,
    clk => clk,
    clkEn => '1',
    mst2demux => arb2demux,
    demux2mst => demux2arb,
    demux2slv(1) => bus2uart,
    demux2slv(2) => bus2irq,
    slv2demux(1) => uart2bus,
    slv2demux(2) => irq2bus
  );

  -- Initialize the rvex2axi bridge
  rvex2AXI4: entity work.pvexAxiSlaveToplevel
  generic map (
    AXI_ADDRESSWIDTHLOG2B => AXI_ADDRESSWIDTHLOG2B,
    AXI_DATAWIDTHLOG2B => AXI_DATAWIDTHLOG2B,
    CACHE_LINECOUNTLOG2 => L2_CACHE_LINECOUNTLOG2,
    CACHE_LINESIZELOG2B => L2_CACHE_LINESIZELOG2B,
    CACHE_BLOCKCOUNTLOG2 => L2_CACHE_BLOCKCOUNTLOG2,
    HALFDELAY_ENABLED => false         
  )
  port map (
    clk => clk,
    rst => axi_reset,
    updateAddrBase  => arm_cntrl_setAddr,
    baseAddrIn      => arm_bAddrOut,
    rvex_bus_in     =>  bus2axi4,
    rvex_bus_out    =>  axi42bus,
    maxi_aclk       =>  maxi_aclk,
    maxi_awid       =>  maxi_awid,
    maxi_awaddr     =>  maxi_awaddr,
    maxi_awlen      =>  maxi_awlen,
    maxi_awsize     =>  maxi_awsize,
    maxi_awburst    =>  maxi_awburst,
    maxi_awlock     =>  maxi_awlock,
    maxi_awcache    =>  maxi_awcache,
    maxi_awprot     =>  maxi_awprot,
    maxi_awqos      =>  maxi_awqos,
    maxi_awuser     =>  maxi_awuser,
    maxi_awvalid    =>  maxi_awvalid,
    maxi_awready    =>  maxi_awready,
    maxi_wid        =>  maxi_wid,
    maxi_wdata      =>  maxi_wdata,
    maxi_wstrb      =>  maxi_wstrb,
    maxi_wlast      =>  maxi_wlast,
    maxi_wvalid     =>  maxi_wvalid,
    maxi_wready     =>  maxi_wready,
    maxi_bid        =>  maxi_bid,
    maxi_bresp      =>  maxi_bresp,
    maxi_bvalid     =>  maxi_bvalid,
    maxi_bready     =>  maxi_bready,
    maxi_arid       =>  maxi_arid,
    maxi_araddr     =>  maxi_araddr,
    maxi_arlen      =>  maxi_arlen,
    maxi_arsize     =>  maxi_arsize,
    maxi_arburst    =>  maxi_arburst,
    maxi_arlock     =>  maxi_arlock,
    maxi_arcache    =>  maxi_arcache,
    maxi_arprot     =>  maxi_arprot,
    maxi_arqos      =>  maxi_arqos,
    maxi_aruser     =>  maxi_aruser,
    maxi_arvalid    =>  maxi_arvalid,
    maxi_arready    =>  maxi_arready,
    maxi_rid        =>  maxi_rid,
    maxi_rdata      =>  maxi_rdata,
    maxi_rresp      =>  maxi_rresp,
    maxi_rlast      =>  maxi_rlast,
    maxi_rvalid     =>  maxi_rvalid,
    maxi_rready     =>  maxi_rready,
    prng_newseed    =>  prng_newseed,
    prng_reseed     =>  arm_cntrl_reseed
  );

end  Behavioral;
