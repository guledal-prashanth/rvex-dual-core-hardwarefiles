# rvex dual-core HardwareFiles

This repository is forked from original Jacko's repository (https://bitbucket.org/themasscontroller/rvex-rewrite/src). It contains mainly the toplevel source file and the required hardware files to create a rvex dual-core processor (in vivado 2017) which is a part of my thesis titled "Optimizing Multicore System Performance Using Dynamically Reconfigurable Architectures on FPGAs".
